#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description     Preprocess model data while loading. Do model specific
                stuff.
author			Christoph Heim
date created    09.07.2019
date changed    20.01.2021
usage           no args
"""
###############################################################################
import os, copy
import numpy as np
import xarray as xr
from datetime import datetime, timedelta
from numba import jit, njit
from package.nl_models import nlm
from package.nl_variables import nlv, add_var_attributes
from package.nl_variables import dimx,dimy,dimz,dimt
from package.utilities import (dt64_to_dt, subsel_domain,
                                select_common_timesteps, Timer)
from package.nl_global import inp_glob_base_dir, model_specifics_path
from package.constants import CON_G, CON_RD
import matplotlib.pyplot as plt
###############################################################################

MODEL_PP        = 'model_pp'
MODEL_PP_DONE   = 'done'

inp_base_dir = inp_glob_base_dir

debug_level_2 = 2
debug_level_4 = 4


def preproc_model(ds, mkey, var_name, date,
                  data_inp_dir,
                  #domain,
                  dims, dep_vars={}):
    """
    Organize preprocessing of models.
    """
    # for all models rename variables to convention of this package
    # (if not yet done)
    #print(nlm[mkey]['vkeys'])
    if nlm[mkey]['vkeys'][var_name] in ds:
        ds = ds.rename({nlm[mkey]['vkeys'][var_name]:var_name})

    if mkey == 'COSMO':
        ds = pp_COSMO(ds, var_name, dims)
    elif mkey == 'NICAM':
        ds = pp_NICAM(ds, var_name, dims)
    elif mkey == 'SAM':
        ds = pp_SAM(ds, var_name, date, data_inp_dir, dims)
    elif mkey == 'ICON':
        ds = pp_ICON(ds, var_name, mkey, date, data_inp_dir, dims)
    elif mkey == 'UM':
        ds = pp_UM(ds, var_name, dims)
    elif mkey == 'MPAS':
        ds = pp_MPAS(ds, var_name, date, data_inp_dir, dims)
    elif mkey == 'IFS':
        ds = pp_IFS(ds, var_name, date, data_inp_dir, dims, dep_vars)
    elif mkey == 'GEOS':
        ds = pp_GEOS(ds, var_name, mkey, data_inp_dir, dims, dep_vars)
    elif mkey == 'ARPEGE-NH':
        ds = pp_ARPEGE(ds, var_name, mkey, date, data_inp_dir, dims, dep_vars)
    elif mkey == 'FV3':
        ds = pp_FV3(ds, var_name, mkey, date, data_inp_dir, dims, dep_vars)
    elif mkey == 'ERA5':
        ds = pp_ERA5(ds, var_name, date, data_inp_dir, dims, dep_vars)

    # remove some attributes for easier readable debugging output 
    remove_attrs = ['CDI', 'history', 'Conventions',
                    'NCO', 'CDO', 'institution']
    for attr_key in remove_attrs:
        if attr_key in ds.attrs:
            del ds.attrs[attr_key]
    # set "model_pp done" flag 
    ds.attrs[MODEL_PP] = MODEL_PP_DONE
    return(ds)



def pp_COSMO(ds, var_name, dims):
    """
    Preprocess COSMO model.
    """
    # depending on sellonlatbox box, rlon or rlat might not exist.
    # (strange cdo behavior that I could not find a way around).
    #print('slatu' in ds.keys())
    #print(ds.slonu.shape)
    #print(ds)
    #quit()
    if dimx in dims:
        ## quick fix for staggered grid
        # ds comes with ... 
        if 'slonu' in ds.keys():
            if 'srlon' not in ds.keys():
                ds['srlon'] = ds['slonu'].values[0,:]
                #else:
                #    ds['rlon'] = ds['lon'].values[:]
            ds = ds.drop(['slonu', 'slatu'])
            ds = ds.rename({'srlon':'lon'})
        else:
            if 'slonv' in ds.keys():
                ds = ds.rename({'slonv':'lon'})
            # ds comes with 1d rlon/rlat and 2d lon/lat(rlon/rlat)
            if 'rlon' not in ds.keys():
                if len(ds['lon'].shape) == 2:
                    ds['rlon'] = ds['lon'].values[0,:]
                else:
                    ds['rlon'] = ds['lon'].values[:]
            if 'lon' in ds.keys():
                ds = ds.drop(['lon'])
            ds = ds.rename({'rlon':'lon'})
            #print(ds)
            #quit()
    if dimy in dims:
        if 'slatv' in ds.keys():
            #if 'srlat' not in ds.keys():
            #    ds['srlat'] = ds['slatv'].values[0,:]
            #    #else:
            #    #    ds['rlon'] = ds['lon'].values[:]
            ds = ds.drop(['slatv'])
            ds = ds.rename({'srlat':'lat'})
        else:
            if 'slatu' in ds.keys():
                ds = ds.rename({'slatu':'lat'})
            if 'rlat' not in ds.keys():
                if len(ds['lat'].shape) == 2:
                    ds['rlat'] = ds['lat'].values[:,0]
                else:
                    ds['rlat'] = ds['lat'].values[:]
            if 'lat' in ds.keys():
                ds = ds.drop(['lat'])
            ds = ds.rename({'rlat':'lat'})
    if (dimz in dims) and ('altitude' in ds):
        ds = ds.rename({'altitude':'alt'})

    # preprocessing of specific variables
    if var_name == 'PP':
        # convert total accum. precip to mm/h
        ds.PP.values = ds.PP.values / np.mean(
                    (np.diff(ds.time.values)/1E9).astype(np.float)/3600)
    return(ds)

def pp_NICAM(ds, var_name, dims):
    """
    Preprocess NICAM model.
    """
    # rename dimensions
    if 'lev' in ds.keys():
        ds = ds.rename({'lev':'alt'})

    # preprocessing of specific variables
    if var_name == 'PP':
        # convert kg/m2/s precip to mm/h
        ds = ds * 3600
    return(ds)


def pp_SAM(ds, var_name, date, data_inp_dir, dims):
    """
    Preprocess SAM model.
    """
    if (dimz in dims) and ('z' in ds):
        # rename dimensions
        ds = ds.rename({'z':'alt'})
    # preprocessing of specific variables
    if var_name == 'PP':
        # convert all time accum mm precip to mm/h
        # load last date file and then apply diff to convert
        # accumulated precip to a precip flux.
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        last_date_ds = xr.open_dataset(last_date_file)
        last_time_step = last_date_ds.isel(time=-1)
        ds = xr.concat([last_time_step, ds], dim='time')
        ds = ds.diff(dim='time')
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float)*3600

    elif var_name == 'P':
        # convert perturbation pressure to absolute pressure 
        #print(ds.mean(dim=['lon', 'lat', 'time']).PP.values)
        p_mean = ds.p*100.
        p_mean = p_mean.expand_dims(lat=ds['P'].lat, axis=1)
        p_mean = p_mean.expand_dims(lon=ds['P'].lon, axis=2)
        p_mean = p_mean.expand_dims(time=ds['P'].time, axis=0)
        ds['P'] = ds['P'] + p_mean
        #print(ds.mean(dim=['lon', 'lat', 'time']).PP.values)

    elif var_name in ['QV','QC']:
        # convert from g/kg to kg/kg
        ds[var_name] /= 1000.

    return(ds)


def pp_ICON(ds, var_name, mkey, date, data_inp_dir, dims):
    """
    Preprocess ICON model.
    """
    # do this not in dimz part because some 2D vars
    # have height as coordinate.
    if (dimz in dims) and ('height' in ds.coords):
        # rename dimensions
        ds = ds.rename({'height':'alt'})

        # transform altitude coordinate
        hgt_in = np.loadtxt(os.path.join(model_specifics_path, 'ICON_hgt.txt'))
        # convert from half levels to full levels
        hgt_in = hgt_in[1:] - np.diff(hgt_in)/2
        ds['alt'].values -= 1
        ds['alt'].values = hgt_in[ds['alt'].values.astype(np.int)]
        ds['alt'].attrs['units'] = 'm'
        # flip vertical coordinate to have increasing alt with index
        ds = ds.sel(alt=slice(None, None, -1))

    # preprocessing of specific variables
    if var_name == 'PP':
        # convert all time accum mm precip to mm/h
        # load last date file and then apply diff to convert
        # accumulated precip to precipitation flux.
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        last_date_ds = xr.open_dataset(last_date_file)
        # rename var name to package naming convention
        last_date_ds = last_date_ds.rename(
                {nlm['ICON']['vkeys'][var_name]:var_name})
        last_time_step = last_date_ds.isel(time=-1)
        ds = xr.concat([last_time_step, ds], dim='time')
        ds = ds.diff(dim='time')
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float)*3600

    if var_name in ['LWUTOA', 'SWNDTOA']:
        start_date = np.datetime64(datetime(2016,8,1))
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        #last_date_ds = xr.open_dataset(last_date_file)
        with xr.open_dataset(last_date_file) as last_date_ds:
            # rename var name to package naming convention
            last_date_ds = last_date_ds.rename(
                    {nlm['ICON']['vkeys'][var_name]:var_name})
            last_time_step = last_date_ds.isel(time=-1)
            ds = xr.concat([last_time_step, ds], dim='time')

        ## look up vkey. if var has already been computed, then take var_name
        #vkey = nlm[mkey]['vkeys'][var_name]
        #if vkey not in ds: vkey = var_name

        # determine output step (output is every 30min)
        #cout = ((ds.time - start_date)/1E9/(3600*0.5)).astype(np.float)
        cout = ((ds.time - start_date)/1E9/ \
                (np.mean(np.diff(ds.time.values)/1E9))).astype(np.float)


        time_index = list(ds.coords.keys()).index('time')
        if time_index == 2:
            for i in range(len(cout)):
                ds[var_name].values[:,:,i] *= cout.values[i]
        else:
            raise NotImplementedError()
        ds = ds.diff(dim='time')
        # alternative way how to remove running mean.
        # A(n) = n*A_mean(n) - (n-1)*A_mean(n-1)
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds.transpose('time','lat','lon')

    return(ds)


def pp_UM(ds, var_name, dims):
    """
    Preprocess UM model.
    """
    if (dimz in dims) and ('model_level_number' in ds):
        # rename dimensions
        ds = ds.rename({'model_level_number':'alt'})
        # transform altitude coordinate
        hgt_in = np.loadtxt(os.path.join(model_specifics_path, 'UM_hgt.txt'))
        ds['alt'].values = hgt_in[ds['alt'].values.astype(np.int)-1]
    # rename dimensions
    try:
        ds = ds.rename({'latitude':'lat', 'longitude':'lon'})
    except ValueError:
        pass

    # preprocessing of specific variables
    if var_name == 'PP':
        # convert kg/m2/s precip to mm/h
        ds.precipitation_flux.values = ds.precipitation_flux.values * 3600

    # TODO this exception may also ocurr in other variables
    #print(var_name)
    #print(ds.time)
    #quit()
    #if var_name in ['SWUTOA']:
    #    #print(ds.time)
    #    ds.time.values += np.timedelta64(30, 'm')
    #    #print(ds.time)
    #    #quit()
    return(ds)


def pp_MPAS(ds, var_name, date, data_inp_dir, dims):
    """
    Preprocess MPAS model.
    """
    if ( (dimz in dims) and 
         ( ('nVertLevels' in ds.dims) or ('nVertLevelsP1' in ds.dims)) ):
        # rename dimensions and load height levels
        hgt_in = np.loadtxt(os.path.join(model_specifics_path, 'MPAS_hgt.txt'))
        try:
            #print('nVertLevels')
            ds = ds.rename({'nVertLevels':'alt'})
            hgt_in = hgt_in[1:] - np.diff(hgt_in)/2
        except ValueError:
            pass
        try:
            #print('nVertLevelsP1')
            ds = ds.rename({'nVertLevelsP1':'alt'})
        except ValueError:
            pass
        ds['alt'] = hgt_in[np.arange(ds.dims['alt'])]

    # preprocessing of specific variables
    if var_name in ['SWNDTOA']:
        # load last date file and then apply diff to convert
        # accumulated radiative energy to a radiative flux.
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        last_date_ds = xr.open_dataset(last_date_file)
        # rename var name to package naming convention
        last_date_ds = last_date_ds.rename(
                {nlm['MPAS']['vkeys'][var_name]:var_name})
        last_time_step = last_date_ds.isel(time=-1)
        ds = xr.concat([last_time_step, ds], dim='time')
        ds = ds.diff(dim='time')
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float)
    if var_name in ['PPGRID', 'PPCONV']:
        # convert all time accum mm precip to mm/h
        # load last date file and then apply diff
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        last_date_ds = xr.open_dataset(last_date_file)
        # rename var name to package naming convention
        last_date_ds = last_date_ds.rename(
                {nlm['MPAS']['vkeys'][var_name]:var_name})
        last_time_step = last_date_ds.isel(time=-1)
        ds = xr.concat([last_time_step, ds], dim='time')
        ds = ds.diff(dim='time')
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float) * 3600
    return(ds)


def pp_IFS(ds, var_name, date, data_inp_dir, dims, dep_vars={}):
    """
    Preprocess IFS model.
    """
    if (dimz in dims) and ('lev' in ds):

        # skip preprocessing for altitude field because it should
        # become the vertical coordinate later and assumed to be
        # in original format (not preprocessed).
        if var_name == 'ALT': return(ds)

        ## vertical interpolation from plev to alt
        # make sure dependency ALT is here
        if 'ALT' not in dep_vars: raise ValueError('should get dependency ALT')
        # load target altitude (user defined)
        targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
                    'IFS_mean_alt_20160810_dom_SEA_Sc.dat')))

        # sort to have levels increasing in altitude direction
        # which is required for interpolation
        ds = ds.sortby('lev', ascending=False)
        dep_vars['ALT'] = dep_vars['ALT'].sortby('lev', ascending=False)
        # make sure ALT and preproc var have same time steps
        dep_vars['ALT'], ds = select_common_timesteps(dep_vars['ALT'], ds)
        # interpolate 
        var = interp_plev_to_alt(ds[var_name], dep_vars['ALT'], targ_alt,
                                vdim_name='lev')
        ds[var_name] = var
        #print(ds)
        #var.to_netcdf('test.nc')
        #quit()

        # delete unnecessary stuff
        for key in ['hyai', 'hybi', 'hyam', 'hybm']:
            if key in ds: del ds[key] 


    # preprocessing of specific variables
    if var_name in ['SWNDTOA', 'LWUTOA', 'SLHFLX', 'SSHFLX']:
        # load last date file and then apply diff to convert
        # accumulated radiative energy to a radiative flux.
        last_date_file = os.path.join(data_inp_dir,
                    '{}_{:%Y%m%d}.nc'.format(var_name,
                                        date-timedelta(days=1)))
        last_date_ds = xr.open_dataset(last_date_file)
        # rename var name to package naming convention
        last_date_ds = last_date_ds.rename(
                            {nlm['IFS']['vkeys'][var_name]:var_name})
        last_time_step = last_date_ds.isel(time=-1)
        ds = xr.concat([last_time_step, ds], dim='time')
        ds = ds.diff(dim='time')
        time_slice = slice('{:%Y-%m-%d}'.format(date),
                           '{:%Y-%m-%d}'.format(date+timedelta(days=1)))
        ds = ds.sel(time=time_slice)
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float)
        ds = ds.transpose('time','lat','lon')

        # convert LWNDTOA to LWUTOA
        if var_name == 'LWUTOA': ds[var_name] *= -1

    if var_name in ['PPCONV', 'PPGRID']:
        # convert kg/m2/s precip to mm/h
        ds = ds * 3600
    return(ds)


def pp_GEOS(ds, var_name, mkey, data_inp_dir, dims, dep_vars={}):
    """
    Preprocess GEOS model.
    """
    if (dimz in dims) and ('lev' in ds):

        # skip preprocessing for altitude field because it should
        # become the vertical coordinate later and assumed to be
        # in original format (not preprocessed).
        if var_name == 'ALT': return(ds)

        ## vertical interpolation from plev to alt
        # make sure dependency ALT is here
        if 'ALT' not in dep_vars: raise ValueError('should get dependency ALT')
        # load target altitude (user defined)
        targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
                    'GEOS_mean_alt_20160810_dom_SEA_Sc.dat')))

        # sort to have levels increasing in altitude direction
        # which is required for interpolation
        ds = ds.sortby('lev', ascending=False)
        dep_vars['ALT'] = dep_vars['ALT'].sortby('lev', ascending=False)
        # make sure ALT and preproc var have same time steps
        dep_vars['ALT'], ds = select_common_timesteps(dep_vars['ALT'], ds)
        # interpolate 
        var = interp_plev_to_alt(ds[var_name], dep_vars['ALT'], targ_alt,
                                vdim_name='lev')
        ds[var_name] = var

    ## preprocessing of specific variables
    if var_name in ['PPCONV', 'PPGRID']:
        # convert kg/m2/s precip to mm/h
        ds = ds * 3600
    ## TODO this exception may also ocurr in other variables
    # do not for SWNDTOA if you do resampling later.
    #if var_name in ['SWNDTOA', 'UV10M']:
    #    #print(ds.time)
    #    #quit()
    #    ds.time.values -= np.timedelta64(90, 'm')
    #else:
    #    print(ds.time)
    #    print(var_name)
    #    raise NotImplementedError()

    return(ds)


def pp_ARPEGE(ds, var_name, mkey, date, data_inp_dir, dims, dep_vars={}):
    """
    Preprocess ARPEGE model.
    """
    # do this not in dimz part because some 2D vars
    # have height as coordinate.
    if 'height' in ds.coords:
        # rename dimensions
        ds = ds.rename({'height':'alt'})
    if (dimz in dims) and ('lev' in ds):

        # remove levels > 6.5km and < -0.5km
        ds = ds.sel(lev=slice(45,73))

        # skip preprocessing for altitude field because it should
        # become the vertical coordinate later and assumed to be
        # in original format (not preprocessed).
        if var_name == 'ALT':
            # convert from geopotential to altitude
            ds['ALT'] /= CON_G
            ds['ALT'] = add_var_attributes(ds['ALT'], 'ALT')
            return(ds)

        #if var_name in ['W']:
        #    print('rerunning model_pp for W in ARPEGE!!!')
        #    quit()
        #    W_file = os.path.join(inp_base_dir, 'ARPEGE-NH_2.5',
        #                            'SA', 'W_mean.nc')
        #    W_mean = xr.open_dataset(W_file)
        #    W_mean = W_mean.isel(lev=59)

        #    #fig,axes = plt.subplots(1,2, figsize=(10,5))
        #    #cf1 = axes[0].pcolormesh(ds['wz'].isel(lev=59, time=0).values.squeeze(),
        #    #                        vmin=-0.01, vmax=0.01)
        #    #ds['wz'].load()
        #    #print('yoo')
        #    #W_mean['wz'].load()
        #    #print('yoo')
        #    #ds['wz'] = ds['wz'] - W_mean['wz']
        #    ds['wz'].values = ds['wz'].values - W_mean['wz'].values
        #    #ds['wz'].values -= ds['wz'].values - 3. 
        #    #cf2 = axes[1].pcolormesh(ds['wz'].isel(lev=59, time=0).values.squeeze(),
        #    #                        vmin=-0.01, vmax=0.01)
        #    #fig.colorbar(cf1, ax=axes[0])
        #    #fig.colorbar(cf2, ax=axes[1])
        #    #plt.show()
        #    #quit()

        ## vertical interpolation from plev to alt
        # make sure dependency ALT is here
        if 'ALT' not in dep_vars: raise ValueError('should get dependency ALT')
        # load target altitude (user defined)
        targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
                    'ARPEGE-NH_mean_geopot_20160810_dom_SEA_Sc.dat')))/CON_G 
        # sort to have levels increasing in altitude direction
        # which is required for interpolation
        ds = ds.sortby('lev', ascending=False)
        dep_vars['ALT'] = dep_vars['ALT'].sortby('lev', ascending=False)
        # make sure ALT and preproc var have same time steps
        dep_vars['ALT'], ds = select_common_timesteps(dep_vars['ALT'], ds)
        # interpolate 
        var = interp_plev_to_alt(ds[var_name], dep_vars['ALT'], targ_alt,
                                vdim_name='lev')
        ds[var_name] = var

    ### PREPROCESSING OF SPECIFIC VARIABLES
    # TODO this should be not required anymore
    #var_key = list(ds.data_vars.keys())[0]
    #print(ds[var_key].attrs['units'])
    #print(ds[var_key].attrs['units'] == 'J m**-2')
    if (var_name in ['SWNDTOA', 'LWUTOA']):
        # radiative fluxes are given accumulated on a daily basis.
        # Reset is done at 00:00. Thus 00:30 shows the accumulated
        # flux between 00:00 and 00:30. 06:00 shows the accum.
        # between 00:00 and 06:00.
        # convert to J/m^{2}/(output increment)
        ds = xr.concat([ds.isel(time=[0]), ds.diff(dim='time')],
                        dim='time')
        # convert to W/m^{2}
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float)
        # adjust units in data array
        ds[var_name].attrs['units'] = 'W m^{-2}'
        #print(ds.mean(dim=['lon', 'lat']).values)
    if var_name in ['PP']:
        # convert from daily accumulated values to mm/h
        # do not load previous values because reset is done after 00:00
        # only (one should however generate the daily files differently..)
        # namely such that 00:00 is part of the last date instead of this date.
        ds = ds.isel(time=range(1,len(ds.time)))
        ds = ds.diff(dim='time')
        ds = ds / np.mean(np.diff(ds.time.values)/1E9).astype(np.float) * 3600

    return(ds)


def pp_FV3(ds, var_name, mkey, date, data_inp_dir, dims, dep_vars={}):
    """
    Preprocess FV3 model.
    """
    plev_vars = ['ALT', 'QC', 'QV']
    pfull_vars = ['P', 'T', 'U', 'V', 'W'] 
    if dimz in dims:
    
        # skip preprocessing for altitude field because it should
        # become the vertical coordinate later and assumed to be
        # in original format (not preprocessed).
        if var_name == 'ALT': return(ds)

        # the plev variables are given on fewer levels than the pfull
        # variables and therefore are interpolated to the pfull levels
        if var_name in plev_vars:
            #print(ds[var_name])
            #plt.plot(ds[var_name].mean(dim=['lat','lon','time']), ds[var_name].plev)
            ds[var_name] = ds[var_name].interp(plev=dep_vars['ALT'].pfull)
            #print(ds[var_name])
            #plt.plot(ds[var_name].mean(dim=['lat','lon','time']), ds[var_name].pfull)
            #plt.show()
            #quit()
        elif var_name in pfull_var:
            pass
        else:
            raise NotImplementedError()

        # checking because some files contained wrong minute values
        if dt64_to_dt(ds.time.values[0]).minute != 0:
            print(dt64_to_dt(ds.time.values[0]))
            raise ValueError()
            quit()

        ## vertical interpolation from plev to alt
        # make sure dependency ALT is here
        if 'ALT' not in dep_vars: raise ValueError('should get dependency ALT')
        # load target altitude (user defined)
        #if var_name in plev_vars:
        #    targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
        #                'FV3_mean_alt_plev_2016081_dom_SEA_Sc.dat'))) 
        #    pvar_name = 'pfull'
        #elif var_name in pfull_vars:
        targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
                    'FV3_mean_alt_pfull_2016081_dom_SEA_Sc.dat'))) 
        pvar_name = 'pfull'
        #else:
        #    raise NotImplementedError()
        # sort to have levels increasing in altitude direction
        # which is required for interpolation
        ds = ds.sortby(pvar_name, ascending=False)
        dep_vars['ALT'] = dep_vars['ALT'].sortby(pvar_name, ascending=False)
        # make sure ALT and preproc var have same time steps
        dep_vars['ALT'], ds = select_common_timesteps(dep_vars['ALT'], ds)
        # interpolate 
        #print(ds[var_name])
        #plt.plot(ds[var_name].mean(dim=['lat','lon','time']), ds[var_name].pfull)
        #plt.show()
        var = interp_plev_to_alt(ds[var_name], dep_vars['ALT'], targ_alt,
                                vdim_name=pvar_name)
        ds[var_name] = var
        #print(ds[var_name])
        #plt.plot(ds[var_name].mean(dim=['lat','lon','time']), ds[var_name].alt)
        #plt.show()
        #quit()


    # CLCT contains this
    if 'grid_xt' in ds.dims.keys():
        ds = ds.rename({'grid_xt':'lon'})
    if 'grid_yt' in ds.dims.keys():
        ds = ds.rename({'grid_yt':'lat'})
    return(ds)



def pp_ERA5(ds, var_name, date, data_inp_dir, dims, dep_vars={}):
    """
    Preprocess ERA5 obs.
    """
    if 'longitude' in ds: ds = ds.rename({'longitude':'lon'})
    if 'latitude' in ds: ds = ds.rename({'latitude':'lat'})

    if dimz in dims:
        # skip preprocessing for altitude field because it should
        # become the vertical coordinate later and assumed to be
        # in original format (not preprocessed).
        if var_name == 'ALT':
            # convert from geopotential to altitude
            #ds['ALT'] /= CON_G
            ds['ALT'] = add_var_attributes(ds['ALT'], 'ALT')
            return(ds)

        ## vertical interpolation from plev to alt
        # make sure dependency ALT is here
        if 'ALT' not in dep_vars: raise ValueError('should get dependency ALT')
        # load target altitude (user defined)
        #targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
        #            'ERA5_plev_mean_geopot_08.2016_dom_SEA_Sc.dat'))) / CON_G
        targ_alt = np.sort(np.loadtxt(os.path.join(model_specifics_path,
                    'ERA5_mlev_mean_alt_08.2016_dom_SEA_Sc.dat')))
        # make sure ALT and preproc var have same time steps
        dep_vars['ALT'], ds = select_common_timesteps(dep_vars['ALT'], ds)

        # interpolate 
        # sort to have levels increasing in altitude direction
        # which is required for interpolation
        ds = ds.sortby('level', ascending=False)
        dep_vars['ALT'] = dep_vars['ALT'].sortby('level', ascending=False)
        var = interp_plev_to_alt(ds[var_name], dep_vars['ALT'], targ_alt,
                                vdim_name='level')
        ds[var_name] = var

        # convert pressure velocity [Pa s-1] to geometric velocity [m s-1]
        if var_name == 'W':
            if 'P' not in dep_vars: raise ValueError('should get dependency P')
            #print(ds[var_name].mean(dim=['time','lon','lat']))
            ds[var_name] /= dep_vars['P'].differentiate(coord='alt')
            #print(ds[var_name].mean(dim=['time','lon','lat']))

    if var_name in ['SWDTOA', 'SWNDTOA', 'LWUTOA']:#, 'SLHFLX', 'SSHFLX']:
        # convert hourly accumulated flux to flux per time
        ds[var_name] /= 3600.

    return(ds)



###############################################################################
###############################################################################
###############################################################################

@njit()
def interp_vprof_with_time(orig_array, src_vcoord_array,
                targ_vcoord, interp_array,
                ntime, nlat, nlon):
    """
    Helper function for vertical interpolation using Numba.
    Speedup of ~100 time compared to pure python code!
    """
    for time_ind in range(ntime):
        #print(time_ind)
        for lat_ind in range(nlat):
            #lat_ind = nlat-1
            for lon_ind in range(nlon):
                #lon_ind = nlon-1
                orig_vcol = orig_array[time_ind, :, lat_ind, lon_ind]
                src_vcoord_vcol = src_vcoord_array[time_ind, :, lat_ind, lon_ind]
                #print(orig_vcol)
                #print(src_vcoord_vcol)
                #print(targ_vcoord)
                interp_vcol = np.interp(targ_vcoord, src_vcoord_vcol, orig_vcol)
                interp_vcol[targ_vcoord < np.min(src_vcoord_vcol)] = np.nan
                #print(interp_vcol)
                #quit()
                interp_array[time_ind, :, lat_ind, lon_ind] = interp_vcol
    return(interp_array)


def interp_plev_to_alt(var_on_plev, alt_of_plev, targ_alt,
                        vdim_name='level'):
    """
    interpolate from pressure levvels to constant altitude levels.
    Currently only does linear interpolation and for extrapolation
    assumes constant values (extrapolated value = nearby value)
    Also assumes that alt_of_plev is sorted in the same direction
    as targ_alt.
    """
    # make sure variables have same order of dimensions
    if ( (list(var_on_plev.dims) != ['time', vdim_name, 'lat', 'lon']) or
         (list(alt_of_plev.dims) != ['time', vdim_name, 'lat', 'lon']) ):
        print(vdim_name)
        print(var_on_plev.dims)
        print(alt_of_plev.dims)
        raise ValueError('Variables not to have same order of dimensions')

    # make sure dimensions are identical
    for dim_key in ['time', 'lat', 'lon']:
        #print(dim_key)
        if len(var_on_plev[dim_key]) != len(alt_of_plev[dim_key]):
            raise ValueError(
                'Vars have different number of elements for {}.'.format(dim_key))
        if np.any(np.abs(var_on_plev[dim_key].values.astype(np.float) - 
                         alt_of_plev[dim_key].values.astype(np.float)) > 0):
            raise ValueError(
                'Vars have different elements for {}.'.format(dim_key))

    # create array for interpolated var values
    var_on_alt = xr.full_like(var_on_plev.isel({vdim_name:1}, drop=True),
                    np.nan).expand_dims(alt=targ_alt, axis=1)
    var_on_alt_array = np.full_like(var_on_alt.values, np.nan)
    # interpolate from plev to alt (var_on_plev -> var_on_alt)
    var_on_alt.values = interp_vprof_with_time(
                    var_on_plev.values,
                    alt_of_plev.values,
                    targ_alt,
                    var_on_alt_array,
                    len(var_on_plev.time.values),
                    len(var_on_plev.lat.values),
                    len(var_on_plev.lon.values))
    var_on_alt.alt.attrs['units'] = 'm'
    var_on_alt.alt.attrs['long_name'] = 'altitude'
    return(var_on_alt)



def fix_wrong_time_steps(ds, mkey):
    """
    If some of the time steps are not unique (who knows why, it is a mess!)
    create a manually created time series under the assumption that
    the time step is constant and that first time step in series is correct.
    """
    if len(np.unique(ds.time.values)) != len(ds.time.values):
        print('{}: fix problem with time steps in preprocessing'.format(mkey))
        supposed_dt = (np.diff(ds.time.values[0:2])/1E9).astype(np.float)
        start_date = dt64_to_dt(ds.time.values[0])
        time_steps = np.arange(start_date, start_date +
                        (len(ds.time.values)) * 
                         timedelta(seconds=int(supposed_dt)),
                         timedelta(seconds=int(supposed_dt)))
        ds.time.values = time_steps
    return(ds)



def compute_ALT_of_plev(mem_dict, P, PHLEV, T, QV, HSURF=None, i_debug=0):
    """
    Integrate geopotential to obtain altitude.
    - Attention!!! this function currently only considers surface elevation
      if it is given as an input arguemtn. Else it assumes HSURF=0
      and thus only works over the ocean!!!
    - Currently neglects QC!
    """
    mkey = mem_dict['mod']
    # model levels
    if mkey == 'ERA5':
        P = P.rename({'level':'lev'})
        T = T.rename({'level':'lev'})
        QV = QV.rename({'level':'lev'})
        lev = P.lev.values
    elif mkey == 'IFS':
        lev = P.lev.values
    elif mkey == 'FV3':
        # interpolate qv to same levels as T
        QV = QV.interp(plev=T.pfull)
        # replace levels by indices
        orig_lev = P.pfull.values
        P.pfull.values = np.arange(0,len(orig_lev))
        P = P.rename({'pfull':'lev'})
        lev = P.lev.values
        PHLEV.hlev.values = np.arange(0,len(orig_lev)+1)
        T = T.rename({'pfull':'lev'})
        T.lev.values = lev
        QV = QV.rename({'pfull':'lev'})
        QV.lev.values = lev
    else: raise NotImplementedError()

    ### COMPUTE ALT (at same time steps as P)
    #######################################################################
    # make sure the same time steps are selected in all data sets
    QV, T = select_common_timesteps(QV, T)
    P, T = select_common_timesteps(P, T)
    PHLEV, T = select_common_timesteps(PHLEV, T)
    QV, T = select_common_timesteps(QV, T)

    # load arrays into memory to speed up the vertical integration
    if i_debug >= debug_level_2: print('load fields')
    T.load()
    QV.load()
    PHLEV.load()
    if i_debug >= debug_level_2: print('load fields done')

    # altitude at full levels (vertical mass points)
    ALT = xr.full_like(P, fill_value=0)
    # altitude at half levels (vertical interfaces)
    ALTHLEV = xr.full_like(PHLEV, fill_value=0)
    # add surface height to obtain altitude as a result instead of height
    if HSURF is not None:
        ALTHLEV.loc[{'hlev':ALTHLEV.hlev[-1]}] = HSURF
    # integrate upward
    if i_debug >= debug_level_2: print('integrate')
    for l in np.flip(lev):
        if i_debug >= debug_level_4: print(l)
        # pressure at lower (altitudewise) half level
        pbelow = PHLEV.sel(hlev=l+1)
        # pressure at upper (altitudewise) half level
        pabove = PHLEV.sel(hlev=l)
        dlogp   = np.log(pbelow/pabove)
        dp      = pbelow - pabove
        alpha   = 1. - ( (pabove/dp) * dlogp )
        # virtual temperature at full level
        Tv_full = T.sel(lev=l) * (1. + 0.609133 * QV.sel(lev=l) )
        # compute geopotential at this full level from half level below
        ALT.loc[{'lev':l}] = (ALTHLEV.sel(hlev=l+1) + Tv_full * 
                                    CON_RD * alpha )
        # compute geopotential at next half level
        ALTHLEV.loc[{'hlev':l}] = (ALTHLEV.sel(hlev=l+1) + Tv_full * 
                                    CON_RD * dlogp)
    if i_debug >= debug_level_2: print('integrate done')

    # convert geopotential to height
    ALT /= CON_G
    ALT = add_var_attributes(ALT, 'ALT')
    ALT['time'] = P.time
    if mkey in ['ERA5']:
        ALT['longitude'] = P.longitude
        ALT['latitude'] = P.latitude
    elif mkey in['IFS', 'FV3']:
        ALT['lon'] = P.lon
        ALT['lat'] = P.lat
    else: raise NotImplementedError()

    # replace labels with original ones
    if mkey == 'ERA5':
        ALT = ALT.rename({'lev':'level'})
    elif mkey == 'FV3':
        ALT.lev.values = orig_lev
        ALT = ALT.rename({'lev':'pfull'})

    return(ALT)

def compute_P_plev(mem_dict, T):
    mkey = mem_dict['mod']
    # simply take pressure from vertical coordinate
    #if mkey == 'ERA5':
    #    P = T.level.expand_dims(dim={
    #                            'time':T.time,
    #                            'latitude':T.latitude,
    #                            'longitude':T.longitude})
    if mkey == 'FV3':
        P = T.pfull.expand_dims(dim={
                                'time':T.time,
                                'lat':T.lat,
                                'lon':T.lon})
    P['time'] = T.time
    P['lon'] = T.lon
    P['lat'] = T.lat
    P = P.copy()
    P.values *= 100
    P = add_var_attributes(P, 'P')
    #if mkey == 'ERA5':
    #    P = P.transpose('time', 'level', 'latitude', 'longitude')
    if mkey == 'FV3':
        P = P.transpose('time', 'pfull', 'lat', 'lon')
    return(P)

def interp_PHLEV_from_P(mem_dict, PS, P):
    """
    Use PS as lowest half level for all levels above, apply 
    simple linear interpolation.... (TODO: could be improved..)
    """
    # for ERA5
    if 'level' in P.dims:
        P = P.rename({'level':'lev'})

    mkey = mem_dict['mod']
    # load variables into memory for later speedup
    P.load()
    if mkey in ['ERA5', 'IFS']:
        lev = P['lev'].values
        # extend full levels by one at the end to obtain
        # half levels
        hlev = np.append(lev, lev[-1]+1)
    elif mkey == 'FV3':
        lev = P['pfull'].values
        # simply set surface half level to some pressure value
        # as a place holder.
        # and keep other levels like full levels
        # this is all incorrect but the level names only act 
        # as names from here on.
        hlev = np.append(lev, 1000.)
        P = P.rename({'pfull':'lev'})
    else: raise NotImplementedError()

    PHLEV = PS.copy()
    PHLEV = PHLEV.sel(time=P.time)
    PHLEV = PHLEV.expand_dims(dim={'hlev':hlev}, axis=1)
    PHLEV = PHLEV.copy()
    # set surface to surface pressure
    PHLEV.loc[{'hlev':hlev[-1]}] = PS.sel(time=PHLEV.time).values
    # interpolate pressure of non-boundary levels
    PHLEV.loc[{'hlev':hlev[1:-1]}] = 0.5 * (P.sel(lev=lev[1:]).values + 
                                         P.sel(lev=lev[:-1]).values)
    ## uppermost (geometrically speaking) half level pressure is not required
    ## for integration of geopotential
    ##PHLEV.loc[{'hlev':hlev[0]}] = np.nan
    # extrapolate uupermost half level pressure
    PHLEV.loc[{'hlev':hlev[0]}] = P.sel(lev=lev[1]).values + 1.5 * (
                                        P.sel(lev=lev[1]).values - 
                                        P.sel(lev=lev[2]).values   )

    return(PHLEV)

def compute_P_hybplev(mem_dict, PS, T):
    mkey = mem_dict['mod']

    # for ERA5
    if mkey == ['ERA5']:
        if 'level' in T.dims:
            T = T.rename({'level':'lev'})
        if 'latitude' in T.dims:
            T = T.rename({'latitude':'lat'})
            PS = PS.rename({'latitude':'lat'})
        if 'longitude' in T.dims:
            T = T.rename({'longitude':'lon'})
            PS = PS.rename({'longitude':'lon'})

    # number of full levels in the model output
    nlev = len(T.lev)

    ### COMPUTE P (at same time steps as T)
    #######################################################################
    # hybrid pressure coordinates
    # compute pressure at mid-levels based on hyam + hybm*PS
    ## orig (correct) alt inds as reference for new method
    #alt_inds = {'IFS':93, 'ARPEGE-NH':15}
    # total number of full vertical levels for whic the model was run
    # determined based on vertical grid parameters
    tot_nlev = len(np.loadtxt(os.path.join(model_specifics_path,
                    '{}_hyam.txt'.format(mkey))))
    v_ind_start = tot_nlev - nlev
    # levels and half levels of imported data set
    lev = np.arange(tot_nlev - nlev+1, tot_nlev+1)
    #hlev = np.arange(tot_nlev - nlev+1, tot_nlev+2)
    # full level hybrid coordinate components
    hyam = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
                    '{}_hyam.txt'.format(mkey)))[v_ind_start:],
                    dims=('lev',), coords={'lev':lev})
    hybm = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
                    '{}_hybm.txt'.format(mkey)))[v_ind_start:],
                    dims=('lev',), coords={'lev':lev})
    # compute level pressure from PS and hyam, hybm
    P = PS.load()
    P = P.sel(time=T.time)
    P['time'] = T.time
    P = P.expand_dims(dim={'lev':lev}, axis=1)
    P = hyam + P*hybm
    P['lev'] = T.lev
    P = P.transpose('time', 'lev', 'lat', 'lon')
    P = add_var_attributes(P, 'P')

    ## estimation of half-level pressure
    #PHLEV = PS.copy()
    #PHLEV = PHLEV.sel(time=T.time)
    #PHLEV = PHLEV.expand_dims(dim={'hlev':hlev}, axis=1)
    ## For IFS, we have given the grid parameters for the half levels
    ## and can use them.
    #if mkey == 'IFS':
    #    # half level hybrid coordinate components
    #    hyai = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
    #                    '{}_hyai.txt'.format(mkey)))[v_ind_start:],
    #                    dims=('hlev',), coords={'hlev':hlev})
    #    hybi = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
    #                    '{}_hybi.txt'.format(mkey)))[v_ind_start:],
    #                    dims=('hlev',), coords={'hlev':hlev})
    #    # compute level and half-level pressure from PS and hyax, hybx
    #    PHLEV = hyai + PHLEV*hybi
    #    PHLEV = PHLEV.transpose('time', 'hlev', 'lat', 'lon')

    # for ERA5
    if mkey == ['ERA5']:
        P = P.rename({'lev':'level',
                      'lat':'latitude',
                      'lon':'longitude'})


    return(P)
