#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description     Namelist for analysis of models
author			Christoph Heim
date created    09.07.2019
date changed    03.03.2021
usage           use in another script
"""
###############################################################################
import os
###############################################################################

nlm = {
###############################################################################
################### MODELS
###############################################################################

'COSMO' :{
    'vkeys'     :{
        'HSURF'     :'HSURF', 

        'U'         :'U', 
        'V'         :'V', 
        'W'         :'W', 
        'T'         :'T', 
        'P'         :'P', 
        'QC'        :'QC', 
        'QV'        :'QV', 
        'QI'        :'QI', 
        'TKE'       :'TKE', 

        'U10M'      :'U_10M',
        'V10M'      :'V_10M',
        'T2M'       :'T_2M',
        'RH2M'      :'RELHUM_2M', 
        'QV2M'      :'QV_2M', 
        'PS'        :'PS', 

        'SWNDTOA'   :'ASOB_T',    
        'SWDTOA'    :'ASOD_T',    
        'LWUTOA'    :'ATHB_T',    
        'CSWNDTOA'  :'ASOBC_T',   
        'CLWUTOA'   :'ATHBC_T',   
        'SWNDSFC'   :'ASOB_S',    
        'SWDIFDSFC' :'ASWDIFD_S',    
        'SWDIRDSFC' :'ASWDIR_S',    
        'SWDIFUSFC' :'ASWDIFU_S',    
        'LWNDSFC'   :'ATHB_S',    
        'LWDSFC'    :'ATHD_S',    
        'CSWNDSFC'  :'ASOBC_S',   
        'CLWNDSFC'  :'ATHBC_S',   

        'SLHFLX'    :'ALHFL_S',
        'SSHFLX'    :'ASHFL_S',
        'SUMFLX'    :'AUMFL_S',
        'SVMFLX'    :'AVMFL_S',
        'TSURF'     :'T_S',       
        'QVSURF'    :'QV_S',      

        'CLCL'      :'CLCL',      
        'CLCM'      :'CLCM',      
        'CLCH'      :'CLCH',      

        'TQC'       :'TQC', 
        'TQI'       :'TQI',       
        'TQR'       :'TQR',       
        'TQV'       :'TQV',       
        'TQG'       :'TQG',       
        'TQS'       :'TQS',       
        'PP'        :'TOT_PREC', 

        'GUST10M'   :'VMAX_10M',       
        'TMAX2M'    :'TMAX_2M',       
        'TMIN2M'    :'TMIN_2M',       
        'WSOIL'     :'W_SO',       
        'RUNOFFG'   :'RUNOFF_G',       
        'RUNOFFS'   :'RUNOFF_S',       
    },
    'vgrid'     :os.path.join('model_specific','COSMO_hgt.txt'),
},


'NICAM' :{
    'vkeys'     :{
        'U'         :'ms_u', 
        'V'         :'ms_v', 
        'W'         :'ms_w', 
        'T'         :'ms_tem', 
        'P'         :'ms_pres', 
        'ALT'       :None,
        'QC'        :'ms_qc', 
        'QV'        :'ms_qv', 

        'U10M'      :'ss_u10m',
        'V10M'      :'ss_v10m',
        'T2M'       :'ss_t2m',
        'MSLP'      :'ss_slp',

        'LWUTOA'    :'sa_lwu_toa', 
        'SWUTOA'    :'ss_swu_toa', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        #'SST'       :'oa_sst',
        'SST'       :'var34', # from IFS
        'SLHFLX'    :'ss_lh_sfc',
        'SSHFLX'    :'ss_sh_sfc',

        'CLCT'      :None,
        'CLCL'      :None,

        'TQC'       :'sa_cldw', 
        'TQI'       :'sa_cldi', 
        'TQV'       :None,
        'PP'        :'sa_tppn', 
    },
},

'SAM' :{
    'vkeys'     :{
        'U'         :'U', 
        'V'         :'V', 
        'W'         :'W', 
        'T'         :'TABS', 
        'P'         :'PP', 
        'ALT'       :None,
        'QC'        :'QC', 
        'QV'        :'QV', 

        'U10M'      :'U10m',
        'V10M'      :'V10m',
        'T2M'       :'T2m',
        'PS'        :'PSFC',

        'LWUTOA'    :'LWNTA', 
        'SWNDTOA'   :'SWNTA', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'LHF',
        'SSHFLX'    :'SHF',

        'CLCT'      :None,
        'CLCL'      :None,

        'TQC'       :'CWP', 
        'TQI'       :'IWP', 
        'TQV'       :None,
        'PP'        :'Precac', 
    },
},

'ICON'  :{
    'vkeys'     :{
        'U'         :'U', 
        'V'         :'V', 
        'W'         :'W', 
        'T'         :'T', 
        'P'         :'P', 
        'ALT'       :None,
        'QV'        :'QV', 
        'QC'        :'QC_DIA', 

        'U10M'      :'U_10M',
        'V10M'      :'V_10M',
        'T2M'       :'T_2M',
        'PS'        :'PS',

        'LWUTOA'    :'ATHB_T', 
        'SWNDTOA'   :'ASOB_T', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'LHFL_S',
        'SSHFLX'    :'SHFL_S',

        'CLCT'      :'CLCT',
        'CLCL'    :None,

        'TQC'       :'TQC_DIA', 
        'TQI'       :'TQI_DIA', 
        'TQV'       :'TQV_DIA', 
        'PP'        :'TOT_PREC', 
    },
    'vgrid'     :os.path.join('model_specific','ICON_hgt.txt'),
},

'UM' :{
    'vkeys'     :{
        'U'         :'eastward_wind', 
        'V'         :'northward_wind', 
        'W'         :'upward_air_velocity', 
        'T'         :'air_temperature', 
        'P'         :'air_pressure', 
        'ALT'       :None,
        'QC'        :'mass_fraction_of_cloud_liquid_water_in_air', 
        'QV'        :'specific_humidity', 

        'U10M'      :'eastward_wind',
        'V10M'      :'northward_wind',
        'T2M'       :'air_temperature',
        'PS'        :'surface_air_pressure', 

        'LWUTOA'    :'toa_outgoing_longwave_flux', 
        'SWUTOA'    :'toa_outgoing_shortwave_flux', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'surface_upward_latent_heat_flux',
        'SSHFLX'    :'surface_upward_sensible_heat_flux',

        'CLCT'      :'cloud_area_fraction',
        'CLCL'    :None,

        'TQC'       :'atmosphere_mass_content_of_cloud_condensed_water', 
        'TQI'       :'atmosphere_mass_content_of_cloud_ice', 
        'TQV'       :'atmosphere_water_vapor_content', 
        'PP'        :'precipitation_flux', 
    },
    'vgrid'     :os.path.join('model_specific','UM_hgt.txt'),
},

'MPAS' :{
    'vkeys' :{
        'U'         :'uReconstructZonal', 
        'V'         :'uReconstructMeridional', 
        'W'         :'w', 
        'T'         :'temperature', 
        'P'         :'pressure', 
        'ALT'       :None,
        'QC'        :'qc', 
        'QV'        :'qv', 

        'U10M'      :'u10',
        'V10M'      :'v10',
        'T2M'       :'t2m',
        'MSLP'      :'mslp',

        'LWUTOA'    :'olrtoa', 
        'SWNDTOA'   :'acswnett', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :None,
        'SLHFLX'    :None,
        'SSHFLX'    :None,

        'CLCT'      :'cldcvr',
        'CLCL'      :None,

        'TQC'       :'vert_int_qc', 
        'TQI'       :'vert_int_qi', 
        'TQV'       :'vert_int_qv',
        'PPGRID'    :'rainnc', 
        'PPCONV'    :'rainc', 
    },
    'vgrid'     :os.path.join('model_specific','MPAS_hgt.txt'),
},

'IFS' :{
    'vkeys'     :{
        'U'         :'u',
        'V'         :'v',
        'W'         :'param120.128.192',
        'T'         :'t', 
        'P'         :'P', # from preproc
        'ALT'       :'ALT', # from preproc
        'QC'        :'clwc', 
        'QV'        :'q', 

        'U10M'      :'var165',
        'V10M'      :'var166',
        'T2M'       :'var167',
        'PS'        :'var151',

        'LWUTOA'    :'var179', 
        'SWNDTOA'   :'var178', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34',
        'SLHFLX'    :'var147',
        'SSHFLX'    :'var146',

        'CLCT'      :'var164',
        'CLCL'      :'var186',

        'TQC'       :'var78', 
        'TQI'       :'var79', 
        'TQV'       :'var137',
        'PPGRID'    :'var219', 
        'PPCONV'    :'var218', 
    },
    'dep' : {
        'U'         :['ALT'],
        'V'         :['ALT'],
        'W'         :['ALT'],
        'T'         :['ALT'],
        'P'         :['ALT'],
        'QV'        :['ALT'],
        'QC'        :['ALT'],
    }
    #'vgrid'     :os.path.join('model_specific','IFS_hgt.txt'),
},

'GEOS' :{
    'vkeys'     :{
        'U'         :'U',
        'V'         :'V',
        'W'         :'W', 
        'T'         :'T', 
        'P'         :'P',
        'ALT'       :'H', 
        'QC'        :'QL', 
        'QV'        :'QV', 

        'U10M'      :'U10M',
        'V10M'      :'V10M',
        'T2M'       :'T2M',
        'PS'        :'PS',

        'LWUTOA'    :'OLR', 
        'SWNDTOA'   :'SWTNET', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'EFLUX',
        'SSHFLX'    :'HFLUX',

        'CLCT'      :None,
        'CLCL'      :None,

        'TQC'       :'CWP', 
        'TQI'       :'IWP', 
        'TQV'       :'TQV',
        'PP'        :'PRECTOT', 
    },
    'dep' : {
        'U'         :['ALT'],
        'V'         :['ALT'],
        'W'         :['ALT'],
        'T'         :['ALT'],
        'P'         :['ALT'],
        'QV'        :['ALT'],
        'QC'        :['ALT'],
    }
},

'ARPEGE-NH' :{
    'vkeys'     :{
        'U'         :'u',
        'V'         :'v',
        'W'         :'wz', 
        'T'         :'t', 
        'P'         :None,
        'ALT'       :'z', 
        'QC'        :'clwc', 
        'QV'        :'q', 

        'U10M'      :'10u',
        'V10M'      :'10v',
        'T2M'       :'2t',
        'PS'        :'sp',

        'LWUTOA'    :'ttr', 
        'SWNDTOA'   :'nswrf', 
        'SWDTOA'    :'ASOD_T', # from COSMO

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'lhtfl',
        'SSHFLX'    :'shtfl',

        'CLCT'      :'tcc',
        'CLCL'      :'lcc',

        'TQC'       :'tclw', 
        'TQI'       :'tciw', 
        'TQV'       :'tciwv',
        'PP'        :'param8.1.0', 
    },
    'dep' : {
        'U'         :['ALT'],
        'V'         :['ALT'],
        'W'         :['ALT'],
        'T'         :['ALT'],
        'P'         :['ALT'],
        'QV'        :['ALT'],
        'QC'        :['ALT'],
    }
},

'FV3' :{
    'vkeys'     :{
        'U'         :'u',
        'V'         :'v',
        'W'         :'w', 
        'T'         :'temp', 
        'P'         :None,
        #'ALT'       :'h_plev', 
        'ALT'       :'ALT',  # preprocessing
        'QC'        :'ql_plev', 
        'QV'        :'q_plev', 

        'U10M'      :'u10m',
        'V10M'      :'v10m',
        'T2M'       :'t2m',
        'PS'        :'ps',

        'LWUTOA'    :'flut', 
        'SWUTOA'    :'fsut', 
        'SWDTOA'    :'fsdt', 

        'SST'       :'var34', # from IFS
        'SLHFLX'    :'lhflx',
        'SSHFLX'    :'shflx',

        'CLCT'      :'cldc',
        'CLCL'      :None,

        'TQC'       :'intql', 
        'TQI'       :'intqi', 
        'TQV'       :'intqv',
        'PP'        :'pr', 
    },
    'dep' : {
        'U'         :['ALT'],
        'V'         :['ALT'],
        'W'         :['ALT'],
        'T'         :['ALT'],
        'P'         :['ALT'],
        'QV'        :['ALT'],
        'QC'        :['ALT'],
    }
},

###############################################################################
################### REANALYSIS
###############################################################################
'ERA5' :{
    'vkeys' :{
        'HSURF'     :'z', 

        'U'         :'u', 
        'V'         :'v', 
        'W'         :'w', 
        'T'         :'t', 
        'P'         :'P', # from preprocess_mod
        'ALT'       :'z', 
        'QV'        :'q', 
        'QC'        :'clwc', 

        'PS'        :'sp', 
        'CLCM'      :'mcc',

        'U10M'      :'u10',
        'V10M'      :'v10',

        'LWUTOA'    :'ttr', 
        'SWDTOA'    :'tisr', 
        'SWNDTOA'   :'tsr', 

        #'SLHFLX'    :'mean_surface_latent_heat_flux', # for cds download
        #'SSHFLX'    :'mean_surface_sensible_heat_flux', # for cds download
        'SLHFLX'    :'mslhf',
        'SSHFLX'    :'msshf',

        'TQC'       :'tclw', 
        'TQI'       :'tciw', 
        'TQV'       :'tcwv', 
        'PP'        :'tp', 

    },
    'dep' : {
        'U'         :['ALT'],
        'V'         :['ALT'],
        'W'         :['ALT','P'],
        'T'         :['ALT'],
        'P'         :['ALT'],
        'QV'        :['ALT'],
        'QC'        :['ALT'],
    }
},


###############################################################################
################### OBSERVATIONS
###############################################################################


'CM_SAF_MSG_AQUA_TERRA' :{
    'vkeys' :{
        'SWUTOA':'Data1', 
        'SWDTOA'    :'ASOD_T', # from COSMO
        #'SWDTOA'    :'tisr', # from ERA5
        'LWUTOA':'Data1', 
    },
},
'CM_SAF_MSG' :{
    'vkeys' :{
        'TQI':'iwp', 
        'CLCL'  :'cfc_low', 
        'CLCL2' :'cfc_low', 
    },
},
'CM_SAF_METEOSAT' :{
    'vkeys' :{
        'SWUTOA':'rsut', 
        'LWUTOA':'rlut', 
    },
},
'CM_SAF_HTOVS' :{
    'vkeys' :{
        'TQV':'HTW_TPWm', 
    },
},
'SUOMI_NPP_VIIRS' :{
    'vkeys' :{
        'CORREFL':'CORREFL', 
    },
},
'RADIO_SOUNDING' :{
    'vkeys' :{
        'INVHGT':'INVHGT', 
    },
},

}












