#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    Class that handles the member of an analysis. A member could
                for instance be a variable of a certain simulation at
                specific resolution or an observational data set.
                The idea of a member is that you have for one variable
                several members to compare them.
author			Christoph Heim
date created    21.03.2019
date changed    16.07.2020
usage			use in another script
"""
###############################################################################
import time, cartopy
import xarray as xr
import matplotlib.pyplot as plt
###############################################################################

class Member:

    def __init__(self, var, mem_dict, comparison=None):
        """
        -----------------------------------------------------------------------
        INPUT:
        var:            xr Dataarray - xarray Dataarray of variable
        mem_dict:       dict - Dictionary with metadata for this member.
        comparison:     Comparison - Instance of Comparison class linking all
                        members for a comparison.
                        Is used to extract global value maxima for plotting.
        -----------------------------------------------------------------------
        COMMENTS:
        """
        self.var = var
        self.mem_dict = mem_dict
        self.comparison = comparison

        if 'label' not in self.mem_dict:
            raise ValueError('Member dict does not contain label')
        
        # mappable object for global colorbars
        self.mappable = None
    ###########################################################################



    def plot_lat_lon(self, ax, nlp=None):
        """
        Add lat/lon plot to ax.
        -----------------------------------------------------------------------
        INPUT:
        -----------------------------------------------------------------------
        OUTPUT:
        -----------------------------------------------------------------------
        COMMENTS:
        No testing for dimensions implemented: TODO
        """
        if nlp is None:
            try:
                from package.nl_plot_global import nlp
                print('Member.plot_lat_lon uses global plot namelist'+
                      ' (package.nl_plot_global).')
            except ModuleNotFoundError:
                print('Member.plot_lat_lon uses default plot namelist values.')
                nlp = {}
                nlp['cmap'] = 'jet'
                nlp['projection'] = cartopy.crs.PlateCarree()
                nlp['2D_type'] = 'contourf'
        
        # in case variable.stat has been computed
        try:
            if nlp['2D_type'] == 'pcolormesh':
                #print(self.mem_dict)
                #print(self.ds)
                self.mappable = self.var.squeeze().\
                        plot.pcolormesh(ax=ax,
                        cmap=nlp['cmap'], vmin=self.comparison.stats['min'],
                        vmax=self.comparison.stats['max'], 
                        add_colorbar=False, add_labels=False)
            elif nlp['2D_type'] == 'contourf':
                self.mappable = self.var.squeeze().\
                        plot.contourf(ax=ax,
                        cmap=nlp['cmap'], vmin=self.comparison.stats['min'],
                        vmax=self.comparison.stats['max'], levels=100,
                        add_colorbar=False, add_labels=False)
        # if not
        except AttributeError:
            self.mappable = self.var.squeeze().\
                    plot.pcolormesh(ax=ax,
                    cmap=nlp['cmap'], transform=nlp['projection'],
                    add_colorbar=False, add_labels=False)

        ax.set_title(self.mem_dict['label'])

        if self.comparison is not None:
            self.comparison.mappable = self.mappable

        

    def plot_alt_lon(self, ax, nlp=None):
        """
        Add lat/lon plot to ax.
        -----------------------------------------------------------------------
        INPUT:
        -----------------------------------------------------------------------
        OUTPUT:
        -----------------------------------------------------------------------
        COMMENTS:
        No testing for dimensions implemented: TODO
        """
        if nlp is None:
            try:
                from package.nl_plot_global import nlp
                print('Member.plot_lat_lon uses global plot namelist'+
                      ' (package.nl_plot_global).')
            except ModuleNotFoundError:
                print('Member.plot_lat_lon uses default plot namelist values.')
                nlp = {}
                nlp['cmap'] = 'jet'
                nlp['projection'] = cartopy.crs.PlateCarree()
                nlp['2D_type'] = 'contourf'
        
        # in case variable.stat has been computed
        try:
            self.mappable = self.var.squeeze().\
                    plot.contourf(ax=ax,
                    cmap=nlp['cmap'], vmin=self.comparison.stats['min'],
                    vmax=self.comparison.stats['max'], levels=100,
                    add_colorbar=False, add_labels=False)
        # if not
        except AttributeError:
            self.mappable = self.var.squeeze().\
                    plot.pcolormesh(ax=ax,
                    cmap=nlp['cmap'], transform=nlp['projection'],
                    add_colorbar=False, add_labels=False)

        ax.set_title(self.mem_dict['label'])

        if self.comparison is not None:
            self.comparison.mappable = self.mappable

        #CF = ax.contourf(mem.var.lon.values, mem.var.alt.values, mem.var.values)
        #fig.color
