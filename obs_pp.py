#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description     Preprocess observation data sets while loading. Do stuff
                specific to each data set and the analysis.
author			Christoph Heim
date created    13.02.2020
date changed    24.04.2020
usage           no args
"""
###############################################################################
import os, re
import numpy as np
import xarray as xr
from datetime import datetime, timedelta
from scipy.interpolate import interp1d
import rasterio as rio
from package.nl_obs import nlo
#from package.nl_variables import dimx,dimy,dimz,dimt
#from package.utilities import dt64_to_dt
###############################################################################

#def preproc_obs(ds, okey, var_name):
#    """
#    Organize preprocessing of observations.
#    """
#    if okey == 'SUOMI_NPP_VIIRS':
#        ds = pp_SUOMI_NPP_VIIRS(ds, var_name)
#    elif okey == 'RADIO_SOUNDING':
#        pass
#    elif okey == 'CM_SAF':
#        pass
#    else:
#        raise NotImplementedError()
#
#    # GENERAL THINGS TO DO
#    # remove attributes for better data overview
#    keys = list(ds.attrs.keys())
#    for key in keys:
#        del ds.attrs[key]
#
#    return(ds)
#
#
#
#def pp_SUOMI_NPP_VIIRS(ds, var_name):
#    """
#    Preprocess SUOMI_NPP_VIIRS observation.
#    """
#    # preprocessing of specific variables
#    if var_name == 'CORREFL':
#
#        ## set values below threshold to zero to get same blue ocean
#        ## as in models
#        #ocean_mask = ds['CORREFL'].values < 0.38
#        ##print('mask {}'.format(
#        ##    np.sum(ocean_mask) / (np.sum(ocean_mask)+
#        ##                        np.sum(~ocean_mask))))
#        #ds['CORREFL'].values[ocean_mask] = 0.
#
#        ## stretch towards 1 (to make more comparable to model tqc)
#        #ds['CORREFL'] /= 0.8
#        ##ds['CORREFL'].values[ds['CORREFL'].values > 1] = 1.0
#    
#        # convert corrected reflectance to units of lwp to
#        # make comparable with models.
#        ds = ds * 0.30
#
#
#    return(ds)
#

#def pp_CLDHGT_hdf_to_nc(raw_inp_file):



def pp_CORREFL_tiff_to_nc(tiff_dir, nc_dir, i_aggregate_rgb=0):
    dates = [datetime.strptime(file[9:19], '%Y-%m-%d') \
                for file in os.listdir(tiff_dir)]
    dates.sort()

    for dtI,dt in enumerate(dates):
        print(dt)
        nc_dt = dt + timedelta(hours=12)
        img_name = 'snapshot-{:%Y-%m-%d}T00_00_00Z.tiff'.format(dt)

        sat_tiff =  rio.open(os.path.join(tiff_dir,img_name))
        nx = sat_tiff.meta['width']
        ny = sat_tiff.meta['height']
        dx = sat_tiff.res[0]
        dy = sat_tiff.res[1]
        lons = np.arange(sat_tiff.bounds.left,
                         sat_tiff.bounds.right - dx/2, dx)
        lats = np.arange(sat_tiff.bounds.bottom,
                         sat_tiff.bounds.top - dy/2, dy)
        data = sat_tiff.read()

        ## THIS IS NOT NECESSARY FOR VIIRS BECAUSE IT DOES NOT HAVE
        ## GAPS. MODIS DOES.
        ## set entries with sum(rgb) == 0 to np.nan
        ## this gives an approximate satellite coverage mask
        ## approx. because there might be some few areas with natural 0 rgb.
        #data = data.astype(np.float)
        #data = np.flip(data, axis=1)
        #mask = np.sum(data, axis=0) == 0
        #for r in range(3):
        #    data[r,:,:][mask] = np.nan
        #data = np.expand_dims(data, axis=0)

        data = data.astype(np.float)
        data = np.flip(data, axis=1)
        ## convert RGB 8 bit to 0-1.
        for r in range(3):
            data[r,:,:] /= 255
        data = np.expand_dims(data, axis=0)

        if i_aggregate_rgb:
            data = data.mean(1)
            data = xr.DataArray(data, coords={'time':[nc_dt], 
                                'lat':lats,'lon':lons},
                                dims=['time', 'lat','lon'],
                                name='CORREFL')
            # coarse grain array to 2km x 2km
            data = data.coarsen(lon=2, lat=2).mean()
            data = data.rename('CORREFL')
        else:
            data = xr.DataArray(data, coords={'time':[nc_dt], 
                                'rgb':['r','g','b'],'lat':lats,'lon':lons},
                                dims=['time', 'rgb','lat','lon'],
                                name='CORREFL')

        out_file = os.path.join(nc_dir, 'CORREFL_{:%Y%m%d}.nc'.format(dt))
        data.to_netcdf(out_file, 'w')



def pp_preprocess_radio_sounding_day(dt, hght, pres, temp, dpd, wdir, wind):
    # finalize day array
    for numb in [-9999, -8888]:
        hght[hght == numb] = np.nan
        temp[temp == numb] = np.nan
        dpd[dpd == numb] = np.nan
        wdir[wdir == numb] = np.nan
        wind[wind == numb] = np.nan

    #inds_temp = np.argwhere(~np.isnan(temp) & ~np.isnan(dpd))
    #print(inds_temp)
    # convert variables
    # temperature to temperature
    temp = temp/10 + 273.15
    # dew point depression to qv
    dpd = dpd/10 + 273.15
    # wind speed
    wind = wind/10
    # store in dict
    this_day = {
        'date'  :dt,
        'pres'  :pres,
        'hght'  :hght,
        'temp'  :temp,
        'dpd'   :dpd,
        'wdir'  :wdir,
        'wind'  :wind,
    }      
    return(this_day)

#def pp_radio_sounding_compute_user_vars(this_day):
#    if var_name == 'RH':
#        print('RH')
#    return(this_day)

def pp_preproc_radio_sounding(raw_inp_file, var_name, n_lowest):
    """
    """
    lon=-5.6672,
    lat=-15.9419,
    line_inds = {
        'pres':slice(9,15), # pressure
        'hght':slice(16,21), # height
        'temp':slice(22,27), # air temperature
        #'relh':slice(28,33), # not in data set (use dpd).
        'dpd':slice(34,39), # dew point depression
        'wdir':slice(40,45), # wind direction
        'wind':slice(46,51), # wind speed
    }

    # READ DATA FROM FILE
    with open(raw_inp_file, 'r') as f:
        raw_in = f.readlines()
    day_data = []
    c = 0
    for line in raw_in:
        split = re.split(r'\s+', line)
        # new day
        if split[0][0] == '#':
            try:
                day_data.append(pp_preprocess_radio_sounding_day(dt, hght, pres,
                            temp, dpd, wdir, wind))
            # on first day none exists
            except UnboundLocalError:
                pass
            dt = datetime(int(split[1]), int(split[2]), int(split[3]), int(split[4]))
            nlev    = int(split[6])
            pres    = np.zeros(nlev)
            hght    = np.zeros(nlev)
            temp    = np.zeros(nlev)
            dpd     = np.zeros(nlev)
            wdir    = np.zeros(nlev)
            wind    = np.zeros(nlev)
            i = 0
        # next level of same day
        else:
            pres[i] = float(line[line_inds['pres']])
            hght[i] = float(line[line_inds['hght']])
            temp[i] = float(line[line_inds['temp']])
            dpd[i]  = float(line[line_inds['dpd']])
            wdir[i] = float(line[line_inds['wdir']])
            wind[i] = float(line[line_inds['wind']])
            i += 1
    day_data.append(pp_preprocess_radio_sounding_day(dt, hght, pres, temp, dpd,
                                                    wdir, wind))
    #for dd in day_data:
    #    print(dd['date'])
    #print(len(day_data))
    #quit()

    # INTERPOLATE DATA ONTO CONSTANT Z GRID
    type_vnames = {'TEMP':['pres', 'temp', 'dpd'], 'WIND':['pres', 'wdir', 'wind']}
    type_vars = {}
    for type,vnames in type_vnames.items():
        type_vars[type] = {} 
        for vname in vnames:
            type_vars[type][vname] = np.zeros((n_lowest,len(day_data)))
    type_hgts = {'TEMP':np.zeros((n_lowest,len(day_data))),
                 'WIND':np.zeros((n_lowest,len(day_data)))}
    dates = []
    for dI in range(len(day_data)):
        #print(dI)
        dayd = day_data[dI]
        dates.append(dayd['date'])
        # get match between p and h on significant levels
        inds = np.argwhere(~np.isnan(dayd['hght']))
        p_ref = dayd['pres'][inds].squeeze()
        h_ref = dayd['hght'][inds].squeeze()
        # get values on all levels (also non-significant)
        type_inds = {}
        type_inds['TEMP'] = np.argwhere(~np.isnan(dayd['temp']) & ~np.isnan(dayd['dpd']))
        type_inds['WIND'] = np.argwhere(~np.isnan(dayd['wdir']) & ~np.isnan(dayd['wind']))
        for type in ['TEMP', 'WIND']:
            inds = type_inds[type]
            p = dayd['pres'][inds].squeeze()
            # logarithmically interpolate (and extrapolate) height 
            # for all levels based on significant levels
            interp = interp1d(np.log10(p_ref), np.log10(h_ref), fill_value='extrapolate')
            h = 10**interp(np.log10(p))
            # return n_lowest levels
            type_hgts[type][:,dI] = h[:n_lowest]
            for vkey in type_vars[type]:
                var = dayd[vkey][inds].squeeze()
                type_vars[type][vkey][:,dI] = var[:n_lowest]


    # compute specific var_names (user input) variables.
    if var_name == 'T':
        type = 'TEMP'
        var = type_vars[type]['temp']
    pvar = type_vars[type]['pres']

    # APPROXIMATION: average height for all days over time to get one
    # vertical grid for all days
    h_mean = np.mean(type_hgts[type], axis=1)

    var = np.expand_dims(np.expand_dims(var.T, 2),3)
    pvar = np.expand_dims(np.expand_dims(pvar.T, 2),3)
    var_xr = xr.DataArray(var, coords=[dates, h_mean,
                                np.asarray(lat), np.asarray(lon)],
                        dims=['time', 'alt', 'lat', 'lon'], name=var_name)
    p_xr = xr.DataArray(pvar, coords=[dates, h_mean,
                                np.asarray(lat), np.asarray(lon)],
                        dims=['time', 'alt', 'lat', 'lon'], name=var_name)
    sounding = xr.Dataset({var_name:var_xr, 'P':p_xr})

    #test = sounding.isel(time=0)
    #import matplotlib.pyplot as plt
    #plt.plot(test[var_name], test.alt)
    #plt.show()
    #quit()
    #quit()
    return(sounding)
