#!/usr/bin/python
# -*- coding: utf-8 -*-
#title			:preproc_functions.py
#description	:Collection of functions to preprocess data while loading
#               :in package.Variable
#author			:Christoph Heim
#date			:20190401
#version		:1.00
#usage			:run from package.Variable
#notes			:
#python_version	:3.7.7
#==============================================================================
#from namelist import update_steps, i_debug
from pathlib import Path
import os, glob, subprocess
from cdo import *
cdo = Cdo()

def check_kwargs(arg_names, kwargs):
    for arg_name in arg_names:
        if arg_name not in kwargs:
            raise ValueError(arg_name +' not given in kwargs.')



def run_ncks(member, var_info, out_path, **kwargs):
    """
    Run ncks command to make time timension a real dimensions (??).
    ---------------------------------------------------------------------------
    INPUT:
    member:     dict - namelist analysis member dict
    var_info:   dict - (see package.Variable)
    **kwargs
    inp_path:   str - path of input files up to and without cosmo output
                      variable group.
    ---------------------------------------------------------------------------
    OUTPUT:
    ---------------------------------------------------------------------------
    COMMENTS:
    """
    check_kwargs(['inp_path'], kwargs)

    if update_steps['ncks']:
        if i_debug:
            print('Run ncks for var ' + var_info['file'] + \
                    ' and member ' + member['label'] + '.')

        source_path = os.path.join(kwargs['inp_path'], var_info['dir_group'],
                        var_info['nc_prfx'][member['type']]+kwargs['file_sel'])
        for source_file in glob.glob(source_path):
            subprocess.call('ncks -O --mk_rec_dmn time ' + 
                    source_file + ' ' + source_file, shell=True)


def run_ncrcat(member, var_info, out_path, **kwargs):
    """
    If necessary, run ncrcat command to aggregate timewise cosmo output data
    to one file for one variable including all timesteps.
    package.NC_Field requires this format.
    ---------------------------------------------------------------------------
    INPUT:
    member:     dict - namelist analysis member dict
    var_info:   dict - (see package.Variable)
    **kwargs
    inp_path:   str - path of input files up to and without cosmo output
                      variable group.
    file_sel:   str - The part of single nc file selection containing the time
                labels. Will be appent to var_info['nc_prefix'].
    ---------------------------------------------------------------------------
    OUTPUT:
    ---------------------------------------------------------------------------
    COMMENTS:
    """
    check_kwargs(['inp_path', 'file_sel'], kwargs)

    source_path = os.path.join(kwargs['inp_path'], var_info['dir_group'],
                    var_info['nc_prfx'][member['type']]+kwargs['file_sel'])
    # if forced update, remove existing output
    if update_steps['ncrcat']:
        try:
            os.remove(out_path)
        except FileNotFoundError:
            pass
    if not os.path.exists(out_path):
        if i_debug:
            print('Run ncrcat for var ' + var_info['file'] + \
                    ' and member ' + member['label'] + '.')
        subprocess.call('ncrcat -v '+var_info['nc_key'][member['type']] + ' ' + 
                source_path + ' ' + out_path, shell=True)


def run_cdo_command(inp_file, command, out_file=None, **kwargs):
    """
    Run cdo commands.
    ---------------------------------------------------------------------------
    INPUT:
    inp_file:       str - Path to input file.
    command:        str - Cdo command to run.
    out_file:       str - Path to output file. If None, inp_file is replaced.
    **kwargs:       strings - parameters for cdo commands.
    ---------------------------------------------------------------------------
    OUTPUT:
    ---------------------------------------------------------------------------
    COMMENTS:
    """

    if inp_file == out_file:
        replace_file = True
    else:
        replace_file = False

    if replace_file:
        out_file = inp_file
        inp_file = inp_file[:-3] + '_tmp.nc'
        os.rename(out_file, inp_file)
    else:
        if out_file is None:
            raise ValueError('out_file not given but replace_file == False.')

    # run command
    if command == 'remapbil':
        check_kwargs(['griddes'], kwargs)
        cdo.remapbil(kwargs['griddes'], input=inp_file, output=out_file)
    elif command == 'mulc':
        check_kwargs(['const'], kwargs)
        cdo.mulc(kwargs['const'], input=inp_file, output=out_file)
    elif command == 'divc':
        check_kwargs(['const'], kwargs)
        cdo.divc(kwargs['const'], input=inp_file, output=out_file)
    else:
        raise NotImplementedError('Cdo Command not yet implemented.')

    if replace_file:
        os.remove(inp_file)





