#!/usr/bin/python
# -*- coding: utf-8 -*-
#title			:variable_namelist.py
#description	:namelist var_infos
#author			:Christoph Heim
#date			:20190401
#version		:1.00
#usage			:import these var_info dicts in scripts using the package.
#notes			:
#python_version	:3.7.7
#==============================================================================

# member groups
MOD = 'MOD'
MODMSC = 'MODMSC'
GERB = 'GERB'
SEVIRI_CERES = 'SEVIRI_CERES'


# ANALYSIS VARIABLES
# var_info dicts
VNL = {
'ATHB_T' : {
    'sh_name'       :'ATHB_T',
    'nc_key'        :{MOD:'ATHB_T', GERB:'rlut', SEVIRI_CERES:'Data1'},
    'label'         :'Flux',
    'title_label'   :'Outgoing LW at TOA',
    'unit'          :'[W/m²]',
    'nc_prefix'     :{MOD:'1h_rad_dy/', GERB:'TET', SEVIRI_CERES:'TET'},
},
'CLCL' : {
    'sh_name'       :'CLCL',
    'nc_key'        :{MOD:'CLCL'},
    'label'         :'Fraction',
    'title_label'   :'Cloud Cover',
    'unit'          :'',
    'nc_prefix'     :{MOD:'1h/'},
},
'CLCT' : {
    'sh_name'       :'CLCT',
    'nc_key'        :{MOD:'CLCT'},
    'label'         :'Fraction',
    'title_label'   :'Cloud Cover',
    'unit'          :'',
    'nc_prefix'     :{MOD:'1h_dy/'},
},
'TQC' : {
    'sh_name'       :'TQC',
    'nc_key'        :{MOD:'TQC'},
    'label'         :'Cloud Liquid Water Path',
    'title_label'   :'Cloud Liquid Water Path',
    'unit'          :'[kg/m²]',
    'nc_prefix'     :{MOD:'1h_vint_dy/'},
},








'V' : {
    'sh_name'       :'V',
    'nc_key'        :{MODMSC:'V'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'W' : {
    'sh_name'       :'W',
    'nc_key'        :{MODMSC:'W'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'T' : {
    'sh_name'       :'T',
    'nc_key'        :{MODMSC:'T'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'P' : {
    'sh_name'       :'P',
    'nc_key'        :{MODMSC:'P'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},







'QV' : {
    'sh_name'       :'QV',
    'nc_key'        :{MODMSC:'QV'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'QC' : {
    'sh_name'       :'QC',
    'nc_key'        :{MODMSC:'QC'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'QR' : {
    'sh_name'       :'QR',
    'nc_key'        :{MODMSC:'QR'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'QI' : {
    'sh_name'       :'QI',
    'nc_key'        :{MODMSC:'QI'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'QS' : {
    'sh_name'       :'QS',
    'nc_key'        :{MODMSC:'QS'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},
'QG' : {
    'sh_name'       :'QG',
    'nc_key'        :{MODMSC:'QG'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},




'AQVT_TOT' : {
    'sh_name'       :'AQVT_TOT',
    'nc_key'        :{MODMSC:'AQVT_TOT'},
    'label'         :'',
    'title_label'   :'',
    'unit'          :'',
    'nc_prefix'     :{MODMSC:'zlev/'},
},












############# CALCULATED FIELDS
'RHO' : {
    'sh_name'       :'RHO',
    'nc_key'        :{MODMSC:'RHO'},
    'label'         :'Air Density',
    'title_label'   :'Air Density',
    'unit'          :'[kg m-3]',
    'nc_prefix'     :{MODMSC:'calc/RHO/'},
},
'EQPOTT' : {
    'sh_name'       :'EQPOTT',
    'nc_key'        :{MODMSC:'EQPOTT'},
    'label'         :'Equivalent Potential Temperature',
    'title_label'   :'Equivalent Potential Temperature',
    'unit'          :'[K]',
    'nc_prefix'     :{MODMSC:'calc/EQPOTT/'},
},
'FQVZ' : {
    'sh_name'       :'FQVZ',
    'nc_key'        :{MODMSC:'FQVZ'},
    'label'         :'Resolved Vertical Water Vapor Flux',
    'title_label'   :'Resolved Vertical Water Vapor Flux',
    'unit'          :'[kg m-2 s-1]',
    'nc_prefix'     :{MODMSC:'calc/FQVZ/'},
},
'FQVY' : {
    'sh_name'       :'FQVY',
    'nc_key'        :{MODMSC:'FQVY'},
    'label'         :'Resolved Meridional Water Vapor Flux',
    'title_label'   :'Resolved Meridional Water Vapor Flux',
    'unit'          :'[kg m-2 s-1]',
    'nc_prefix'     :{MODMSC:'calc/FQVY/'},
},
'RH' : {
    'sh_name'       :'RH',
    'nc_key'        :{MODMSC:'RH'},
    'label'         :'Relative Humidity',
    'title_label'   :'Relative Humidity',
    'unit'          :'[%]',
    'nc_prefix'     :{MODMSC:'calc/RH/'},
},
'WVP_0_10' : {
    'sh_name'       :'WVP_0_10',
    'nc_key'        :{MODMSC:'WVP_0_10'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_0_10/'},
},
'WVP_0_4' : {
    'sh_name'       :'WVP_0_4',
    'nc_key'        :{MODMSC:'WVP_0_4'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_0_4/'},
},
'WVP_0_2' : {
    'sh_name'       :'WVP_0_2',
    'nc_key'        :{MODMSC:'WVP_0_2'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_0_2/'},
},
'WVP_2_4' : {
    'sh_name'       :'WVP_2_4',
    'nc_key'        :{MODMSC:'WVP_2_4'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_2_4/'},
},
'WVP_2_10' : {
    'sh_name'       :'WVP_2_10',
    'nc_key'        :{MODMSC:'WVP_2_10'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_2_10/'},
},
'WVP_4_10' : {
    'sh_name'       :'WVP_4_10',
    'nc_key'        :{MODMSC:'WVP_4_10'},
    'label'         :'Water Vapor Path',
    'title_label'   :'Water Vapor Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/WVP_4_10/'},
},

'LWP_0_10' : {
    'sh_name'       :'LWP_0_10',
    'nc_key'        :{MODMSC:'LWP_0_10'},
    'label'         :'Liquid Water Path',
    'title_label'   :'Liquid Water Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/LWP_0_10/'},
},
'LWP_0_2' : {
    'sh_name'       :'LWP_0_2',
    'nc_key'        :{MODMSC:'LWP_0_2'},
    'label'         :'Liquid Water Path',
    'title_label'   :'Liquid Water Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/LWP_0_2/'},
},
'LWP_2_4' : {
    'sh_name'       :'LWP_2_4',
    'nc_key'        :{MODMSC:'LWP_2_4'},
    'label'         :'Liquid Water Path',
    'title_label'   :'Liquid Water Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/LWP_2_4/'},
},
'LWP_4_10' : {
    'sh_name'       :'LWP_4_10',
    'nc_key'        :{MODMSC:'LWP_4_10'},
    'label'         :'Liquid Water Path',
    'title_label'   :'Liquid Water Path',
    'unit'          :'[kg m-2]',
    'nc_prefix'     :{MODMSC:'calc/LWP_4_10/'},
},












}
