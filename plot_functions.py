#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    Functions to automate plotting, drawing spatial maps.
                Class PlotOrganizer: Helps setting up and finalizing plots.
author			Christoph Heim
date created    20.04.2019
date changed    05.01.2021
usage			no args
"""
###############################################################################
import os, cartopy
import numpy as np
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from pathlib import Path
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from package.nl_variables import nlv
###############################################################################


class PlotOrganizer:

    def __init__(self, i_save_fig=0, path=None, name_dict=None, nlp=None,
                 geo_plot=False):
        """
        ARGS:
        i_save_fig: int - 0: show, 1: png, 2: pdf, 3: jpg
        path:       str - path to output directory (if i_save_fig > 0).
        name_dict:  OrderedDict - contains key/value pairs that should enter
                    the plot name. Examples:
                    simple name: name_dict = {'':'psurf'}
                                 --> 'path'/psurf.png
                    more complex: name_dict = {'':'p_surf', 'time':12}
                                 --> 'path'/psurf_time_12.png
        nlp:        dict - namelist with few plotting attributes.
                    If it is None, the default nlp setting will be used.
        geo_plot:   log - should the plot become a map with projection using
                    cartopy?
        """
        self.i_save_fig = i_save_fig
        self.path = path
        self.name_dict = name_dict
        if nlp is None:
            self.nlp = {}
        else:
            self.nlp = nlp
        self.geo_plot = geo_plot
        self.handles = []


        ## generate plot name and directory
        if (self.path is None) or (name_dict is None):
            raise ValueError('Plot output path or name_dict not given')
        else:
            plot_name = ''
            for key,value in name_dict.items():
                if key != '':
                    plot_name += str(key).replace(' ','_') + '_' + \
                                    str(value).replace(' ','_') + '_'
                else:
                    plot_name += str(value).replace(' ','_') + '_'
            plot_name = plot_name[:-1] 

            # create plotting directory if it does not exist
            Path(path).mkdir(parents=True, exist_ok=True)

            # join plotting directory and plot name
            self.path = os.path.join(path,plot_name)

        if i_save_fig == 0:
            pass
        elif i_save_fig == 1:
            self.path += '.png'
        elif i_save_fig == 2:
            self.path += '.pdf'
        elif i_save_fig == 3:
            self.path += '.jpg'
        else:
            raise ValueError()


    def plot_exists(self):
        """
        Check whether this plot already exists.
        OUT:
            True if plot exists, else False.
        """
        if os.path.exists(self.path):
            return(True)
        else:
            return(False)


    def initialize_plot(self, nrows=1, ncols=1, figsize=None):
        """
        Set up plot basic structure
        ARGS:
            nrows:       int - number of rows
            ncols:       int - number of columns
            figsize:    tuple - size of figure
        """
        if (nrows == 1) and (ncols == 1):
            if figsize is None:
                figsize=(12,12)
            self.fig = plt.figure(figsize=figsize)
            if self.geo_plot:
                self.axes = np.asarray(plt.axes(
                                projection=self.nlp['projection']))
            else:
                self.axes = np.asarray(plt.gca())
        else:
            self.nrows = nrows
            self.ncols = ncols
            if figsize is None:
                figsize=(ncols*6+2,nrows*6)
            if self.geo_plot:
                self.fig,self.axes = plt.subplots(
                            nrows,ncols, figsize=figsize,
                            subplot_kw={'projection':self.nlp['projection']})
            else:
                self.fig,self.axes = plt.subplots( nrows,ncols, figsize=figsize)

        # expand dimensions to always have a 2D axes array
        if nrows < 2:
            self.axes = np.expand_dims(self.axes,axis=0)
        if ncols < 2:
            self.axes = np.expand_dims(self.axes,axis=-1)

        return(self.fig, self.axes)


    def add_panel_labels(self, order='cols', start_ind=0):
        """
        Add alphabetic labels to each panel in plot.
        If order=='cols': a b c .. starts in column direction.
        If order=='rows': a b c .. starts in row direction.
        """
        panel_labels = ['a.', 'b.', 'c.', 'd.',
                        'e.', 'f.', 'g.', 'h.',
                        'i.', 'j.', 'k.', 'l.',
                        'm.', 'n.', 'o.', 'p.', 'q.',
                        'r.', 's.', 't.', 'u.',
                        'v.', 'w.', 'x.', 'y.', 'z.',
                        'aa.', 'ab.', 'ac.', 'ad.',
                        'ae.', 'af.', 'ag.', 'ah.',
                        'ai.', 'aj.', 'ak.', 'al.',]
        row_inds = range(self.axes.shape[0])
        col_inds = range(self.axes.shape[1])
        c = start_ind
        for i in row_inds:
            for j in col_inds:
                if order == 'cols':
                    ax = self.axes[i,j]
                elif order == 'rows':
                    ax = self.axes[j,i]
                # make panel label
                pan_lab_x = ax.get_xlim()[0] + (
                                ax.get_xlim()[1] - ax.get_xlim()[0]) * 0.00
                pan_lab_y = ax.get_ylim()[0] + (
                                ax.get_ylim()[1] - ax.get_ylim()[0]) * 1.06
                ax.text(pan_lab_x, pan_lab_y,
                                    panel_labels[c], weight='bold')
                c += 1

    #def format_axes(xlabel='', ylabel='', xticks=None, yticks=None,
    #                force_label=False):
    #    if col_ind > 0:
    #        ax.set_yticklabels([]) 
    #        ax.set_ylabel('')
    #    else:


    def set_axes_labels(self, ax, x_var_name, y_var_name):
        """
        Set axes labels for x and y axis of the given variables
        """
        unit = nlv[x_var_name]['units']
        unit = '' if unit == '' else '[{}]'.format(unit)
        xlabel = '{} {}'.format(nlv[x_var_name]['label'], unit)
        unit = nlv[y_var_name]['units']
        unit = '' if unit == '' else '[{}]'.format(unit)
        ylabel = '{} {}'.format(nlv[y_var_name]['label'], unit)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)


    def finalize_plot(self, fig=None):
        """
        Either show, save as png, save as pdf figure.
        ARGS:
            fig:    - matplotlib figure object
        """
        print('finish figure {}'.format(self.path))
        if fig is None:
            fig = self.fig

        if self.i_save_fig == 0:
            img_format = 'png'
            plt.show()
        elif self.i_save_fig == 1:
            img_format = 'png'
        elif self.i_save_fig == 2:
            img_format = 'pdf'
        elif self.i_save_fig == 3:
            img_format = 'jpeg'
        if self.i_save_fig > 0:
            if 'transparent_bg' in self.nlp:
                plt.savefig(self.path, format=img_format,
                            transparent=self.nlp['transparent_bg'])
            else:
                plt.savefig(self.path, format=img_format)
            plt.close(fig.number)
###############################################################################



def draw_map(ax, dom, nlp, add_xlabel=True, add_ylabel=True, dticks=10):
    """
    Draw a nice background map.
    ARGS:
        dom:            dict - domain dictionary from package.domains.py
        add_x/y_label:  log - Should x and y labels be added?
    """
    coastline_resolution = '50m' # '110m', '50m', '10m'

    ax.set_xticks(np.arange(-180,180,dticks), crs=nlp['projection'])
    ax.set_yticks(np.arange(-80,90,dticks), crs=nlp['projection'])
    lon_formatter = LongitudeFormatter(degree_symbol='°',
                                        dateline_direction_label=True)
    ax.xaxis.set_major_formatter(lon_formatter)
    lat_formatter = LatitudeFormatter(degree_symbol='°')
    ax.yaxis.set_major_formatter(lat_formatter)

    xlim = (dom['lon'].start-nlp['map_margin'][0],
            dom['lon'].stop +nlp['map_margin'][1])
    ylim = (dom['lat'].start-nlp['map_margin'][2],
            dom['lat'].stop +nlp['map_margin'][3])
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    if add_xlabel:
        ax.set_xlabel(nlv['COORD_LON']['label'])
    if add_ylabel:
        ax.set_ylabel(nlv['COORD_LAT']['label'])

    #ax.stock_img()
    ax.coastlines(resolution=coastline_resolution)
    ax.add_feature(cartopy.feature.LAND, color=nlp['land_color'])
    ax.add_feature(cartopy.feature.OCEAN, color=nlp['ocean_color'])
    ax.add_feature(cartopy.feature.RIVERS, edgecolor=nlp['river_color'])
    ax.stock_img()





def draw_domain(ax, dom, nlp, **kwargs):
    """
    Draw a rectangle onto Basemap plot.
    ARGS:
        dom:        dict - domain dictionary from package.domains.py
    OUT:
        handle:     Legend handle for this rectangle.
    """
    lons = [[dom['lon'].start, dom['lon'].stop ],
            [dom['lon'].start, dom['lon'].stop ],
            [dom['lon'].start, dom['lon'].start],
            [dom['lon'].stop , dom['lon'].stop ],
            ]
    lats = [[dom['lat'].start, dom['lat'].start],
            [dom['lat'].stop , dom['lat'].stop ],
            [dom['lat'].start, dom['lat'].stop ],
            [dom['lat'].start, dom['lat'].stop ],
            ]
    for i in range(len(lons)):
        handle, = ax.plot(lons[i], lats[i], transform=nlp['projection'],
                          label=dom['label'], **kwargs)

        #ax.plot(np.repeat(dom['lon'][i],2), dom['lat'],
        #            transform=nlp['projection'],
        #            color=color, linestyle=linestyle,
        #            linewidth=linewidth, zorder=zorder)

    return(handle)
