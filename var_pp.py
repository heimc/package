#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description     Preprocess (or derive) variables specific for different models
author			Christoph Heim
date created    25.11.2019
date changed    15.03.2021
usage           use from another script.
"""
###############################################################################
import copy, os
import numpy as np
import xarray as xr
from numba import jit, njit
import matplotlib.pyplot as plt
from package.constants import CON_M_PER_DEG
from package.nl_global import model_specifics_path
from package.model_pp import MODEL_PP, MODEL_PP_DONE
from package.utilities import Timer, select_common_timesteps, dt64_to_dt
from package.nl_variables import add_var_attributes
from package.external.lcl import lcl
###############################################################################

DIRECT = 'direct'
DERIVE = 'derive'

VAR_PP        = 'var_pp'
VAR_PP_DONE   = 'done'

CON_RE = 6371000

## "model key" for reference fields that should be used for
## computations of fields in other simulations
## (e.g. SWDTOA to compute SWUTOA from SWNDTOA in other fields)
#__REF = '__REFERENCE'

#####
direct_vars = [
    'QV', 'QC', 'T', 'W', 'PS',
    'SWDTOA', 'SWNDTOA', 'LWUTOA',
    'CLCT', 'TSURF',
]
equally_derived_vars = {
    'QVFT':             ['QV'],
    'QVHDIV':           ['QV', 'U', 'V'],
    'QVVDIV':           ['QV', 'W'],
    'UV':               ['U', 'V'],
    'UVFLXDIV':         ['U', 'V', 'RHO'],
    'UVFLXDIVNORMI':    ['UVFLXDIV', 'INVHGT'],
    'UNORMI':           ['U', 'INVHGT'],
    'VNORMI':           ['V', 'INVHGT'],
    'UVNORMI':          ['UV', 'INVHGT'],
    'WFLX':             ['W', 'RHO'],
    'WFLXI':            ['WFLX', 'INVHGT'],
    'WTURB':            ['W', 'WMEAN', 'INVHGT'],
    'WTURBNORMI':       ['WTURB', 'INVHGT'],
    'WTURBNORMISCI':    ['WTURBNORMI', 'INVHGT'],
    'WMEAN':            ['W'], # assumes tmean inputs
    'ENTR':             ['ENTRH', 'ENTRV'],
    #'ENTRV':            ['WI', 'RHOI'],
    #'ENTRH':            ['UI', 'VI', 'INVHGT', 'RHOI'],
    'ENTRV':            ['WI'],
    'ENTRH':            ['UI', 'VI', 'INVHGT'],
    'UI':               ['U', 'INVHGT'],
    'VI':               ['V', 'INVHGT'],
    'WI':               ['W', 'INVHGT'],
    'WNORMI':           ['W', 'INVHGT'],
    'WMBLI':            ['W', 'INVHGT'],
    'AW':               ['W'],
    'AWU':              ['W'],
    'AWD':              ['W'],
    'AWNORMI':          ['AW', 'INVHGT'],
    'AWUNORMI':         ['AWU', 'INVHGT'],
    'AWDNORMI':         ['AWD', 'INVHGT'],
    'AWMBLI':           ['AW', 'INVHGT'],
    'KEW':              ['W', 'RHO'],
    'KEWNORMI':         ['KEW', 'INVHGT'],
    'KEWMBLI':          ['KEW', 'INVHGT'],
    'TKE':              ['TKE', 'RHO'],
    'WFLXNORMI':        ['WFLX', 'INVHGT'],
    'QVNORMI':          ['QV', 'INVHGT'],
    'QVFLXZ':           ['QV', 'W', 'RHO'],
    'QCNORMI':          ['QC', 'INVHGT'],
    'TV':               ['T', 'QV'],
    'POTT':             ['T', 'P'],
    'POTTV':            ['POTT', 'QV'],
    'POTTVFT':          ['POTTV'],
    'POTTBL':           ['POTT'],
    'POTTFT':           ['POTT'],
    'POTTHDIV':         ['POTT', 'U', 'V'],
    'POTTHDIVMBLI':     ['POTTHDIV', 'INVHGT', 'RHO'],
    'POTTMBLI':         ['POTT', 'INVHGT', 'RHO'],
    'POTTVDIV':         ['POTT', 'W'],
    'POTTVDIVWPOS':     ['POTT', 'W'],
    'POTTVDIVWNEG':     ['POTT', 'W'],
    'LTS':              ['POTT'],
    'LCL':              ['RHL', 'T', 'P'],
    'DCLDBASELCL':      ['LCL', 'LCLDBASE'],
    'DCLDTOPINVHGT':    ['INVHGT', 'LCLDTOP'],
    'DINVHGTLCL':       ['INVHGT', 'LCL'],
    'PVAPSATL':         ['T'],
    'PVAP':             ['P', 'QV'],
    'RHL':              ['PVAP', 'PVAPSATL'],
    'RHLNORMI':         ['RHL', 'INVHGT'],
    'DIABH':            ['POTTHDIV', 'POTTVDIV'],
    'DIABHNORMI':       ['DIABH', 'INVHGT'],
    'POTTHDIVMEAN':     ['POTTHDIV'], # assumes tmean inputs
    'POTTVDIVMEAN':     ['POTTVDIV'], # assumes tmean inputs
    'POTTHDIVNORMI':    ['POTTHDIV', 'INVHGT'],
    'POTTVDIVNORMI':    ['POTTVDIV', 'INVHGT'],
    'POTTDIVMEAN':      ['POTTHDIV', 'POTTVDIV'], # assumes tmean inputs
    'POTTDIVMEANNORMI': ['POTTDIVMEAN'], # assumes tmean inputs
    'POTTHDIVMEANNORMI':['POTTHDIV'], # assumes tmean inputs
    'POTTVDIVMEANNORMI':['POTTVDIV'], # assumes tmean inputs
    'POTTHDIVTURB':     ['POTTHDIV', 'POTTHDIVMEAN'],
    'POTTVDIVTURB':     ['POTTVDIV', 'POTTVDIVMEAN'],
    'POTTDIVTURB':      ['DIABH', 'POTTDIVMEAN'],
    'POTTHDIVTURBNORMI':['POTTHDIVTURB', 'INVHGT'],
    'POTTVDIVTURBNORMI':['POTTVDIVTURB', 'INVHGT'],
    'POTTDIVTURBNORMI': ['POTTDIVTURB', 'INVHGT'],
    'DIABHMINV':        ['DIABH', 'INVHGT'],
    'TNORMI':           ['T', 'INVHGT'],
    'POTTNORMI':        ['POTT', 'INVHGT'],
    'PNORMI'   :        ['P', 'INVHGT'],
    'RHO':              ['TV', 'P'],
    'RHOI':             ['RHO', 'INVHGT'],
    'TKEMBLI':          ['TKE', 'INVHGT'],
    'SUBS':             ['W'],
    'UV10M' :           ['U10M', 'V10M'],
    'U10M_W' :          ['U10M'],
    'U10M_E' :          ['U10M'],
    'V10M_S' :          ['V10M'],
    'V10M_N' :          ['V10M'],
    'INVHGT':           ['TV'], 
    'NOINVF':           ['INVHGT'], 
    'LCLDMASK':         ['QC', 'INVHGT'], 
    'LCLDBASE':         ['LCLDMASK'], 
    'LCLDBASENORMI':    ['LCLDBASE', 'INVHGT'], 
    'LCLDTOP':          ['LCLDMASK'], 
    'LCLDTOPNORMI':     ['LCLDTOP', 'INVHGT'], 
    'LCLDDEPTH':        ['LCLDTOP', 'LCLDBASE'], 
    'INVSTR':           ['T', 'POTT', 'INVHGT'], 
    'INVSTRV':          ['TV', 'POTTV', 'INVHGT'], 
    'INVSTRA':          ['T', 'POTT', 'INVHGT'], 
    'ALBEDO':           ['SWUTOA', 'SWDTOA'],
    'SLHFLX':           ['SLHFLX'],
    'SSHFLX':           ['SSHFLX'],
    'TQC':              ['TQC'],
    'TQI':              ['TQI'],
    #'QVFLXZI':           ['INVHGT', 'QV', 'RHO', 'W'],
    'QVFLXZCB':         ['CLDBASE', 'QVFLXZ'],
    'SST':              ['TSURF'],
}
members = [
        # MODELS
        'COSMO', 'NICAM', 'SAM', 'ICON', 'UM', 'MPAS',
        'IFS' ,'GEOS', 'ARPEGE-NH', 'FV3',
        # OBSERVATIONS
        'SUOMI_NPP_VIIRS', 'RADIO_SOUNDING',
        'CM_SAF_METEOSAT', 'CM_SAF_MSG_AQUA_TERRA', 
        'CM_SAF_MSG',
        'ERA5']
# specifically dervied variables
var_mapping = {
    ###### MODELS
    'COSMO':{
        'U'     :       ['U', 'TQC'],
        'V'     :       ['V', 'TQC'],
        'P'     :       ['P'],
        'SWUTOA':       ['SWNDTOA', 'SWDTOA'],
        'PP'    :       ['PP'],
        'CORREFL':      ['TQC'],
        #'CLCL':     ['CLCL', 'CLCM', 'CLCH', 'TQI'],
        'CLCL':         ['CLCL'],
        'CLCL2':        ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'NICAM':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['P'],
        'SWUTOA':['SWUTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCL'], # not available
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'SAM':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['P'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCL'], # not available
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'ICON':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['P'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCT', 'TQI'], 
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'UM':{
        'U'     :['U', 'TQC'],
        'V'     :['V', 'TQC'],
        'P'     :['P'],
        'SWUTOA':['SWUTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCT', 'TQI'], 
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'MPAS':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['P'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PPGRID', 'PPCONV'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCT', 'TQI'],
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'IFS':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['QV', 'T', 'PS'],
        'H'     :['QV', 'T', 'PS'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PPGRID', 'PPCONV'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCL'],
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'GEOS':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['P'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCL'], # not available
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'ARPEGE-NH':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['QV', 'PS'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        #'CLCL':     ['CLCL', 'CLCT', 'TQI'], 
        'CLCL':     ['CLCL'], 
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'FV3':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['T'],
        'SWUTOA':['SWUTOA'],
        'PP'    :['PP'],
        'CORREFL':  ['TQC'],
        'CLCL':     ['CLCT', 'TQI'], 
        'CLCL2':    ['TQC'], 
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    ###### OBSERVATIONS
    'CM_SAF_MSG':{
        'SWUTOA':['SWUTOA'],
        'CLCL'  :['CLCL'],
        'CLCL2' :['CLCL2'],
    },
    'CM_SAF_MSG_AQUA_TERRA':{
        'SWUTOA':['SWUTOA'],
    },
    'CM_SAF_METEOSAT':{
        'SWUTOA':['SWUTOA'],
    },
    'CM_SAF_HTOVS':{
        'TQV'   :['TQV'],
    },
    'ERA5':{
        'U'     :['U'],
        'V'     :['V'],
        'P'     :['QV', 'PS'],
        #'H'     :['QV', 'T', 'PS'],
        'SWUTOA':['SWNDTOA','SWDTOA'],
        'PP'    :['PP'],
        'CLCL2':        ['TQC'], 
        'CORREFL':  ['TQC'],
        'TQV':          ['QV', 'RHO'],
        'TQVFT':        ['QV', 'RHO', 'INVHGT'],
    },
    'SUOMI_NPP_VIIRS':{
        'CORREFL':  ['CORREFL'],
    },
}
# add variables that do not need to be derived and can
# be loaded directly
for var_name in direct_vars:
    for mkey in members:
        if mkey not in var_mapping:
            var_mapping[mkey] = {}
        var_mapping[mkey][var_name] = [var_name]
# add variables that do have to be derived but based on
# the same input variables (equally_derived_vars)
for var_name,inp_vars in equally_derived_vars.items():
    for mkey in members:
        if mkey not in var_mapping:
            var_mapping[mkey] = {}
        var_mapping[mkey][var_name] = inp_vars


    


def compute_variable(var_name, mkey, inputs, domain=None):
    """
    Compute derived variable var_name based on data stored in inputs.
    ARGS:
        var_name:   name of variable (e.g. SWUTOA)
        mkey:       key of model to look up how to compute the variable.
        inputs:     dict -  contains name of input variables as keys and arrays
                            as values.
    OUT:
        out_var:    computed variable
    """
    if var_name == 'P':
        out_var = compute_P(mkey, inputs) 
    elif var_name in ['U','V']:
        out_var = compute_U_V(mkey, inputs, var_name) 
    elif var_name in ['UV']:
        out_var = compute_UV(mkey, inputs) 
    elif var_name in ['RHLNORMI', 'TNORMI', 'POTTNORMI', 'QVNORMI',
                      'QCNORMI',
                      'PNORMI', 'WNORMI', 'WFLXNORMI',
                      'AWNORMI', 'AWDNORMI', 'AWUNORMI',
                      'KEWNORMI',
                      'UNORMI', 'VNORMI', 'UVNORMI', 'UVFLXDIVNORMI',
                      'POTTVDIVNORMI', 'POTTHDIVNORMI',
                      'DIABHNORMI',
                      'POTTHDIVMEANNORMI','POTTVDIVMEANNORMI',
                      'POTTHDIVTURBNORMI','POTTVDIVTURBNORMI',
                      'POTTDIVMEANNORMI', 'POTTDIVTURBNORMI',
                      'LCLDBASENORMI', 'LCLDTOPNORMI',
                      'WTURBNORMI']:
        out_var = compute_VARNORMI(mkey, inputs, var_name)
    elif var_name in ['WTURBNORMISCI']:
        out_var = compute_VARSCI(mkey, inputs, var_name)
    elif var_name == 'ALT':
        out_var = compute_ALT(mkey, inputs) 
    elif var_name in ['TV', 'POTTV']:
        out_var = compute_virtual_temperature(mkey, inputs, var_name) 
    elif var_name == 'POTT':
        out_var = compute_POTT(mkey, inputs) 
    elif var_name in ['POTTBL',
                      'POTTVFT', 'POTTFT', 'QVFT']:
        if var_name == 'POTTBL': alt = 300; inp_var_name = 'POTT'
        elif var_name == 'POTTVFT': alt = 3000; inp_var_name = 'POTTV'
        elif var_name == 'POTTFT': alt = 3000; inp_var_name = 'POTT'
        elif var_name == 'QVFT': alt = 3000; inp_var_name = 'QV'
        out_var = compute_var_at_alt(mkey, inputs, inp_var_name, alt) 
    elif var_name == 'RHO':
        out_var = compute_RHO(mkey, inputs) 
    elif var_name == 'TKE':
        out_var = compute_TKE(mkey, inputs) 
    elif var_name in ['UVFLXDIV', 'POTTHDIV', 'POTTVDIV',
                      'QVHDIV', 'QVVDIV',
                      'POTTVDIVWPOS', 'POTTVDIVWNEG']:
        out_var = compute_DIV(mkey, inputs, var_name) 
    elif var_name in ['DIABH',
                      'POTTHDIVTURB', 'POTTVDIVTURB', 'POTTDIVTURB',
                      'POTTHDIVMEAN', 'POTTVDIVMEAN', 'POTTDIVMEAN']:
        out_var = compute_DIABH_POTTDIV(mkey, inputs, var_name) 
    elif var_name in ['AW', 'AWU', 'AWD']:
        out_var = compute_AUVW(mkey, inputs, var_name) 
    elif var_name in ['KEW']:
        out_var = compute_KE(mkey, inputs, var_name) 
    elif var_name in ['WFLX', 'QVFLXZ']:
        out_var = compute_FLX(mkey, inputs, var_name) 
    elif var_name in ['AWMBLI', 'KEWMBLI', 'WMBLI', 
                      'TKEMBLI', 'DIABHMINV', 'POTTHDIVMBLI',
                      'POTTMBLI']:
        out_var = compute_vertical_integ_or_mean(mkey, inputs, var_name) 
    elif var_name == 'SUBS':
        out_var = compute_SUBS(mkey, inputs)
    elif var_name == 'SWUTOA':
        out_var = compute_SWUTOA(mkey, inputs)
    elif var_name == 'ALBEDO':
        out_var = compute_ALBEDO(mkey, inputs)
    elif var_name == 'LWUTOA':
        out_var = compute_LWUTOA(mkey, inputs)
    elif var_name == 'UV10M':
        out_var = compute_UV10M(mkey, inputs)
    elif var_name in ['U10M_W', 'U10M_E', 'V10M_S', 'V10M_N']:
        out_var = compute_UV_domain_boundaries(mkey, inputs, var_name, domain)
    elif var_name == 'INVHGT':
        out_var = compute_INVHGT(mkey, inputs)
    elif var_name == 'LTS':
        out_var = compute_LTS(mkey, inputs)
    elif var_name == 'LCL':
        out_var = compute_LCL(mkey, inputs)
    elif var_name == 'TQI':
        out_var = compute_TQI(mkey, inputs)
    elif var_name == 'DCLDBASELCL':
        out_var = compute_level_diff(mkey, inputs, 'LCLDBASE', 'LCL')
    elif var_name == 'DCLDTOPINVHGT':
        out_var = compute_level_diff(mkey, inputs, 'LCLDTOP', 'INVHGT')
    elif var_name == 'DINVHGTLCL':
        out_var = compute_level_diff(mkey, inputs, 'INVHGT', 'LCL')
    elif var_name == 'PVAPSATL':
        out_var = compute_PVAPSATL(mkey, inputs)
    elif var_name == 'PVAP':
        out_var = compute_PVAP(mkey, inputs)
    elif var_name == 'RHL':
        out_var = compute_RHL(mkey, inputs)
    elif var_name == 'NOINVF':
        out_var = np.isnan(inputs['INVHGT'])
    elif var_name in ['LCLDMASK', 'LCLDBASE', 'LCLDTOP', 'LCLDDEPTH']:
        out_var = compute_CLD(mkey, inputs, var_name)
    #elif var_name in ['CLDHGT', 'CLDBASE', 'CLDTOP', 'CLDBASENORMI']:
    #    out_var = compute_CLDHGT(mkey, inputs, var_name)
    elif var_name in ['INVSTR', 'INVSTRV', 'INVSTRA']:
        out_var = compute_INVSTR(mkey, inputs, var_name)
    elif var_name in ['SLHFLX', 'SSHFLX']:
        out_var = compute_SHFLX(mkey, inputs, var_name)
    elif var_name == 'PP':
        out_var = compute_PP(mkey, inputs)
    elif var_name in ['CORREFL']:
        out_var = compute_CORREFL(mkey, inputs) 
    elif var_name in ['CLCL', 'CLCL2', 'CLCM', 'CLCH', 'CLCT']:
        out_var = compute_CLC(mkey, inputs, var_name) 
    elif var_name in ['TQV', 'TQVFT']:
        out_var = compute_TQV(mkey, inputs, var_name) 
    elif var_name in ['QVFLXZI','WFLXI', 'UI', 'VI', 'WI', 'RHOI']:
        out_var = compute_VARI(mkey, inputs, var_name) 
    elif var_name in ['ENTR', 'ENTRV', 'ENTRH']:
        out_var = compute_ENTR(mkey, inputs, var_name) 
    elif var_name in ['QVFLXZCB']:
        out_var = compute_VARCB(mkey, inputs, var_name) 
    elif var_name == 'SST':
        out_var = compute_SST(mkey, inputs) 
    elif var_name == 'WMEAN':
        out_var = compute_WMEAN(mkey, inputs) 
    elif var_name == 'WTURB':
        out_var = compute_variance(mkey, inputs, var_name) 
    elif ( (var_name in equally_derived_vars) or
           (var_name in direct_vars) ):
        #print(inputs.keys())
        out_var = inputs[var_name]
    else:
        raise NotImplementedError()

    # rename variable and add attributes
    #out_var = out_var.rename(var_name)
    out_var = add_var_attributes(out_var, var_name)

    # set "var_pp done" flag 
    out_var.attrs[VAR_PP] = VAR_PP_DONE

    # make sure MODEL_PP flag is set and set in derived variable
    model_pp_done = False
    for var_name,var in inputs.items():
        if MODEL_PP in var.attrs:
            if var.attrs[MODEL_PP] == MODEL_PP_DONE:
                model_pp_done = True
    if model_pp_done:
        out_var.attrs[MODEL_PP] = MODEL_PP_DONE
    else:
        out_var.attrs[MODEL_PP] = MODEL_PP_DONE
        #raise ValueError()

    return(out_var)



def compute_U_V(mkey, inputs, var_name):
    wind = inputs[var_name]
    # unstagger UM
    if mkey in ['COSMO', 'UM']:
        ref = inputs['TQC']
        out_var = wind.interp(lon=ref.lon, lat=ref.lat)
        #print(out_var.isel(lon=0).values)
        # quick fix for missing-value-at-boundary problem
        # (this probelm arised for U wind in COSMO_12)
        # simply take next staggered value
        #print(out_var.loc[{'lon':out_var.lon[0]}].values.shape)
        #print(wind.isel(lon=0).values.shape)
        #print(out_var.loc[{'lon':out_var.lon[0]}].lon.values)
        #print(wind.isel(lon=0).lon.values)
        if var_name == 'U':
            out_var.loc[{'lon':out_var.lon[0]}].values[:] = wind.isel(lon=0).values[:]
            out_var.loc[{'lon':out_var.lon[-1]}].values[:] = wind.isel(lon=-1).values[:]
        elif var_name == 'V':
            out_var.loc[{'lat':out_var.lat[0]}].values[:] = wind.isel(lat=0).values[:]
            out_var.loc[{'lat':out_var.lat[-1]}].values[:] = wind.isel(lat=-1).values[:]
    else:
        out_var = wind
    return(out_var)

def compute_UV(mkey, inputs):
    out_var = np.sqrt(inputs['U']**2 + inputs['V']**2)
    return(out_var)

#def compute_P(mkey, inputs):
#    if mkey in ['IFS', 'ARPEGE-NH']:
#        # hybrid pressure coordinates
#        # compute pressure at mid-levels based on hyam + hybm*PS
#        alt_inds = {'IFS':93, 'ARPEGE-NH':15}
#        hyam = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
#                        '{}_hyam.txt'.format(mkey)))[alt_inds[mkey]:],
#                        dims=('alt',),
#                        coords={'alt':inputs['QV'].alt.values})
#        hybm = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
#                        '{}_hybm.txt'.format(mkey)))[alt_inds[mkey]:],
#                        dims=('alt',),
#                        coords={'alt':inputs['QV'].alt.values})
#        P = inputs['PS'].load()
#        P = P.sel(time=inputs['QV'].time)
#        P = P.expand_dims(alt=inputs['QV'].alt, axis=1)
#        P = hyam + P*hybm
#        P = P.transpose('time', 'alt', 'lat', 'lon')
#        out_var = P
#    elif mkey == 'FV3':
#        # pressure coordinates
#        # take pressure from vertical coordinate (
#        P = inputs['T']
#        P.values[:] = 100. # hPa --> Pa
#        plev = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
#                        'FV3_pfull.txt')),
#                        dims=('alt',),
#                        coords={'alt':inputs['T'].alt.values})
#        P = P * plev
#        P = P.transpose('time', 'alt', 'lat', 'lon')
#        out_var = P
#    elif mkey in ['COSMO', 'NICAM', 'SAM', 'ICON', 'UM', 'MPAS', 'GEOS']:
#        out_var = inputs['P']
#        #if 'fmissing_value' in out_var.attrs:
#        #    del out_var.attrs['fmissing_value']
#    else:
#        raise NotImplementedError
#    out_var.attrs[MODEL_PP] = MODEL_PP_DONE
#    return(out_var)


#def compute_ALT(mkey, inputs):
#    if mkey in ['IFS', 'ERA5']:
#        # hybrid pressure coordinates
#        # compute pressure at interfaces based on hyai + hybi*PS
#        #alt_inds = {'IFS':93}
#        hyai = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
#                        '{}_hyai.txt'.format(mkey))),
#                        dims=('hlev',), coords={'hlev':range(138)})
#        hybi = xr.DataArray(np.loadtxt(os.path.join(model_specifics_path,
#                        '{}_hybi.txt'.format(mkey))),
#                        dims=('hlev',), coords={'hlev':range(138)})
#        PS = inputs['PS']
#        # indices to select hybi/hyai values
#        hlev_inds = np.arange(137, 137-len(inputs['QV'].lev.values)-1,-1)
#        # indices to select from model variables at full levels
#        lev_inds = np.arange(len(inputs['QV'].lev.values)-1, -1,-1)
#        # height at full levels (vertical mass points)
#        H_full = xr.full_like(inputs['QV'], fill_value=0.)
#        # height at half levels (vertical interfaces)
#        H_half = xr.full_like(inputs['PS'], fill_value=0.)
#        Rd = 287.06
#        # integrate upward
#        for k in range(len(lev_inds)):
#            # pressure at lower (altitudewise) half level
#            pbelow = hyai[hlev_inds[k  ]] + hybi[hlev_inds[k  ]] * PS
#            # pressure at upper (altitudewise) half level
#            pabove = hyai[hlev_inds[k+1]] + hybi[hlev_inds[k+1]] * PS
#            dlogp   = np.log(pbelow/pabove)
#            dp      = pbelow - pabove
#            alpha   = 1. - ( (pabove/dp) * dlogp )
#            # virtual temperature
#            Tv_lev = ( inputs['T'].isel(lev=lev_inds[k]) *
#                       (1. + 0.609133 * inputs['QV'].isel(lev=lev_inds[k])) )
#            # compute geopotential at this full level from half level below
#            H_full.loc[{'lev':H_full.lev.isel(lev=lev_inds[k]).values}] = (
#                    H_half + Tv_lev * Rd * alpha )
#            #print(H_full.isel(lev=lev_inds[k]).mean().values)
#            # compute geopotential at next half level
#            H_half = H_half + Tv_lev * Rd * dlogp
#
#        # convert geopotential to height
#        H_full /= 9.81
#        out_var = H_full
#        #print(H_full.mean(dim=['time', 'lon', 'lat']).values)
#    else:
#        raise NotImplementedError
#    return(out_var)

def compute_RHO(mkey, inputs):
    Rd = 287.1
    out_var = inputs['P']/(inputs['TV']*Rd)
    return(out_var)

def compute_TKE(mkey, inputs):
    out_var = inputs['TKE']*inputs['RHO']
    return(out_var)

def compute_DIV(mkey, inputs, var_name):
    if var_name == 'UVFLXDIV':
        u = inputs['U']
        v = inputs['V']
        rho = inputs['RHO']
        # compute fluxes
        # FV3 needs interpolation here because of slightly different alt
        # values
        if np.any(rho.alt.values != u.alt.values):
            u = u.interp(alt=rho.alt)
        if np.any(rho.alt.values != v.alt.values):
            v = v.interp(alt=rho.alt)
        uflx = u * rho
        vflx = v * rho
        # compute divergences  [m/s / degree]
        uflxdiv = uflx.differentiate(coord='lon')
        vflxdiv = vflx.differentiate(coord='lat')
        # divide by m/degree to convert to [1/s]
        uflxdiv = uflxdiv / (1/180*np.pi*CON_RE)
        vflxdiv = vflxdiv / (1/180*np.pi*CON_RE)
        # compute horizontal divergence in [1/s]
        out_var = uflxdiv + vflxdiv

    elif var_name in ['POTTHDIV', 'QVHDIV']:
        u = inputs['U']
        v = inputs['V']
        var = inputs[var_name[:-4]]
        # FV3 needs interpolation here because of slightly different alt
        # values
        if np.any(var.alt.values != u.alt.values):
            u = u.interp(alt=var.alt)
        if np.any(var.alt.values != v.alt.values):
            v = v.interp(alt=var.alt)
        # horizontal gradients of var [VAR/m]
        dvardx = var.differentiate(coord='lon') / (1/180*np.pi*CON_RE)
        dvardy = var.differentiate(coord='lat') / (1/180*np.pi*CON_RE)
        # horizontal divergence of var in [VAR/s]
        out_var = u * dvardx + v * dvardy

    elif var_name in ['POTTVDIV', 'QVVDIV']:
        var = inputs[var_name[:-4]]
        w = inputs['W']
        # if W is staggered, interpolate W onto full levels
        if np.any(var.alt.values != w.alt.values):
            w = w.interp(alt=var.alt)
        # make sure the two arrays have the same times
        w, var = select_common_timesteps(w, var)
        # compute vertical gradient of potential temperature [K/m]
        dvardz = var.differentiate(coord='alt')
        # vertical divergence of POTT in [K/s]
        out_var = w * dvardz

    #elif var_name in ['POTTVDIVWPOS', 'POTTVDIVWNEG']:
    #    var = inputs['POTT']
    #    w = inputs['W']
    #    # if W is staggered, interpolate W onto full levels
    #    if np.any(var.alt.values != w.alt.values):
    #        w = w.interp(alt=var.alt)
    #    # select only updrafts or downdrafts
    #    if var_name == 'POTTVDIVWPOS':
    #        w = w.where(w > 0, np.nan)
    #    elif var_name == 'POTTVDIVWPOS':
    #        w = w.where(w < 0, np.nan)
    #    # make sure the two arrays have the same times
    #    w, var = select_common_timesteps(w, var)
    #    # compute vertical gradient of potential temperature [K/m]
    #    dvardz = var.differentiate(coord='alt')
    #    # vertical divergence of POTT in [K/s]
    #    out_var = w * dvardz

    else: raise NotImplementedError()
    return(out_var)

def compute_DIABH_POTTDIV(mkey, inputs, var_name):
    if var_name in ['DIABH', 'POTTDIVMEAN']:
        # in case of POTTDIVMEAN this assumes that POTTHDIV is averaged already.
        out_var = inputs['POTTHDIV'] + inputs['POTTVDIV']
    elif var_name == 'POTTHDIVMEAN':
        # this assumes that POTTHDIV is averaged already.
        out_var = inputs['POTTHDIV']
    elif var_name == 'POTTVDIVMEAN':
        # this assumes that POTTVDIV is averaged already.
        out_var = inputs['POTTVDIV']
    elif var_name == 'POTTHDIVTURB':
        out_var =  inputs['POTTHDIV'] - inputs['POTTHDIVMEAN']
    elif var_name == 'POTTVDIVTURB':
        out_var =  inputs['POTTVDIV'] - inputs['POTTVDIVMEAN']
    elif var_name == 'POTTDIVTURB':
        out_var = inputs['DIABH'] - inputs['POTTDIVMEAN']
    else: raise NotImplementedError(var_name)
    return(out_var)


def compute_virtual_temperature(mkey, inputs, var_name):
    """
    Compute the virtual (potential) temperature using T and QV as input.
    """
    if var_name == 'TV': inp_var_name = 'T'
    elif var_name == 'POTTV': inp_var_name = 'POTT'
    else: raise NotImplemtedError()
    out_var = inputs[inp_var_name] * (1. + 0.609133 * inputs['QV'])
    return(out_var)

def compute_POTT(mkey, inputs):
    Rd = 287.1
    cp = 1005.
    T = inputs['T']
    p = inputs['P']
    if np.any(p.alt.values != T.alt.values):
        # if differences are only due to differences in numeric precision
        # simply replace alt values
        if np.mean(np.abs(p.alt.values - T.alt.values)) < 1E-3:
            p.alt.values = T.alt.values
        # if differences are real, run interpolation
        else:
            p = p.interp(alt=T.alt)
    # compute potential temperature
    out_var = T*(100000/p)**(Rd/cp)
    return(out_var)


def compute_AUVW(mkey, inputs, var_name):
    if var_name == 'AW':
        out_var = np.abs(inputs['W'])
    elif var_name == 'AWU':
        out_var = inputs['W'].where(inputs['W'] > 0, np.nan)
    elif var_name == 'AWD':
        out_var = inputs['W'].where(inputs['W'] < 0, np.nan)
    else:
        raise NotImplementedError()
    return(out_var)

def compute_KE(mkey, inputs, var_name):
    if var_name == 'KEW':
        out_var = inputs['W']**2
    rho = inputs['RHO']
    # if W is staggered, interpolate onto full levels
    if np.any(out_var.alt.values != rho.alt.values):
        out_var = out_var.interp(alt=rho.alt)
    # make sure the two arrays have the same times
    rho, out_var = select_common_timesteps(rho, out_var)
    # compute TKE per unit volume
    out_var *= 0.5 * rho
    return(out_var)

def compute_FLX(mkey, inputs, var_name):
    if var_name in ['WFLX', 'QVFLXZ']:
        out_var = inputs['W']
    rho = inputs['RHO']
    # if W is staggered, interpolate RHO onto full levels
    if np.any(out_var.alt.values != rho.alt.values):
        rho = rho.interp(alt=out_var.alt)
    # make sure the two arrays have the same times
    rho, out_var = select_common_timesteps(rho, out_var)
    # compute flux FLX in kg/m^2/s
    out_var *= rho
    if var_name == 'QVFLXZ':
        qv = inputs['QV']
        # if WFLX is staggered, interpolate QV onto half levels
        if np.any(out_var.alt.values != qv.alt.values):
            qv = qv.interp(alt=out_var.alt)
        # make sure the two arrays have the same times
        qv, out_var = select_common_timesteps(qv, out_var)
        # compute QVFLXZ in kg/m^2/s
        out_var *= qv
        #print(out_var)
        #quit()
    return(out_var)


def compute_SUBS(mkey, inputs):
    # 1.* removes a warning of serialization issue while saving to netCDF file.
    out_var = 1.*inputs['W'].sel(alt=3000, method='nearest')
    return(out_var)


@njit()
def interp_vprof_no_time(orig_array, src_vprof_vals_array,
                targ_vprof_vals, interp_array,
                nlat, nlon):
    """
    Helper function for compute_VARNORMI. Speedup of ~100 time
    compared to pure python code!
    """
    for lat_ind in range(nlat):
        for lon_ind in range(nlon):
            qv_col = orig_array[:, lat_ind, lon_ind]
            rel_alts_col = src_vprof_vals_array[:, lat_ind, lon_ind]
            interp_col = np.interp(targ_vprof_vals, rel_alts_col, qv_col)
            interp_array[:, lat_ind, lon_ind] = interp_col
    return(interp_array)

@njit()
def interp_vprof_with_time(orig_array, src_vprof_vals_array,
                targ_vprof_vals, interp_array,
                ntime, nlat, nlon):
    """
    Helper function for compute_VARNORMI. Speedup of ~100 time
    compared to pure python code!
    """
    for time_ind in range(ntime):
        for lat_ind in range(nlat):
            for lon_ind in range(nlon):
                qv_col = orig_array[time_ind, :, lat_ind, lon_ind]
                rel_alts_col = src_vprof_vals_array[time_ind, :, lat_ind, lon_ind]
                interp_col = np.interp(targ_vprof_vals, rel_alts_col, qv_col)
                interp_array[time_ind, :, lat_ind, lon_ind] = interp_col
    return(interp_array)

def compute_VARNORMI(mkey, inputs, var_name):
    #interp_rel_alts = np.arange(0.1, 3.1, 0.1)
    interp_rel_alts = np.asarray( 
                      [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95,
                       1.0, 1.05, 1.1, 1.2, 1.3, 1.4, 1.5, 1.75, 2.0, 2.5, 3.0])
    sel_alt = 6000
    raw_var_name = var_name[:-5]
    out_var = inputs[raw_var_name]

    # if alt is not in the coordinates it means that
    # the field itself is the altitude value (e.g. LCLDBASE)
    # simply divide by local inversion height
    if 'alt' not in inputs[raw_var_name].coords:
        out_var /= inputs['INVHGT']

    # alt is part of coordinates which means that the field is 3D (at least)
    # and its coordinate should be transformed to relative height.
    else:


        # subselect altitude
        var = subsel_alt(inputs[raw_var_name], mkey, slice(0,sel_alt))
        invhgt = inputs['INVHGT']
        var, invhgt = select_common_timesteps(var, invhgt)

        # vertical axis should be with ascending altitude. else sort
        if var.alt[-1] < var.alt[0]:
            #print('sort')
            var = var.sortby('alt', ascending=True)

        # time is part of the data set
        if 'time' in var.dims:
            if ( (list(var.dims) != ['time', 'alt', 'lat', 'lon']) or
                 (list(invhgt.dims) != ['time', 'lat', 'lon']) ):
                print(invhgt)
                quit()
                raise ValueError()
                # transpose to make sure to have same order of dimensions 
                # irrespective of model
                #var = var.transpose('time', 'alt', 'lat', 'lon')
                #invhgt = invhgt.transpose('time', 'lat', 'lon')

            # create array of altitudes (the same in every column)
            alts = var.alt
            alts = alts.expand_dims(lat=var.lat, axis=1)
            alts = alts.expand_dims(lon=var.lon, axis=2)
            alts = alts.expand_dims(time=var.time, axis=0)
            # create array for interpolated var values
            interp_var = xr.full_like(invhgt, np.nan).expand_dims(
                            rel_alt=interp_rel_alts, axis=1)
            interp_var_array = np.full((alts.shape[0], len(interp_rel_alts),
                                       alts.shape[2], alts.shape[3]),
                                      np.nan)
            # convert altitude array to relative altitude (with respect to invhgt)
            rel_alts = alts / invhgt
            # interpolate abs heights to relative heights along (time), lon and lat.
            interp_var.values = interp_vprof_with_time(var.values,
                            rel_alts.values,
                            interp_rel_alts, interp_var_array,
                            len(var.time.values), len(var.lat.values),
                            len(var.lon.values))
        # time is not part of the data set
        else:
            if ( (list(var.dims) != ['alt', 'lat', 'lon']) or
                 (list(invhgt.dims) != ['lat', 'lon']) ):
                raise ValueError()


            # create array of altitudes (the same in every column)
            alts = var.alt
            alts = alts.expand_dims(lat=var.lat, axis=1)
            alts = alts.expand_dims(lon=var.lon, axis=2)
            # create array for interpolated var values
            interp_var = xr.full_like(invhgt, np.nan).expand_dims(
                            rel_alt=interp_rel_alts, axis=0)
            interp_var_array = np.full((len(interp_rel_alts),
                                       alts.shape[1], alts.shape[2]),
                                      np.nan)

            # convert altitude array to relative altitude (with respect to invhgt)
            rel_alts = alts / invhgt
            #print(rel_alts.shape)
            #print(interp_var.shape)
            #print(interp_var_array.shape)
            #print(invhgt.shape)
            #print(var.shape)
            #quit()
            # interpolate abs heights to relative heights along (time), lon and lat.
            interp_var.values = interp_vprof_no_time(var.values,
                            rel_alts.values,
                            interp_rel_alts, interp_var_array,
                            len(var.lat.values),
                            len(var.lon.values))
        out_var = interp_var
    return(out_var)

def compute_VARSCI(mkey, inputs, var_name):
    # scale with inversion height and convert to per hours
    if var_name == 'WTURBNORMISCI':
        out_var = inputs['WTURBNORMI'] / inputs['INVHGT']**2. * 3600**2
    return(out_var)
    

def compute_SWUTOA(mkey, inputs):
    # more or equally frequent than hourly output
    if mkey in ['COSMO', 'SAM', 'ICON', 'MPAS', 'IFS',
               'GEOS', 'ARPEGE-NH', 'ERA5']:
        out_var = inputs['SWDTOA'] - inputs['SWNDTOA']
        out_var = out_var.resample(time='1h').mean(
                                        dim='time', skipna=False)
    # more or equally frequent than hourly output
    elif mkey in ['FV3', 'NICAM', 'UM',
                  'CM_SAF_MSG_AQUA_TERRA', 'CM_SAF_METEOSAT']:
        out_var = inputs['SWUTOA'].resample(time='1h').mean(
                                        dim='time', skipna=False)
    else:
        raise NotImplementedError()
    ## THIS DOES NOT WORK FOR UM..
    ## take orig time dim to retain time encoding
    #vkey = 'SWNDTOA' if 'SWNDTOA' in inputs else 'SWUTOA'
    #inputs[vkey], out_var = select_common_timesteps(inputs[vkey], out_var)
    #out_var['time'] = inputs[vkey].time
    return(out_var)

def compute_ALBEDO(mkey, inputs):
    # aggregate to daily means because SWDTOA (which may come from COSMO)
    # and SWNDTOA do not necessarily have the same time resolution.
    date = np.datetime64(dt64_to_dt(inputs['SWUTOA'].time.isel(time=0)).date())
    inputs['SWUTOA'] = inputs['SWUTOA'].mean(dim='time')
    inputs['SWUTOA'] = inputs['SWUTOA'].expand_dims({'time':[date]})
    inputs['SWDTOA'] = inputs['SWDTOA'].mean(dim='time')
    inputs['SWDTOA'] = inputs['SWDTOA'].expand_dims({'time':[date]})

    #inputs['SWUTOA'] = inputs['SWUTOA'].resample(time='1h').mean(
    #                                dim='time', skipna=False)
    #inputs['SWDTOA'] = inputs['SWDTOA'].resample(time='1h').mean(
    #                                dim='time', skipna=False)

    out_var = inputs['SWUTOA'] / inputs['SWDTOA']
    return(out_var)

def compute_LWUTOA(mkey, inputs):
    out_var = inputs['LWUTOA']
    if mkey in ['COSMO', 'ICON', 'ARPEGE-NH', 'ERA5']:
        out_var *= -1
        out_var = out_var.resample(time='1h').mean(
                                        dim='time', skipna=False)
    elif mkey in ['NICAM', 'SAM', 'UM', 'MPAS', 'IFS', 'FV3']:
        out_var = out_var.resample(time='1h').mean(
                                        dim='time', skipna=False)
    elif mkey in ['GEOS', 'CM_SAF_MSG_AQUA_TERRA', 'CM_SAF_METEOSAT']:
        pass
    else:
        raise ValueError()
    # pass along attributes
    out_var.attrs = inputs['LWUTOA'].attrs
    ## take orig time dim to retain time encoding
    #out_var, inputs['LWUTOA'] = select_common_timesteps(
    #                                out_var, inputs['LWUTOA'])
    #out_var['time'] = inputs['LWUTOA'].time
    return(out_var)

def compute_UV10M(mkey, inputs):
    out_var = np.sqrt(inputs['U10M']**2 + inputs['V10M']**2)
    return(out_var)

def compute_UV_domain_boundaries(mkey, inputs, var_name, domain):
    if var_name == 'V10M_S':
        out_var = inputs['V10M'].sel(lat=domain['lat'].start, method='nearest')
    elif var_name == 'V10M_N':
        out_var = inputs['V10M'].sel(lat=domain['lat'].stop, method='nearest')
    elif var_name == 'U10M_W':
        out_var = inputs['U10M'].sel(lon=domain['lon'].start, method='nearest')
    elif var_name == 'U10M_E':
        out_var = inputs['U10M'].sel(lon=domain['lon'].stop, method='nearest')
        #print(out_var.shape)
    else: raise NotImplementedError
    return(out_var)


    
def compute_INVHGT(mkey, inputs):
    """
    compute inversion height
    works irrespective of the orientation of the vertical model grid.
    This function sould be made more efficient..
    """
    temp = inputs['TV']
    #print(temp.time)
    # make sure tropopause inversion is not in dataset
    temp = subsel_alt(temp, mkey, slice(0,5000))
    #temp = subsel_alt(temp, mkey, slice(0,3000))
    height = temp['alt'].values

    temp = temp.differentiate(coord='alt')
    #temp = temp.transpose('time', 'alt', 'lat', 'lon')

    # set (weak or) no inversions below threshold to very negative value
    # argmax will definitely not choose it unless all are -99999.
    temp.values[temp.values < 0.000] = -99999.
    max_ind = temp.argmax(dim='alt')
    # set those places where the inversion is at the surface
    # (and thus inexistent) to negative values such that aux_compute_heights
    # will set it to np.nan.
    #max_ind[max_ind == 0] = -1
    max_ind = max_ind.where(max_ind > 0, -1)
    if np.sum(max_ind < 0):
        ngp = 1
        for dim in np.shape(max_ind): ngp *= dim
        print('{}: found {:06.3f}% of grid points without inversion.'.format(
                        mkey, np.sum(max_ind == -1).values/ngp*100))

    inv_hgt = np.zeros_like(max_ind).astype(np.float)
    #aux_compute_heights(max_ind, height, inv_hgt)
    aux_vind_to_height(max_ind.values, height, inv_hgt,
                        len(max_ind.time.values),
                        len(max_ind.lat.values),
                        len(max_ind.lon.values),
                        mode='centred')
    out_var = temp.mean(dim='alt')
    out_var.values = inv_hgt 
    return(out_var)

def compute_LTS(mkey, inputs):
    """
    compute lower-tropospheric stability index
    """
    pott_surf = inputs['POTT'].sel(alt=0, method="nearest")
    pott_700hPa = inputs['POTT'].sel(alt=3200, method="nearest")
    # compute lts index
    out_var = (pott_700hPa - pott_surf)
    # convert to lapser rate [K/100m]
    out_var /= (pott_700hPa.alt - pott_surf.alt) / 100
    return(out_var)

def compute_LCL(mkey, inputs):
    """
    compute lifting condensation level.
    """
    # make sure vertical dimension is increasing
    if inputs['P'].alt.values[-1] < inputs['P'].alt.values[0]:
        raise ValueError('Vertical dimension is not increasing')
    # select lowest vertical level
    P = inputs['P'].isel(alt=0)
    T = inputs['T'].isel(alt=0)
    RHL = inputs['RHL'].isel(alt=0)
    # TODO:
    # if lowest model level is nan (e.g. in orography),
    # currently the lcl values for these grid points are set to nan
    # as a quick "fix". However it would be better to start with
    # the computation of lcl at the first model level that is not nan.
    #
    P, T = select_common_timesteps(P, T)
    P, RHL = select_common_timesteps(P, RHL)
    P, T = select_common_timesteps(P, T)
    # compute LCL
    out_var = xr.zeros_like(P)
    out_var_arr = lcl(P.values,
                  T.values,
                  RHL.values)
    out_var.values = out_var_arr
    # add altitude of lowest model level because LCL is computed
    # relative to lowest model level
    out_var += RHL.alt.values
    # set LCL values below 0 (->fog) to zero.
    out_var = out_var.where(out_var > 0, 0)
    #
    # set LCL values to nan if RHL/T/P values are nan (within orography)
    # TODO: this should be done nicer. See TODO above.
    out_var = out_var.where(~np.isnan(RHL), np.nan)
    return(out_var)

def compute_TQI(mkey, inputs):
    """
    compute vertically integrated ice.
    """
    out_var = inputs['TQI']
    if mkey == 'CM_SAF_MSG':
        out_var = out_var.where(~np.isnan(out_var), 0)
    return(out_var)

def compute_var_at_alt(mkey, inputs, var_name, alt):
    """
    compute variable value at specific alt.
    """
    out_var = inputs[var_name].interp(alt=alt)
    return(out_var)

def compute_level_diff(mkey, inputs, var_name_ref, var_name_sub):
    """
    compute difference between var_name_sub and var_name_ref.
    Thus var_name_ref - var_name_sub
    """
    out_var = inputs[var_name_ref] - inputs[var_name_sub]
    return(out_var)

def compute_PVAP(mkey, inputs):
    """
    Compute vapor pressure
    https://cran.r-project.org/web/packages/humidity/vignettes/humidity-measures.html
    """
    eps = 0.622 # ratio of molecular mass between water and dry air
    out_var = inputs['QV'] * inputs['P'] / (eps + 0.378 * inputs['QV'])
    return(out_var)

def compute_PVAPSATL(mkey, inputs):
    """
    The saturation vapor pressure over liquid water (after package.external.lcl)
    """
    Ttrip = 273.16     # K
    ptrip = 611.65     # Pa
    E0v   = 2.3740e6   # J/kg
    cvv   = 1418       # J/kg/K 
    cvl   = 4119       # J/kg/K 
    rgasv = 461        # J/kg/K 
    cpv   = cvv + rgasv
    T = inputs['T']
    return ptrip * (T/Ttrip)**((cpv-cvl)/rgasv) * \
        np.exp( (E0v - (cvv-cvl)*Ttrip) / rgasv * (1/Ttrip - 1/T) )

def compute_RHL(mkey, inputs):
    """
    Relative humidity with respect to liquid water (0-1).
    """
    return( inputs['PVAP'] / inputs['PVAPSATL'] ) 

def compute_CLD(mkey, inputs, var_name):
    """
    compute 3D cloud properties based on 3D QC field:
        - LCLDMASK either 0 or 1 depending if QC > cloud_qc_thresh &
          cloud is rooted in MBL (below INVHGT)
        - height of low cloud base (LCLDBASE)
        - height of low cloud top (LCLDTOP)
        - depth of low cloud layer (LCLDDEPTH = LCLDTOP - LCLDBASE)
    works irrespective of the orientation of the vertical model grid.
    """
    cloud_qc_thresh = 1E-5
    if var_name == 'LCLDMASK':
        qc = inputs['QC']
        qc = subsel_alt(qc, mkey, slice(0,4000))
        height = qc['alt'].values
        # reverse altitude if necessary (for pressure-based models)
        if height[-1] < height[0]:
            qc = qc.sortby('alt', ascending=True)
        out_var = xr.where(qc > cloud_qc_thresh, 1, 0)
        #print(np.sum(out_var))
        aux_filter_MBL_base(out_var.values, out_var.alt.values,
                            inputs['INVHGT'].values,
                            len(out_var.time.values), len(out_var.alt.values),
                            len(out_var.lat.values), len(out_var.lon.values))
        #print(np.sum(out_var))
        #print()

    elif var_name in ['LCLDBASE', 'LCLDTOP']:
        cldmask = inputs['LCLDMASK']
        cldmask_2D = cldmask.sum(dim='alt') > 0
        height = cldmask['alt'].values
        if var_name == 'LCLDBASE':
            # find altitude of cloud base
            vind = cldmask.argmax(dim='alt')
        elif var_name == 'LCLDTOP':
            # vertical axis should be with DESCENDING altitude.k
            if height[-1] > height[0]:
                cldmask = cldmask.sortby('alt', ascending=False)
                height = cldmask['alt'].values
            # find altitude of cloud top
            vind = cldmask.argmax(dim='alt')

        # mask grid points that do not have clouds
        # (mask means set to negative because function
        # aux_compute_heights will assume colums with negatives are masked)
        vind = vind.where(cldmask_2D, -1)
        vind_height = xr.zeros_like(vind, dtype=np.float)
        aux_vind_to_height(vind.values, height, vind_height.values,
                            len(vind.time.values), len(vind.lat.values),
                            len(vind.lon.values),
                            mode='below')
        #print(vind_height)
        out_var = vind_height

    elif var_name in ['LCLDDEPTH']:
        out_var = inputs['LCLDTOP'] - inputs['LCLDBASE']


    return(out_var)

def compute_CLDHGT(mkey, inputs, var_name):
    """
    compute:
        - height of maximum cloud liquid water content (CLDHGT)
        - height of cloud base (CLDBASE)
        - height of cloud top (CLDTOP)
        - the above variables normalised by inversion height (...NORMI)
    works irrespective of the orientation of the vertical model grid.
    """
    qc = inputs['QC']
    qc = subsel_alt(qc, mkey, slice(0,4000))
    height = qc['alt'].values
    # if alt is reversed (starting from the top)
    # flip dataset. This is computing intensive
    # but the cloud base is selected according to first cloud level.
    # if dim is reversed, the cloud top is selected


    ## TODO very bad fix for COSMO_2.2 which contains missing values
    #if (mkey == 'COSMO') and (np.sum(np.isnan(qc)) > 0):
    #    qc.values[:,:,-1] = qc.values[:,:,-2]

    # generate cloud mask
    binary_cloud = xr.where(qc > 1E-5, 1, 0)
    cloud_mask = binary_cloud.sum(dim='alt') > 0
    if var_name == 'CLDHGT':
        # find altitude of max qc
        vind = qc.argmax(dim='alt')
    elif var_name in ['CLDBASE', 'CLDBASENORMI']:
        # vertical axis should be with ASCENDING altitude. else sort
        if height[-1] < height[0]:
            qc = qc.sortby('alt', ascending=True)
            height = qc['alt'].values
        # find altitude of cloud base
        qc = xr.where(qc > 1E-5, 1, 0)
        vind = qc.argmax(dim='alt')
    elif var_name in ['CLDTOP', 'CLDTOPNORMI']:
        # vertical axis should be with DESCENDING altitude. else sort
        if height[-1] > height[0]:
            qc = qc.sortby('alt', ascending=False)
            height = qc['alt'].values
        # find altitude of cloud top
        qc = xr.where(qc > 1E-5, 1, 0)
        vind = qc.argmax(dim='alt')
    else:
        raise NotImplementedError
    # mask grid points that do not have clouds
    # (mask means set to zero because function
    # aux_compute_heights will assume colums with zeroj are masked) 
    vind = vind.where(cloud_mask, 0)
    vind_height = xr.zeros_like(vind, dtype=np.float)
    aux_compute_heights(vind, height, vind_height.values)
    #plt.contourf(vind_height.isel(time=7))
    #plt.colorbar()
    #plt.show()
    #quit()
    out_var = vind_height

    # if necessary, normalise by inversion height
    if var_name == 'CLDBASENORMI':
        # make sure invhgt and out_var have same time steps
        out_var, invhgt = select_common_timesteps(out_var, inputs['INVHGT'])
        out_var /= inputs['INVHGT']
        # remove thos with rel_alt > 1.0 because
        # they are not rooted in the MBL
        out_var = out_var.where(out_var <= 1.0)
    return(out_var)

def compute_INVSTR(mkey, inputs, var_name):
    """
    compute absolute or average inversion strength
    either including (INVSTRV) or excluding (INVSTR) QV effect (virtual temperature)
    var_names to compute:
    INVSTR & INVSTRA
    """
    # select virtual temperatures or normal temperatures
    # depending on how it should be computed.
    if var_name in ['INVSTR', 'INVSTRA']:
        T = inputs['T']
        POTT = inputs['POTT']
    elif var_name in ['INVSTRV', 'INVSTRVA']:
        T = inputs['TV']
        POTT = inputs['POTTV']
    else: raise NotImplementedError()
        
    T.load()
    POTT.load()
    # vertical axis should be with ascending altitude.
    if T.alt.values[-1] < T.alt.values[0]:
        raise ValueError()
        #T = T.sortby('alt', ascending=True)
    if POTT.alt.values[-1] < POTT.alt.values[0]:
        raise ValueError()
        #POTT = POTT.sortby('alt', ascending=True)
    T_BL = T.where(T.alt <= inputs['INVHGT'], 1000)
    T_BL = T_BL.where(~np.isnan(inputs['INVHGT']), 1000)
    T_FT = T.where(T.alt >  inputs['INVHGT'], 0)

    ind_T_max_FT = T_FT.argmax(dim='alt')
    ind_T_min_BL = T_BL.argmin(dim='alt')
    alt_T_min_BL = T_BL.alt.isel(alt=ind_T_min_BL)
    alt_T_max_FT = T_FT.alt.isel(alt=ind_T_max_FT)

    #T_min_BL = T_BL.isel(alt=ind_T_min_BL)
    #T_max_FT = T_FT.isel(alt=ind_T_max_FT)
    #dT = T_max_FT - T_min_BL
    #dT = dT.where(~np.isnan(inputs['INVHGT']), np.nan)
    POTT_min_BL = POTT.isel(alt=ind_T_min_BL)
    POTT_max_FT = POTT.isel(alt=ind_T_max_FT)
    dPOTT = POTT_max_FT - POTT_min_BL
    dPOTT = dPOTT.where(~np.isnan(inputs['INVHGT']), np.nan)
    if var_name in ['INVSTRA', 'INVSTRVA']:
        dalt = alt_T_max_FT - alt_T_min_BL
        dalt = dalt.where(~np.isnan(inputs['INVHGT']), np.nan)
        if np.sum(dalt <= 0):
            print('INVSTRA: {}: {} negative dalt values!'.format(mkey,
                    np.sum(dalt <= 0).values))
            dalt = dalt.where(dalt > 0, np.nan)
        #dT = dT.where(dalt > 0, np.nan)
        dPOTT_dalt = dPOTT/dalt
        if np.sum(dPOTT_dalt <= 0):
            print('INVSTRA: {}: {} negative dPOTT_dalt values!'.format(mkey,
                    np.sum(dT_dalt <= 0).values))
            dPOTT_dalt = dPOTT_dalt.where(dPOTT_dalt > 0, np.nan)
        #plt.contourf(dT_dalt.mean(dim='time').squeeze())
        #plt.colorbar()
        #plt.show()
        #quit()
        out_var = dPOTT_dalt
    elif var_name in ['INVSTR', 'INVSTRV']:
        out_var = dPOTT
    else: raise ValueError()
    return(out_var)


def compute_SHFLX(mkey, inputs, var_name):
    if mkey in ['COSMO', 'ICON', 'ARPEGE-NH', 'IFS', 'ERA5']:
        out_var = inputs[var_name] * -1
    elif mkey in ['NICAM', 'SAM', 'UM', 'MPAS', 'GEOS', 'FV3']:
        out_var = inputs[var_name]
    return(out_var)


def compute_PP(mkey, inputs):
    if mkey in ['COSMO', 'NICAM', 'SAM', 'ICON', 'UM',
                'GEOS', 'ARPEGE-NH']:
        out_var = inputs['PP']
    elif mkey in ['ERA5']:
        out_var = inputs['PP']*1000
    elif mkey in ['MPAS', 'IFS']:
        out_var = inputs['PPCONV'] + inputs['PPGRID']
    elif mkey == 'FV3':
        # convert kg/m2/s precip to mm/h
        out_var = inputs['PP'] * 3600
    else:
        raise ValueError
    return(out_var)

def compute_CORREFL(mkey, inputs):
    if mkey == 'SUOMI_NPP_VIIRS':
        out_var = inputs['CORREFL'] * 0.10
    else:
        out_var = inputs['TQC']
        #out_var.values[out_var.values < 0.0001] = 0.
        #out_var.values = out_var.values**(1/2)
    return(out_var)

def compute_CLC(mkey, inputs, var_name):
    if var_name == 'CLCL':
        if mkey == 'ARPEGE-NH':
            # TEST: CLCL
            out_var = inputs['CLCL'] / 100
            # TEST: CLCT
            #out_var = inputs['CLCT'] / 100
            # TEST: CLCT but mask with TQI
            #out_var = inputs['CLCT'] / 100
            #out_var = out_var.where(inputs['TQI'] < 1E-5, np.nan)
        elif mkey in ['COSMO']:
            # TEST: CLCL
            out_var = inputs['CLCL']
            # TEST: CLCT
            #out_var = np.minimum(inputs['CLCL'] + 
            #                     inputs['CLCM'] + 
            #                     inputs['CLCH'], 1)
            # TEST: CLCT but mask with TQI
            #out_var = np.minimum(inputs['CLCL'] + 
            #                     inputs['CLCM'] + 
            #                     inputs['CLCH'], 1)
            #out_var = out_var.where(inputs['TQI'] < 1E-5, np.nan)
        elif mkey in ['IFS']:
            out_var = inputs['CLCL']
        elif mkey in ['UM', 'MPAS']:
            out_var = inputs['CLCT']
            tqi, out_var = select_common_timesteps(inputs['TQI'], out_var)
            out_var = out_var.where(tqi < 1E-5, np.nan)
        elif mkey in ['ICON']:
            out_var = inputs['CLCT'] / 100
            tqi, out_var = select_common_timesteps(inputs['TQI'], out_var)
            out_var = out_var.where(tqi < 1E-5, np.nan)
        elif mkey in ['FV3']:
            out_var = inputs['CLCT'] / 100
            # this takes very long but is necessary....
            tqi = inputs['TQI'].interp_like(out_var)
            out_var = out_var.where(tqi < 1E-5, np.nan)
        elif mkey in ['NICAM', 'SAM', 'GEOS']:
            pass
            # not CLCL data
        elif mkey in ['CM_SAF']:
            out_var = inputs['CLCL'] / 100
        else:
            raise ValueError()
    elif var_name == 'CLCL2':
        if mkey in ['CM_SAF_MSG']:
            out_var = inputs['CLCL2'] / 100
        else:
            tqc = inputs['TQC']
            out_var = xr.where(tqc > 0.001, 1., 0.)
    else:
        raise NotImplementedError()
    return(out_var)

def compute_TQV(mkey, inputs, var_name):
    """
    Compute vertical integral of QV*RHO between specific heights.
    """
    if mkey == 'CM_SAF_HTOVS':
        return(inputs['TQV'])

    qv = inputs['QV']
    rho = inputs['RHO']
    qv, rho = select_common_timesteps(qv, rho)

    # set missing values to zero (for integration)
    qv = qv.where(~np.isnan(qv), 0)
    rho = rho.where(~np.isnan(rho), 0)

    # select specific altitude slice
    if var_name == 'TQV':
        alt_slice = slice(0,6000)
    elif var_name == 'TQVFT':
        alt_slice = slice(3000,6000)
    else: raise NotImplementedError()
    qv = subsel_alt(qv, mkey, alt_slice)
    rho = subsel_alt(rho, mkey, alt_slice)

    if var_name == 'TQVFT':
        # select above inversion
        qv = qv.where(qv.alt > inputs['INVHGT'], 0)
        rho = rho.where(rho.alt > inputs['INVHGT'], 0)

    # convert to kg_water / m^3 air
    #print(qv.alt)
    #print(rho.alt)
    #quit()
    qv *= rho

    # take into account possible wrong orientations of coordinates
    factor = 1
    if qv.alt.values[-1] < qv.alt.values[0]:
        factor *= -1
    qv *= factor

    # integrate over vertical extent
    out_var = qv.integrate(dim=['alt'])

    if var_name == 'TQVFT':
        # set locations without inversion to missing
        out_var = out_var.where(~np.isnan(inputs['INVHGT']), np.nan)
    return(out_var)


def compute_vertical_integ_or_mean(mkey, inputs, var_name):
    """
    """
    if var_name == 'KEWMBLI':
        raise NotImplementedError()
        out_var = inputs['KEW']
        rel_alt = (0, 1)
        abs_alt = None
        compute_mean = 1
        density_weight = 0
    elif var_name == 'AWMBLI':
        raise NotImplementedError()
        out_var = inputs['AW']
        rel_alt = (0, 1)
        abs_alt = None
        compute_mean = 1
        density_weight = 0
    elif var_name == 'TKEMBLI':
        out_var = inputs['TKE']
        rel_alt = (0, 1)
        abs_alt = None
        density_weight = 0
    elif var_name == 'DIABHMINV':
        raise NotImplementedError()
        out_var = inputs['DIABH']
        rel_alt = (0.75, 1.25)
        abs_alt = None
        compute_mean = 1
        density_weight = 0
    elif var_name == 'POTTHDIVMBLI':
        out_var = inputs['POTTHDIV']
        rel_alt = None
        #rel_alt = (0.00, 1.00)
        #abs_alt = None
        abs_alt = (0, 500)
        weight = 'density'
        weight = 'volume'
    elif var_name == 'POTTMBLI':
        out_var = inputs['POTT']
        rel_alt = None
        #rel_alt = (0.00, 1.00)
        #abs_alt = None
        abs_alt = (0, 500)
        weight = 'density'
        weight = 'volume'
    else:
        raise NotImplementedError()
    if weight == 'volume':
        weight = xr.ones_like(out_var)
    elif weight == 'density':
        weight = inputs['RHO']
    else: raise ValueError('no weight given')
    if not np.array_equal(weight.alt.values, out_var.alt.values):
        weight = weight.interp(alt=out_var.alt)
    # set missing values to zero for integration
    # (e.g. nicam has nan for topography points)
    out_var.values[np.isnan(out_var.values)] = 0.
    weight.values[np.isnan(out_var.values)] = 0.
    # get altitude values that limit vertical integration domain
    invhgt = inputs['INVHGT']
    # make sure the two arrays have the same times
    invhgt, out_var = select_common_timesteps(invhgt, out_var)
    weight, out_var = select_common_timesteps(weight, out_var)
    if rel_alt is not None and abs_alt is not None:
        raise ValueError('both rel_alt and abs_alt given')
    if rel_alt is not None:
        lower_limit = copy.deepcopy(invhgt) * rel_alt[0]
        upper_limit = copy.deepcopy(invhgt) * rel_alt[1]
    elif abs_alt is not None:
        lower_limit = xr.full_like(invhgt, abs_alt[0])
        upper_limit = xr.full_like(invhgt, abs_alt[1])
    # weight variable
    out_var *= weight
    # set values above inversion to 0 (again not to nan becuase of integration)
    out_var = out_var.where(((out_var.alt >= lower_limit) &
                             (out_var.alt <= upper_limit)), 0.)
    weight = weight.where(((weight.alt >= lower_limit) &
                           (weight.alt <= upper_limit)), 0.)
    # check if vertical dimension needs to be flipped
    # this is necessary for integration
    if np.diff(out_var.isel(alt=[0,-1]).alt.values) < 0:
        raise ValueError()
        #out_var = out_var.sel(alt=slice(None, None, -1))
    # vertically integrate variable
    out_var = out_var.integrate(dim='alt')
    weight = weight.integrate(dim='alt')
    out_var /= weight

    # in case of relative height selection,
    # set grid points with no inversion to nan
    if rel_alt is not None:
        out_var = out_var.where(invhgt != np.nan, np.nan)
    return(out_var)



def compute_VARI(mkey, inputs, var_name):
    var = inputs[var_name[:-1]]
    # reverse altitude if necessary
    if var.alt[-1] < var.alt[0]:
        raise ValueError()
        var = var.sortby('alt', ascending=True)
    invhgt = inputs['INVHGT']
    var, invhgt = select_common_timesteps(var, invhgt)
    # in case the lowest invhgt is below the lowest level of var (IFS_4)
    invhgt_tmp = invhgt.where(invhgt >= np.min(var.alt.values), np.min(var.alt.values))

    if invhgt_tmp.shape[1:3] != var.shape[2:4]:
        print('ERROR: INVHGT is not on same grid as VAR')
        quit()
    var.load()
    invhgt_tmp.load()
    invhgt.load()
    if var_name in ['WI']:
        # does not work for some models because of inaccurate vertical coord.
        # (TODO: why exactly?)
        try:
            out_var = var.sel(alt=invhgt_tmp, method='pad')
        except KeyError:
            print('take method nearest for {}'.format(mkey))
            out_var = var.sel(alt=invhgt_tmp, method='nearest')
    elif var_name in ['UI', 'VI']:
        try:
            out_var = var.sel(alt=invhgt_tmp, method='backfill')
        except KeyError:
            print('take method nearest for {}'.format(mkey))
            out_var = var.sel(alt=invhgt_tmp, method='nearest')
    else:
        raise ValueError()
    out_var.load()
    ## set to negative because flux should point into MBL
    #out_var = - out_var
    # if no inversion, set var to np.nan
    out_var = out_var.where(~np.isnan(invhgt), np.nan)
    #quit()
    return(out_var)


def compute_ENTR(mkey, inputs, var_name):
    """
    Entrainment velocity is compute according to Stevens TCFD 2006 Eq. 18
    """
    if var_name  == 'ENTRH':
        ui = inputs['UI']
        vi = inputs['VI']
        #rhoi = inputs['RHOI']
        ui, vi = select_common_timesteps(ui, vi)
        #ui, rhoi = select_common_timesteps(ui, rhoi)
        #ui, vi = select_common_timesteps(ui, vi)
        dinvhgtdx = inputs['INVHGT'].differentiate(coord='lon') / CON_M_PER_DEG
        dinvhgtdy = inputs['INVHGT'].differentiate(coord='lat') / CON_M_PER_DEG
    if var_name == 'ENTRV':
        wi = inputs['WI']
        #rhoi = inputs['RHOI']
        #wi, rhoi = select_common_timesteps(wi, rhoi)
    if var_name == 'ENTR':
        entrh, entrv = select_common_timesteps(inputs['ENTRH'], inputs['ENTRV'])

    if var_name == 'ENTRV':
        #out_var = - wi * rhoi
        out_var = - wi
    elif var_name == 'ENTRH':
        #out_var = rhoi * (ui * dinvhgtdx + vi * dinvhgtdy)
        out_var = ui * dinvhgtdx + vi * dinvhgtdy
    elif var_name == 'ENTR':
        out_var = entrh + entrv

    return(out_var)

def compute_VARCB(mkey, inputs, var_name):
    out_var = inputs[var_name[:-2]]
    # vertical axis should be with ascending altitude. else sort
    if out_var.alt.values[-1] < out_var.alt.values[0]:
        out_var = out_var.sortby('alt', ascending=True)
    cldbase = inputs['CLDBASE']
    out_var, cldbase = select_common_timesteps(out_var, cldbase)
    # make temporary cldbase field with missing values
    # artificially set to 1000 m. These locations will be
    # overwritten afterwards.
    cldbase_tmp = cldbase.where(~np.isnan(cldbase), 1000)
    out_var = out_var.sel(alt=cldbase_tmp, method='pad')
    # if no cloudbase, set out_var to np.nan
    out_var = out_var.where(~np.isnan(cldbase), np.nan)
    out_var['alt'] = out_var.alt.where(~np.isnan(cldbase), np.nan)
    ## if no cloudbase, set var to np.nan
    #out_var = out_var.where(~np.isnan(cldbase), np.nan)
    #out_var = out_var.sel(alt=cldbase, method='pad')
    return(out_var)


def compute_SST(mkey, inputs):
    # TODO! Attention! Currently no land-sea mask is used to differentiate
    # between land and sea.
    if mkey == 'COSMO':
        out_var = inputs['TSURF']
    return(out_var)

def compute_WMEAN(mkey, inputs):
    out_var = inputs['W'] # assumes W is tmean
    return(out_var)

def compute_variance(mkey, inputs, var_name):
    if var_name in 'WTURB':
        out_var = ( inputs['W'] - inputs['WMEAN'] )**2
        #out_var = (inputs['W'] - inputs['WMEAN'])/inputs['INVHGT']
        #out_var = ( (inputs['W'] - inputs['WMEAN'])/inputs['INVHGT'] )**2
    return(out_var)






@njit()
def aux_filter_MBL_base(cldmask, alts, invhgt,
                        ntime, nalt, nlat, nlon):
    """
    set in cldmask(time,alt,lat,lon) all grid boxes to zero (= no cloud)
    which demark a cloud base that is above the inversion height.
    This is to select only low clouds that are rooted in the MBL.
    """
    for time_ind in range(ntime):
        for lat_ind in range(nlat):
            for lon_ind in range(nlon):
                mask_below = 1
                for alt_ind in range(nalt):
                    # remove clouds that are above the inversion and
                    # do not have a cloud in the cell below.
                    if (
                        (mask_below == 0) &
                        (cldmask[time_ind, alt_ind, lat_ind, lon_ind] == 1) &
                        (invhgt[time_ind, lat_ind, lon_ind] < alts[alt_ind])
                       ):
                        cldmask[time_ind, alt_ind, lat_ind, lon_ind] = 0
                    mask_below = cldmask[time_ind, alt_ind, lat_ind, lon_ind]



@njit()
def aux_vind_to_height(vinds, heights,
            out_height, ntime, nlat, nlon, mode='centred'):
    """
    Select from heights(alt) the index vinds(time,lat,lon)
    and insert it in out_height(time,lat,lon).
    mode:
        'centred':
            assume that the height index should be taken at vind
        'below':
            assume that the height index should be taken between
            vind and vind-1 (staggered)
        'above':
            assume that the height index should be taken between
            vind and vind+1 (staggered)
    """
    for time_ind in range(ntime):
        for lat_ind in range(nlat):
            for lon_ind in range(nlon):
                vind = vinds[time_ind,lat_ind,lon_ind]
                if vind > 0:
                    if mode == 'centred':
                        out_height[time_ind,lat_ind,lon_ind
                                    ] = heights[vind]
                    elif mode == 'below':
                        out_height[time_ind,lat_ind,lon_ind
                                    ] = 0.5*(heights[vind] + heights[vind-1])
                    elif mode == 'above':
                        out_height[time_ind,lat_ind,lon_ind
                                    ] = 0.5*(heights[vind] + heights[vind+1])
                    else:
                        raise NotImplementedError()
                else:
                    out_height[time_ind,lat_ind,lon_ind] = np.nan



def subsel_alt(ds, mkey, alt):
    """
    Subselect in xarray Dataset (or DataArray) ds, the altitude alt
    (if alt is slice, it is assumed to increase from start to stop!!)
    """
    if isinstance(alt, slice):
        ds = ds.sel(alt=alt)
    else:
        raise NotImplementedError()
    return(ds)



