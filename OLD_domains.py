

###############################################################################
# OLD ANALYSIS DOMAINS
###############################################################################
## North Atlantic Stratocumulus
#dom_NA = {
#    'label':'North Atlantic Stratocumulus',
#    'code':'N_Atl_Sc',
#    'lat':slice(18,26),
#    'lon':slice(-46,-20)
#}
#dom_NA_12 = {
#    'label':'lm_c',
#    'lon':slice(-70.115, -70.115+0.11*581),
#    'lat':slice(- 7.31 , - 7.31 +0.11*400),
#    'nlon':581,
#    'nlat':400,
#}
#
#dom_NA_2 = {
#    'label':'lm_f',
#    'nlon':2470,
#    'nlat':1550,
#    'lon':slice(-65.625, -65.625+0.02*2470),
#    'lat':slice(- 1.77 , - 1.77 +0.02*1550),
#}

#dom_ana_lh = {
#    'label':'Laureline',
#    'lat':slice(-5,22),
#    'lon':slice(-63,-23)
#}


## South-East Pacific Stratocumulus
#dom_SE_Pac_Sc = {
#    'label':'South-East Pacific Stratocumulus',
#    'code':'SE_Pac_Sc',
#    'lat':slice(-24,-14),
#    'lon':slice(-95,-79)
#}




###############################################################################
# OLD MODEL DOMAINS
###############################################################################
dom_lm_c_lh = {
    #'label':'12km Trop. Atlantic',
    'label':'12km',
    'lon':slice(-70.115, - 9.505),
    'lat':slice(-12.26 ,  27.45 ),
    'nlon':551,
    'nlat':361,
}

dom_lm_f_lh = {
    #'label':'2.2km Trop. Atlantic',
    'label':'2.2km',
    'lon':slice(-65.625, -19.425),
    'lat':slice(- 7.77 ,  23.07 ),
    'nlon':2310,
    'nlat':1542,
}




###### 12km test full domain
lon0 = -70.
lat0 = -38.1
nlon = 873
nlat = 688
dom_lm_12_test_full = {
    'code':'12km',
    'label':'12km',
    'lon':slice(lon0, lon0 + 0.11*nlon),
    'lat':slice(lat0, lat0 + 0.11*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 4km test full domain
lon0 = -66
lat0 = -34
nlon = 2200
nlat = 1690
dom_lm_4_test_full = {
    'code':'4km',
    'label':'4km',
    'lon':slice(lon0, lon0 + 0.04*nlon),
    'lat':slice(lat0, lat0 + 0.04*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 12km test no north atlantic
lon0 = -50.09
lat0 = -38.10
nlon = 691
nlat = 546
dom_lm_12_test_nona = {
    'code':'12km_test_nona',
    'label':'12km no North Atlantic',
    'lon':slice(lon0, lon0 + 0.11*nlon),
    'lat':slice(lat0, lat0 + 0.11*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 4km test no north atlantic
lon0 = -46
lat0 = -34
nlon = 1700
nlat = 1300
dom_lm_4_test_nona = {
    'code':'4km_test_nona',
    'label':'4km no North Atlantic',
    'lon':slice(lon0, lon0 + 0.04*nlon),
    'lat':slice(lat0, lat0 + 0.04*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 12km test no itcz
lon0 = -50.09
lat0 = -38.1
nlon = 691
nlat = 386
dom_lm_12_test_noitcz = {
    'code':'12km_test_noitcz',
    'label':'12km no ITCZ',
    'lon':slice(lon0, lon0 + 0.11*nlon),
    'lat':slice(lat0, lat0 + 0.11*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 4km test no itcz
lon0 = -46
lat0 = -34
nlon = 1700
nlat = 860
dom_lm_4_test_noitcz = {
    'code':'4km_test_noitcz',
    'label':'4km no ITCZ',
    'lon':slice(lon0, lon0 + 0.04*nlon),
    'lat':slice(lat0, lat0 + 0.04*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 12km test no land
lon0 = -35.02
lat0 = -28.09
nlon = 564
nlat = 291
dom_lm_12_test_land = {
    'code':'12km_test_land',
    'label':'12km more land',
    'lon':slice(lon0, lon0 + 0.11*nlon),
    'lat':slice(lat0, lat0 + 0.11*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 4km test more land
lon0 = -31
lat0 = -24
nlon = 1350
nlat = 600
dom_lm_4_test_land = {
    'code':'4km_test_land',
    'label':'4km more land',
    'lon':slice(lon0, lon0 + 0.04*nlon),
    'lat':slice(lat0, lat0 + 0.04*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 12km test small (minimal domain)
lon0 = -35.02
lat0 = -28.09
nlon = 500
nlat = 291
dom_lm_12_test_small = {
    'code':'12km_test_small',
    'label':'12km minimal',
    'lon':slice(lon0, lon0 + 0.11*nlon),
    'lat':slice(lat0, lat0 + 0.11*nlat),
    'nlon':nlon,
    'nlat':nlat,}
###### 4km test small (minimal domain)
lon0 = -31
lat0 = -24
nlon = 1175
nlat = 600
dom_lm_4_test_small = {
    'code':'4km_test_small',
    'label':'4km minimal',
    'lon':slice(lon0, lon0 + 0.04*nlon),
    'lat':slice(lat0, lat0 + 0.04*nlat),
    'nlon':nlon,
    'nlat':nlat,}



###############################################################################
# OLD DYAMOND DOMAINS
###############################################################################
## dyamond western Pacific domain
#dom_dya_wp = {
#    'label':'dya_west_Pac',
#    'lat':slice(-24,-14),
#    'lon':slice(265-360,281-360)
#}
#dom_test = {
#    'label':'dya_west_Pac',
#    'lat':slice(-24,-20),
#    'lon':slice(265-360,268-360)
#}
