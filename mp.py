#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    Class for parallel computing and loading of time step files.
author			Christoph Heim
date created    21.03.2019
date changed    01.02.2020
usage			use in another script
"""
###############################################################################
import sys, time
import multiprocessing as mp
import xarray as xr
import numpy as np
from datetime import datetime,timedelta
from package.member import Member
###############################################################################



def starmap_helper(tup):
    func = tup['func']
    del tup['func']
    return(func(**tup))


def run_starmap(func, fargs={}, njobs=1, run_async=False):
    outputs = []
    if njobs > 1:
        pool = mp.Pool(processes=njobs)
        if run_async:
            outputs = pool.starmap_async(starmap_helper, fargs).get()
        else:
            outputs = pool.starmap(starmap_helper, fargs)
    else:
        for i in range(len(fargs)):
            out = func(**fargs[i])
            outputs.append(out)
    return(outputs)



class TimeStepMP:

    def __init__(self, timesteps, njobs=None, run_async=False):
        self.timesteps = timesteps
        self.run_async = run_async

        if njobs is None:
            if len(sys.argv) > 1:
                self.njobs = int(sys.argv[1])
            else:
                self.njobs = 1
        else:
            self.njobs = njobs
        #print('TimeStepMP: njobs = '+str(self.njobs))

        self.output = None


    def run(self, func, fargs={}, step_args=None):
        outputs = []

        input = []
        for tI,ts in enumerate(self.timesteps):
            this_fargs = fargs.copy()
            this_fargs['ts'] = ts
            if step_args is not None:
                this_fargs.update(step_args[tI])

            if self.njobs > 1:
                this_fargs['func'] = func
                this_fargs = (this_fargs,)
            input.append(this_fargs)

        self.output = run_starmap(func, fargs=input,
                        njobs=self.njobs, run_async=self.run_async) 
        

    def concat_timesteps(self):
        if (self.output is not None):
            vars = []
            mkeys = list(self.output[0]['members'].keys())
            members = {}
            for mkey in mkeys:
                print(mkey)
                # in case members contains a dict with multiple
                # variables for each member
                #elif isinstance(self.output[0]['members'][mkey], dict):
                members[mkey] = {}
                # find first entry that is not none and get variables
                c = 0
                try:
                    while self.output[c]['members'][mkey] is None:
                        c += 1
                except IndexError:
                    print('No entry for member {}'.format(mkey)) 
                    continue
                vkeys = self.output[c]['members'][mkey].keys()
                # go through each variable
                for vkey in vkeys:
                    vars = []
                    # go through each date
                    for time_step in self.output:
                        member_in = time_step['members'][mkey]
                        # if member contains data for this date
                        if member_in is not None:
                            vars.append(member_in[vkey].var)
                            member_dict = member_in[vkey].mem_dict
                    members[mkey][vkey] = Member(
                                    xr.concat(vars, dim='time'),
                                    member_dict)

            self.concat_output = {'members':members}
        else:
            print('TimeStepWise: No values calculated')



class IterMP:

    def __init__(self, njobs=None, run_async=False):
        self.run_async = run_async

        if njobs is None:
            if len(sys.argv) > 1:
                self.njobs = int(sys.argv[1])
            else:
                self.njobs = 1
        else:
            self.njobs = njobs
        print('IterMP: njobs = '+str(self.njobs))

        self.output = None


    def run(self, func, fargs={}, step_args=None):
        outputs = []

        input = []
        for tI in range(len(step_args)):
            this_fargs = fargs.copy()
            if step_args is not None:
                this_fargs.update(step_args[tI])

            if self.njobs > 1:
                this_fargs['func'] = func
                this_fargs = (this_fargs,)
            input.append(this_fargs)

        self.output = run_starmap(func, fargs=input,
                        njobs=self.njobs, run_async=self.run_async) 
        




def test_IMP(iter_arg, fixed_arg):
    #print(str(iter_arg) + ' ' + str(fixed_arg))
    work = []
    for i in range(int(1E7)):
        work.append(1)
    return(iter_arg)

def test_TSMP(ts, task_no, test_arg):
    #print(str(ts) + ' ' + str(task_no) + ' ' + test_arg)
    return(task_no)



if __name__ == '__main__':


    if len(sys.argv) > 1:
        njobs = int(sys.argv[1])
    else:
        njobs = 1
    
    # TEST TSMP
    timesteps = np.arange(datetime(2015,1,1,1), datetime(2015,1,1,5),
                        timedelta(hours=1))
    TSMP = TimeStepMP(timesteps, njobs=njobs, run_async=False)
    fargs = {'test_arg':'test',}
    step_args = []
    for i,dt in enumerate(timesteps):
        step_args.append({'task_no':i})
    TSMP.run(test_TSMP, fargs, step_args)
    print(TSMP.output)

    # TEST BASE 
    t0 = time.time()
    IMP = IterMP(njobs=njobs, run_async=False)
    fargs = {'fixed_arg':'fixed',}
    step_args = []
    for i in range(20):
        step_args.append({'iter_arg':i})
    IMP.run(test_IMP, fargs, step_args)
    print(IMP.output)
    t1 = time.time()
    print(t1 - t0)

