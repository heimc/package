#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description:    Useful stuff
author:         Christoph Heim
date created:   27.06.2019
date changed:   03.03.2021
usage:          import in other scripts
"""
###############################################################################
import time, glob, os, subprocess, pickle
import numpy as np
import xarray as xr
from datetime import datetime, timedelta
from datetime import time as dttime
from pathlib import Path
from datetime import datetime
from cdo import Cdo
###############################################################################

cdo = Cdo()

###############################################################################
## TIMER CLASS
###############################################################################
class Timer:
    
    TOTAL_CPU_TIME  = 'tot_cpu'
    REAL_TIME       = 'total'
    
    def __init__(self, mode='minutes', parallel=False):
        self.mode = mode
        self.parallel = parallel

        self.timings = {self.TOTAL_CPU_TIME:0.}
        self.flags = {self.TOTAL_CPU_TIME:None}

        # start real total computation time timer.
        self.start(self.REAL_TIME)


    def start(self, timer_key):
        """
        Start timer with key timer_key.
        Also start TOTAL_CPU_TIME timer if not already running.
        """
        # start TOTAL_CPU_TIME timer
        if self.flags[self.TOTAL_CPU_TIME] is None:
            self.flags [self.TOTAL_CPU_TIME] = time.time()
        # start timer_key timer
        if timer_key not in self.timings.keys():
            self.timings[timer_key] = 0.
        else:
            if self.flags[timer_key] is not None:
                print('Caution: Timer', timer_key, 'already running')
        self.flags[timer_key] = time.time()

    def stop(self, timer_key):
        """
        Stop timer with key timer_key.
        Also stop TOTAL_CPU_TIME timer if timer_key is the only running task
        """
        if (timer_key not in self.flags.keys()
            or self.flags[timer_key] is None):
            raise ValueError('No time measurement in progress for timer ' +
                            str(timer_key) + '.')

        self.timings[timer_key] += time.time() - self.flags[timer_key]
        self.flags[timer_key] = None

        # Check if any other timer is running. If not, stop TOTAL_CPU_TIME timer
        other_timers_running = False
        for key,flag in self.flags.items():
            if key not in [self.TOTAL_CPU_TIME, self.REAL_TIME]:
                if flag is not None:
                    other_timers_running = True
        if not other_timers_running:
            self.timings[self.TOTAL_CPU_TIME] += (time.time() - 
                                            self.flags[self.TOTAL_CPU_TIME])
            self.flags[self.TOTAL_CPU_TIME] = None

    def merge_timings(self, Timer):
        """
        Merge timings from parallel execution Timer instances into this timer.
        """
        for timer_key in Timer.flags.keys():
            if ( (timer_key in self.timings.keys()) and 
                 (timer_key != self.REAL_TIME) ):
                self.timings[timer_key] += Timer.timings[timer_key]
            else:
                self.timings[timer_key] = Timer.timings[timer_key]

    def print_report(self, short=False):

        timer_key = self.REAL_TIME
        self.timings[timer_key] += time.time() - self.flags[timer_key]
        self.flags[timer_key] = None

        n_decimal_perc = 0
        n_decimal_sec = 1
        n_decimal_min = 2
        cpu_time = max(0.00001,self.timings[self.TOTAL_CPU_TIME])
        real_time = max(0.00001,self.timings[self.REAL_TIME])

        # shorten labels of different timings for nice printing
        max_len = 7
        new_timings = {}
        for key,value in self.timings.items():
            if len(key) > max_len:
                key = key[(len(key)-max_len):]
                new_timings[key] = value
            else:
                new_timings[key] = value
        self.timings = new_timings

        if not short:
            if self.mode == 'minutes':
                print('##########################################################')
                print('Took ' + str(np.round(real_time/60,n_decimal_min))
                      + ' min.')
                print('Detailed process times (cpu time, not real time):')
                for key,value in self.timings.items():
                    if key != self.REAL_TIME:
                        print(key + '\t' + 
                            str(np.round(100*value/cpu_time,n_decimal_perc)) +
                        '\t%\t' + str(np.round(value/60,n_decimal_min)) + ' \tmin')
            elif self.mode == 'seconds':
                print('##########################################################')
                print('Took ' + str(np.round(real_time,n_decimal_sec)) + ' sec.')
                print('Detailed process times (cpu time, not real time):')
                for key,value in self.timings.items():
                    if key != self.REAL_TIME:
                        print(key + '\t' + 
                            str(np.round(100*value/cpu_time,n_decimal_perc)) +
                        '\t%\t' + str(np.round(value,n_decimal_sec)) + ' \tsec')
            else:
                raise ValueError()
        else:
            print('##########################################################')
            print('Took ' + str(np.round(real_time/60,n_decimal_min))
                  + ' min.')







###############################################################################
## CDO COMMANDS
###############################################################################
def cdo_mergetime(inp_folder, out_folder, var_name):
    """
    Merge all nc files with name $var_name_*.nc in $inp_folder
    and copy result to $out_folder.
    Used for dyamond copies on mistral.
    """
    #inp_files = glob.glob(os.path.join(inp_folder,var_name+'_*'))
    #inp_files.sort()
    #out_file = os.path.join(out_folder,var_name+'.nc')
    #cdo.mergetime(input=inp_files, output=out_file, options='-O')
    inp_files = os.path.join(inp_folder,var_name+'_*')
    out_file = os.path.join(out_folder,var_name+'.nc')
    print(inp_files)
    print(out_file)
    subprocess.call(['cdo', '-O', 'mergetime', inp_files, out_file])







###############################################################################
## FIELD COMPUTATIONS
###############################################################################
def calc_mean_diurnal_cycle(field_hourly, aggreg_type='MEAN'):
    """
    Calculates the mean diurnal cycle for the input field.
    Removes dimension 'time' and introduces new dimension
    'diurnal' ranging from 0 to 23.
    ---------------------------------------------------------------------------
    INPUT:
    field_hourly:   xarray DataArray - Input field with hourly timesteps and
                    dimension 'time'.
    aggreg_type:    str - How to aggregate the days for one hour.
    ---------------------------------------------------------------------------
    OUTPUT:
    xarray DataArray - Output field with dimension 'time' removed and dimension
                       'diurnal' created. 
    ---------------------------------------------------------------------------
    COMMENTS:
    """
    hr_fields = []
    for hour in range(0,24):
        this_hr_field = field_hourly.sel(time=dttime(hour))
        this_hr_field = this_hr_field.assign_coords(diurnal=hour)
        hr_fields.append(this_hr_field)
    field_diurn = xr.concat(hr_fields, dim='diurnal')


    if aggreg_type == 'MEAN':
        field_diurn = field_diurn.mean(dim='time')
    else:
        raise NotImplementedError()

    return(field_diurn)


def subsel_domain(ds, domain):
    """
    Subselect latitude longitude domain.
    Input:
      ds        - an xarray Dataset/DataArray object
      domain    - a dict object describing the domain with a
                    lat and a lon slice (for a rectangular domain)
                    or single values (for a point)
    Output:
      DATE - a python datetime object
    """
    # 2D domain
    if (isinstance(domain['lon'], slice) and 
        isinstance(domain['lat'], slice)):
        # revert latitude axis? (the case in IFS raw data)
        try:
            if ds.lat[-1] < ds.lat[0]:
                lat_slice = slice(domain['lat'].stop, domain['lat'].start)
            else: lat_slice = domain['lat']
        except IndexError:
            lat_slice = domain['lat']
        #print(ds.shape)
        if 'lat' in ds.dims:
            ds = ds.sel(lat=lat_slice)
        else:
            raise ValueError('lat: Is this really ok? Check if subdom is really cut out!')
        if 'lon' in ds.dims:
            ds = ds.sel(lon=domain['lon'])
        else:
            raise ValueError('lon: Is this really ok? Check if subdom is really cut out!')

        #print(ds.shape)
        #quit()
    # point value
    else:
        # select nearest grid point
        ds = ds.sel(lat=domain['lat'], lon=domain['lon'], method='nearest')
    return(ds)




def select_common_timesteps(array1, array2):
    """
    Select from both arrays only the time step they have in common.
    Input:
      array1,array2 - to xr data arrays containing time
    Output:
      array1,array2 reduced to common time steps.
    """
    if ('time' in array1.dims) and ('time' in array2.dims):
        sel_time = []
        for ts in array1.time.values:
            if ts in array2.time:
                sel_time.append(ts)
        array2 = array2.sel(time=sel_time)
        sel_time = []
        for ts in array2.time.values:
            if ts in array1.time:
                sel_time.append(ts)
        array1 = array1.sel(time=sel_time)
    return((array1, array2))





###############################################################################
## REMAPPING FUNCTIONS
###############################################################################
def write_grid_des_file(domain, file, dx_km, padding=0):
    """
    Writes a file with a grid description that can be used for cdo remap
    """
    rE = 6371
    dx_deg = dx_km / rE / np.pi * 180
    lat0 = domain['lat'].start - padding
    lat1 = domain['lat'].stop + padding
    lon0 = domain['lon'].start - padding
    lon1 = domain['lon'].stop + padding
    xsize = (lon1 - lon0)/dx_deg
    ysize = (lat1 - lat0)/dx_deg
    with open(file, 'w') as f:
        f.write('gridtype    =   lonlat\n')
        f.write('xsize       =   '+str(int(np.ceil(xsize)))+'\n')
        f.write('ysize       =   '+str(int(np.ceil(ysize)))+'\n')
        f.write('xname       =   lon'+'\n')
        f.write('xlongname   =   "longitude"'+'\n')
        f.write('xunits      =   "degrees_east"'+'\n')
        f.write('yname       =   lat'+'\n')
        f.write('ylongname   =   "latitude"'+'\n')
        f.write('yunits      =   "degrees_north"'+'\n')
        f.write('xfirst      =   '+str(lon0)+'\n')
        f.write('xinc        =   '+str(np.round(dx_deg,3))+'\n')
        f.write('yfirst      =   '+str(lat0)+'\n')
        f.write('yinc        =   '+str(np.round(dx_deg,3))+'\n')


def cdo_remap(grid_des_file, inp_file, out_file):
    """
    Run cdo remap command.
    """
    #print('remap {}'.format(inp_file))
    cdo.remapbil(grid_des_file, input=inp_file, output=out_file)
    #cdo.remapcon(grid_des_file, input=inp_file, output=out_file)


def remap_member_for_date(date, mem_key, mem_dict, var_name,
                           inp_base_dir, out_base_dir,
                           grid_des_file, remap_dx):
    """
    Wrapper to construct input and output paths for remapping based on
    date, mem_key, mem_dict, var_name. Then calls cdo_remap().
    """
    inp_file = os.path.join(inp_base_dir, mem_key,
                            mem_dict['case'], 'daily', var_name,
                            '{}_{:%Y%m%d}.nc'.format(var_name, date) )
    out_path = os.path.join(out_base_dir,
                        'remapped_{:g}'.format(remap_dx), mem_key,
                        mem_dict['case'], 'daily', var_name)
    Path(out_path).mkdir(parents=True, exist_ok=True)
    out_file = os.path.join(out_path,
                    '{}_{:%Y%m%d}.nc'.format(var_name, date))
    #print(out_file)
    #quit()
    if os.path.exists(inp_file):
        cdo_remap(grid_des_file, inp_file, out_file)
    else:
        loc = 'package.utilities.remap_member_for_date()'
        print('WARNING: {} file {} not found.'.format(loc, inp_file))





###############################################################################
## IO FUNCTIONS
###############################################################################
def pickle_load(folder, name):
    file = os.path.join(folder, '{}.pkl'.format(name))
    if os.path.exists(file):
        with open(file, 'rb') as f:
            return(pickle.load(f))
    else: return(False)

def pickle_save(obj, folder, name):
    Path(folder).mkdir(parents=True, exist_ok=True)
    file = os.path.join(folder, '{}.pkl'.format(name))
    with open(file, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)





###############################################################################
## OTHER
###############################################################################
class cd:
    def __init__(self, new_path):
        self.new_path = os.path.expanduser(new_path)

    def __enter__(self):
        self.saved_path = os.getcwd()
        os.chdir(self.new_path)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.saved_path)

def dt64_to_dt(date):
    """
    Converts a numpy datetime64 object to a python datetime object 
    Input:
      date - a np.datetime64 object
    Output:
      DATE - a python datetime object
    source: https://gist.github.com/blaylockbk/1677b446bc741ee2db3e943ab7e4cabd
    """
    timestamp = ((date - np.datetime64('1970-01-01T00:00:00'))
                 / np.timedelta64(1, 's'))
    return datetime.utcfromtimestamp(timestamp)






if __name__ == '__main__':
    #domain = {'lon'=slice(265,281, 'lat0': -24, 'lat1': -14,
    #       'vert0':1,'vert1':22}
    file = 'test_grid'
    write_grid_file(box, file, dx_km=1)
