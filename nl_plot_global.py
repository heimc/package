#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description     Global plotting namelist that is important within each anlysis
                in the specific plot namelists.
author			Christoph Heim
date created    22.06.2019
date changed    15.02.2020
usage           no args
"""
###############################################################################
import cartopy
import matplotlib.pyplot as plt

#print(plt.rcParams.keys())
nlp = {}

#### PLOT TYPE
#nlp['2D_type'] = 'contourf'
nlp['2D_type'] = 'pcolormesh'

# transparent_background
nlp['transparent_bg'] = False

#### PLOT DESIGN
plt.rcParams['axes.labelsize'] = 16
plt.rcParams['axes.titlesize'] = 18
plt.rcParams['figure.titlesize'] = 22
# generic text labels (from plt.text function)
plt.rcParams['font.size'] = 18



nlp['cmap']                 = 'jet'
nlp['arg_subplots_adjust']  = {
                                'left':0.08,
                                'right':0.95,
                                'bottom':0.10,
                                'top':0.95,
                                'wspace':0.14,
                                'hspace':0.00,
                              }
# shared colorbar
cax_x0                      = 0.05
cax_y0                      = 0.05
cax_dx                      = 0.90
cax_dy                      = 0.05
nlp['cax_pos']              = [cax_x0, cax_y0, cax_dx, cax_dy]

#### CARTOPY
nlp['projection']           = cartopy.crs.PlateCarree()
nlp['map_margin']           = (3,3,3,3) # lon0, lon1, lat0, lat1
nlp['land_color']           = (0,0.5,0,0.5)
nlp['ocean_color']          = (0,0.3,1,0.5)
nlp['river_color']          = (0,0.2,0.7,0.3)
