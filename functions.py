#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    Collection of useful functions for data handling and
                processing
author			Christoph Heim
date created    20.04.2019
date changed    25.01.2021
usage			no args
"""
###############################################################################
import os
import numpy as np
import xarray as xr
from datetime import timedelta
from package.nl_variables import nlv
from package.utilities import subsel_domain, pickle_save, pickle_load
from package.model_pp import preproc_model, MODEL_PP, MODEL_PP_DONE
from package.var_pp import (VAR_PP, VAR_PP_DONE, DERIVE, DIRECT,
                            var_mapping, compute_variable)
from package.nl_variables import nlv
from package.nl_models import nlm

###############################################################################

# the following could be used to limit the number of threads that dask uses
# but I do not employ dask here because parallelisation is done manually.
#import dask
#from multiprocessing.pool import ThreadPool
#dask.config.set(pool=ThreadPool(1))


# set debug level of load_member_var function
debug_level_1 = 2
debug_level_2 = 3


def load_member_var(var_name, first_date, last_date,
                    mem_dict, var_src_dict, derive_or_direct,
                    i_debug=0, supress_model_pp=False,
                    domain=None):

    # define member for which to load variable
    if 'res' in mem_dict:
        modres_key =  '{}_{:g}'.format(mem_dict['mod'], mem_dict['res'])
    else: modres_key =  '{}'.format(mem_dict['mod'])
    # set run modus. leads to small differences in execution.
    load_single_day = False
    if first_date == last_date:
        load_single_day = True
    if i_debug >= debug_level_2:
        print(('### function load_member_var: {:%Y%m%d} {:%Y%m%d} '+
                '{} {} var {}').format(
            first_date, last_date, modres_key, derive_or_direct, var_name))

    if derive_or_direct == DERIVE:
        # load raw variables from which this one is derived
        raw_var_names = var_mapping[mem_dict['mod']][var_name]
        if i_debug >= debug_level_2:
            print('from raw_vars {}'.format(raw_var_names))
        raw_vars = {}
        for raw_var_name in raw_var_names:
            # if the raw_var_name equals the var_name, then
            # the variable should be loaded directly and not derived
            # else we would get an infinit recursion loop.
            if raw_var_name == var_name:
                derive_or_direct = DIRECT
            # else take value according to var_src_dict
            else:
                derive_or_direct = var_src_dict[raw_var_name]['load']
            if i_debug >= debug_level_2:
                print('var_name {} and raw_var_name {} --> {} {}'.format(
                        var_name, raw_var_name, derive_or_direct,
                        raw_var_name))
            raw_var = load_member_var(raw_var_name, first_date, last_date,
                                    mem_dict, var_src_dict, derive_or_direct,
                                    i_debug=i_debug,
                                    supress_model_pp=supress_model_pp,
                                    domain=domain)
            if raw_var is None:
                if i_debug >= debug_level_2:
                    print('return None')
                return(None)
            #if domain is not None:
            #    raw_var = subsel_domain(raw_var, domain)
            raw_vars[raw_var_name] = raw_var
        # if variable should get derived from raw_vars but itself is
        # already contained in raw_vars, make sure that its not
        # computed a second time (this would cause errors).
        run_var_computation = True
        if var_name in raw_vars:
            if ( (VAR_PP in raw_vars[var_name].attrs) and
                 (raw_vars[var_name].attrs[VAR_PP] == VAR_PP_DONE) ):
                run_var_computation = False 
        if run_var_computation:
            if domain is not None:
                raw_var = subsel_domain(raw_var, domain)
            var = compute_variable(var_name, mem_dict['mod'], raw_vars, domain)
            if i_debug >= debug_level_2:
                print('Computed var {} from raw_vars {}.'.format(
                            var_name, raw_var_names))
        else:
            if i_debug >= debug_level_2:
                print('Selected var {} from raw_vars {}.'.format(
                            var_name, raw_var_names))
            var = raw_vars[var_name]

    elif derive_or_direct == DIRECT:
        # Some models have some variables that depend on other variables
        # for model preprocessing. (e.g. models that have pressure levels
        # require altitude of pressure levels to convert pressure levels
        # to altitude levels during model_pp).
        dep_vars = {}
        if (('dep' in nlm[mem_dict['mod']]) and 
            (var_name in nlm[mem_dict['mod']]['dep'])):
            dep_var_names = nlm[mem_dict['mod']]['dep'][var_name]
            for dep_var_name in dep_var_names:
                if i_debug >= debug_level_2:
                    print(('var_name {} has dependency on {}'+
                           ' --> load dependency').format(
                            var_name, dep_var_name))
                dep_vars[dep_var_name] = load_member_var(dep_var_name, first_date, last_date,
                                        mem_dict, var_src_dict, derive_or_direct,
                                        i_debug=i_debug, 
                                        supress_model_pp=supress_model_pp,
                                        # do not cut out subdomain for dep_vars
                                        # because it is used in model preprocessing
                                        domain=None)
        # load this variable
        if load_single_day:
            inp_dir = os.path.join(var_src_dict[var_name]['src'],
                                   modres_key, mem_dict['case'],
                                   'daily', var_name)
            inp_file = os.path.join(inp_dir,
                        '{}_{:%Y%m%d}.nc'.format(var_name, first_date))
        else:
            inp_dir = os.path.join(var_src_dict[var_name]['src'],
                                   modres_key, mem_dict['case'],
                                   'tmean', var_name)
            inp_file = os.path.join(inp_dir,
                        '{}_{:%Y%m%d}_{:%Y%m%d}.nc'.format(var_name,
                                        first_date, last_date))
        # load data if file exists
        if not os.path.exists(inp_file):
            print('{}: No file for var {} and dates {:%Y%m%d} and {:%Y%m%d}'.format(
                        modres_key, var_name, first_date, last_date))
            if i_debug >= debug_level_1:
                print(inp_file)
                #quit()
            if i_debug >= debug_level_2:
                print('return None')
            return(None)
        else:
            with xr.open_dataset(inp_file) as ds:
                # preprocess model output if not yet done.
                # testing is done with MODEL_PP flag in attributes
                # first in full data set and else in the separate
                # data fields (usually its only one, namely the
                # var_name data_var. But the loop is here to make
                # sure it does not crash if a dimension is stored
                # as data_var.
                run_model_pp = True
                # if function input argument supresses model_pp
                if supress_model_pp:
                    run_model_pp = False
                # if model_pp already done (still relevant?)                
                if ( (MODEL_PP in ds.attrs) and
                     (ds.attrs[MODEL_PP] == MODEL_PP_DONE) ):
                    run_model_pp = False
                for data_var in list(ds.data_vars.keys()):
                    if ( (MODEL_PP in ds[data_var].attrs) and
                         (ds[data_var].attrs[MODEL_PP] == MODEL_PP_DONE) ):
                        run_model_pp = False
                if run_model_pp:
                    if i_debug >= debug_level_2:
                        print('Run model preprocessing for {}'.format(var_name))
                    if not load_single_day:
                        raise NotImplementedError('Model pp should not be necessary'+
                                'for multi-day data loading and is not implemented.')
                    ds = preproc_model(ds=ds, mkey=mem_dict['mod'],
                                       var_name=var_name,
                                       date=first_date,
                                       data_inp_dir=inp_dir,
                                       dims=nlv[var_name]['dims'],
                                       dep_vars=dep_vars)

                # if variable is loaded from ana_base_dir, it may already
                # be derived/computed and contain the actual var_name as key.
                # and it may thus not be contained within nlm
                #print(ds)
                #print(var_name)
                #quit()
                # If multiday is loaded, remove "_tmean" from var_name
                if load_single_day:
                    file_var_name = var_name
                else:
                    file_var_name = var_name.split('_')[0]
                try:
                    vkey = nlm[mem_dict['mod']]['vkeys'][file_var_name]
                    var = ds[vkey]
                except KeyError:
                    var = ds[file_var_name]
                # make sure var inherits model_pp flag from ds
                if MODEL_PP in ds.attrs:
                    var.attrs[MODEL_PP] = ds.attrs[MODEL_PP]

                # cut out domain of interest
                if domain is not None: var = subsel_domain(var, domain)
    #elif derive_or_direct == 'bin':
    #    pickle_dir = os.path.join(var_src_dict[var_name]['src'])
    #    print(pickle_dir)
    #    print(mem_dict)
    #    print(domain)
    #    quit()
    #    var = load_member_data_from_pickle(pickle_dir,
    #                    mem_dict, domain, [var_name],
    #                    nl.time_periods, nl.i_skip_missing)

    if i_debug >= debug_level_2:
        print('return {}'.format(var_name))
        #print(var)
        #quit()
    return(var)




###############################################################################
## IO FUNCTIONS
###############################################################################
def time_periods_to_dates(time_periods):
    """
    Based on the time_periods list containing all time_periods,
    each represented
    as a dict like e.g.:
    time_periods = [
        {
            'first_date':    datetime(2016,8,6),
            'last_date':     datetime(2016,9,9),
        },
    ]
    create datetime representation of all dates
    """
    dates = []
    for tp in time_periods:
        dates.extend(np.arange(tp['first_date'].date(),
                          tp['last_date'].date()+timedelta(days=1),
                          timedelta(days=1)).tolist())
    return(dates)

def create_time_periods_code(time_periods):
    """
    From time_periods list containing all time_periods, each represented
    as a dict like e.g.:
    time_periods = [
        {
            'first_date':    datetime(2016,8,6),
            'last_date':     datetime(2016,9,9),
        },
    ]
    this function will create a short string representation (code)
    of the time periods to use in names of pickle files.
    """
    year0 = 9999
    year1 = 0
    month0 = 9999
    month1 = 0
    day0 = 9999
    day1 = 0
    for time_period in time_periods:
        if time_period['first_date'].year < year0:
            year0 = time_period['first_date'].year
        if time_period['last_date'].year > year1:
            year1 = time_period['last_date'].year
        if time_period['first_date'].month < month0:
            month0 = time_period['first_date'].month
        if time_period['last_date'].month > month1:
            month1 = time_period['last_date'].month
        if time_period['first_date'].day < day0:
            day0 = time_period['first_date'].day
        if time_period['last_date'].day > day1:
            day1 = time_period['last_date'].day
        
    time_periods_code = 'Y{}-{}m{}-{}d{}-{}'.format(year0,year1,month0,
                                                    month1,day0,day1)
    return(time_periods_code)

def save_member_data_to_pickle(base_dir, member, domain, var_names,
                                time_periods):
    time_periods_code = create_time_periods_code(time_periods)
    if not isinstance(var_names, list):
        var_names = [var_names]
    for var_name in var_names:
        # skip var if this member does not contain in.
        if var_name not in member: continue
        mem_var = member[var_name]
        mem_dict = mem_var.mem_dict
        if 'res' not in mem_dict:
            modres_key = mem_dict['mod']
        else: 
            modres_key = '{}_{:g}'.format(mem_dict['mod'], mem_dict['res'])
        pickle_dir = os.path.join(base_dir, modres_key, mem_dict['case'])
        pickle_file = 'dom_{}_time_{}_var_{}'.format(
                        domain['code'], time_periods_code, var_name)
        pickle_save(mem_var, pickle_dir, pickle_file)


def load_member_data_from_pickle(base_dir, mem_dict, domain, var_names,
                               time_periods, skip_missing):
    time_periods_code = create_time_periods_code(time_periods)
    if 'res' not in mem_dict:
        modres_key = mem_dict['mod']
    else: 
        modres_key = '{}_{:g}'.format(mem_dict['mod'], mem_dict['res'])
    if not isinstance(var_names, list):
        var_names = [var_names]
    member = {}
    for var_name in var_names:
        pickle_dir = os.path.join(base_dir, modres_key, mem_dict['case'])
        pickle_file = 'dom_{}_time_{}_var_{}'.format(
                        domain['code'], time_periods_code, var_name)
        mem_var = pickle_load(pickle_dir, pickle_file)
        if mem_var != False:
            member[var_name] = mem_var
        else:
            print('No pickle file {} for {}'.format(pickle_file,
                                                    modres_key))
            if not skip_missing:
                quit()

    return(member)


if __name__ == '__main__':
    pass
