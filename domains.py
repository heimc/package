#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    geographic domains to use in other scripts
author			Christoph Heim
date created    20.04.2019
date changed    07.04.2021
usage			import in another script
"""
###############################################################################



###############################################################################
# MODEL DOMAINS
###############################################################################

###### 12km
lon0 = -33.04
lat0 = -30.07
nlon = 510
nlat = 456
dom_DYA_12km = {
    'code':'DYA_12km',
    'label':'COSMO 12',
    'lon':slice(lon0, lon0 + 0.11*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.11*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,}

###### 4km
lon0 = -32
lat0 = -29
nlon = 1350
nlat = 1200
dom_DYA_4km = {
    'code':'DYA_4km',
    #'label':'COSMO 4.4 (DYA)',
    'label':'COSMO 4.4',
    'lon':slice(lon0, lon0 + 0.04*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.04*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}
#print(dom_DYA_4km)
#quit()


lon0 = -40
lat0 = -29
nlon = 1600
nlat = 1440
dom_SA_4km = {
    'code':'SA_4km',
    'label':'COSMO 4.4 SA',
    'lon':slice(lon0, lon0 + 0.04*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.04*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}
#print(dom_SA_4km)
#quit()

lon0 = -27
lat0 = -29
nlon = 1200
nlat = 700
dom_SSA_4km = {
    'code':'SSA_4km',
    'label':'COSMO 4.4 SSA',
    'lon':slice(lon0, lon0 + 0.04*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.04*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}


###### 3km
lon0 = -25.02
lat0 = -25.00
nlon = 1450
nlat = 850
dom_SSA_3km = {
    'code':'SSA_3km',
    'label':'COSMO 3.3',
    'lon':slice(lon0, lon0 + 0.03*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.03*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}
#print(dom_SSA_3km)
#quit()

lon0 = -32.01
lat0 = -29.02
nlon = 1800
nlat = 1600
dom_DYA_3km = {
    'code':'DYA_3km',
    'label':'COSMO 3.3',
    'lon':slice(lon0, lon0 + 0.03*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.03*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}
#print(dom_SA_3km_test)
#quit()

lon0 = -34.02
lat0 = -31.00
nlon = 2014
nlat = 1800
dom_SA_3km = {
    'code':'SA_3km',
    'label':'COSMO 3.3',
    'lon':slice(lon0, lon0 + 0.03*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.03*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}
#print(dom_SA_3km)
#quit()



###### 2km
lon0 = -24
lat0 = -24
nlon = 2100
nlat = 1180
dom_DYA_2km = {
    'code':'DYA_2km',
    'label':'COSMO 2.2',
    'lon':slice(lon0, lon0 + 0.02*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.02*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,}
#print(dom_2km)
#quit()


###### 1km
lon0 = -16.8
lat0 = -21
nlon = 3200
nlat = 1830
dom_DYA_1km = {
    'code':'DYA_1km',
    'label':'COSMO 1.1',
    'lon':slice(lon0, lon0 + 0.01*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.01*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,}

###### 0.5km
lon0 = -15.25
lat0 = -19.75
nlon = 5300
nlat = 3100
dom_DYA_05km = {
    'code':'DYA_0.5km',
    'label':'COSMO 0.5',
    'lon':slice(lon0, lon0 + 0.005*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.005*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,}






###### 24km soil spin up
#lon0 = -80.
#lat0 = -50
#nlon = 550
#nlat = 450
lon0 = -75
lat0 = -37
nlon = 490
nlat = 330
dom_lm_24_soil_spinup = {
    'code':'24km',
    'label':'24km',
    'lon':slice(lon0, lon0 + 0.22*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.22*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,}


###### ERA5 download
lon0 = -41
lat0 = -32
nlon = 230
nlat = 190
dom_ERA5 = {
    'code':'ERA5_download',
    'label':'ERA5_download',
    'lon':slice(lon0, lon0 + 0.30*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.30*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}


###### ERA5 COSMO nesting
lon0 = -70.875
lat0 = -31.89694
nlon = 349
nlat = 228
dom_ERA5_COSMO_nesting = {
    'code':'COSMO_nesting',
    'label':'COSMO_nesting',
    'lon':slice(lon0, lon0 + 0.28331*(nlon-1)),
    'lat':slice(lat0, lat0 + 0.28105*(nlat-1)),
    'nlon':nlon,
    'nlat':nlat,
    'dticks':20,}

#print(dom_ERA5_COSMO_nesting)
#quit()



###############################################################################
# ANALYSIS DOMAINS
###############################################################################

######### DYAMOND paper domains START
# South-East Atlantic Stratocumulus
dom_SEA_Sc = {
    'label' :'full analysis',
    'code'  :'SEA_Sc',
    #'lon'   :slice(-15,11),
    #'lat'   :slice(-19.5,-4.5),
    'lon'   :slice(-14.7,10.3),
    'lat'   :slice(-18.4,-4.8),
    'dticks':6,
}
# Snapshot for COSMO LWP
dom_Sc_zoom = {
    'label' :'snapshot',
    'code'  :'test',
    'lon'   :slice(-4,2),
    'lat'   :slice(-15,-9),
    #'lon'   :slice(  6.0, 10.3),
    #'lat'   :slice(-18.4,-14.0),
    'dticks':3,
}
# South-East Atlantic Stratocumulus subdomain for trade wind Cumulus
dom_SEA_Sc_sub_Cu = {
    #'label' :'trade-wind cumulus',
    'label' :'cross-section 2',
    'code'  :'SEA_Sc_sub_Cu',
    'lon'   :slice(-14.7,-10.7),
    #'lat'   :slice(- 8.8,- 4.8),
    'lat'   :slice(- 9.0,- 9.0),
    'dticks':3,
}
# South-East Atlantic Stratocumulus only low-lying part
dom_SEA_Sc_sub_Sc = {
    #'label' :'stratocumulus',
    'label' :'cross-section 1',
    'code'  :'SEA_Sc_sub_Sc',
    #'lon'   :slice(- 0.0,  3.9), # used for spatial plot subticks
    'lon'   :slice(- 0.0,  4.0),
    #'lat'   :slice(-15.0,-11.0),
    'lat'   :slice(-15.0,-15.0),
    'dticks':3,
}
dom_SEA_Sc_sub_St = {
    'label' :'Stratus',
    'code'  :'SEA_Sc_sub_St',
    'lon'   :slice(  6.3, 10.3),
    'lat'   :slice(-18.4,-14.4),
    'dticks':3,
}
######### DYAMOND paper domains END

######### IAV paper domains START
# ITCZ
dom_iav_ITCZ = {
    'label' :'ITCZ',
    'code'  :'ITCZ',
    'lon'   :slice(-37.0, 20.0),
    'lat'   :slice(- 5.0, 25.0),
    'dticks':10,
}
# trade wind / low cloud region
dom_iav_trades = {
    'label' :'Trades',
    'code'  :'trades',
    'lon'   :slice(-23.0, 12.0),
    #'lat'   :slice(-23.0,- 5.0),
    'lat'   :slice(-22.0,- 2.0),
    'dticks':10,
}
######### IAV domains END



# South Atlantic Stratocumulus
dom_test = {
    'label' :'snapshot',
    'code'  :'test',
    'lon'   :slice(-4,2),
    'lat'   :slice(-15,-9),
    'dticks':3,
}
# subdomain DYAMOND simulations
dom_SA = {
    'label' :'South Atlantic',
    'code'  :'SA',
    'lon'   :slice(-35,15),
    'lat'   :slice(-26,5),
}
# domain used for 2D cross-sections
dom_cross_sect = {
    'label' :'cross-section',
    'code'  :'crosss',
    'lon'   :slice(-19, 11),
    'lat'   :slice(-17,-14),
}
# domain used for 2D cross-sections
dom_cs_close = {
    'label' :'close cross-section',
    'code'  :'crosss',
    'lon'   :slice(- 8.0, 10.0),
    'lat'   :slice(-15.1,-14.9),
    #'lat'   :slice(-16.0),
}
# South-East Atlantic Stratocumulus TQC animation
dom_SEA_Sc_anim = {
    'label' :'South-East Atlantic Sc Animation',
    'code'  :'SEA_anim',
    'lon'   :slice(-8,11),
    'lat'   :slice(-18,-6),
    'dticks':6,
    #'lon'   :slice(-4,5.5),
    #'lat'   :slice(-15,-9),
    #'dticks':3,
}

#dom_SEA = {
#    'label' :'South-East Atlantic',
#    'code'  :'SEA',
#    'lon'   :slice(-25,14),
#    'lat'   :slice(-21,0),
#    #'lon'   :slice(0,18),
#    #'lat'   :slice(-16,0),
#}
## South-East Atlantic Stratocumulus
#dom_SEA_Sc_less = {
#    'label' :'Analysis',
#    'code'  :'SEA_Sc',
#    'lon'   :slice(-15,11),
#    'lat'   :slice(-20,-4),
#    'dticks':10,
#}
#



###############################################################################
# ANALYSIS POINTS
###############################################################################
# point St. Helena island
point_st_helena = {
    'label' :'St. Helena grid point',
    'code'  :'StHelena',
    'lon'   :-5.6672,
    'lat'   :-15.9419,
}


# MASTER THESIS ANALYSIS DOMAINS
dom_alpine_region = {
    'label':'Alpine Region',
    ## normal
    #'lon':slice(-3.76+10,3.68+10),
    #'lat':slice(-3.44+47,1.079+47),
    # rotated
    'lon':slice(-3.76,3.68),
    'lat':slice(-3.44,1.079),
}
dom_northern_italy = {
    'label':'Northern Italy',
    'lat':slice(-2.68+10,-1.12+10),
    'lon':slice(-1.96+47,0.60+47),
    ## rotated
    #'lat':slice(-2.68,-1.12),
    #'lon':slice(-1.96,0.60),
}
dom_alpine_ridge = {
    'label':'Alpine Ridge',
    # rotated
    'lon':slice(-1.375, 0.225),
    'lat':slice(-0.695,-0.295),
}
dom_alps_vert_prof = {
    'label':'Vertical Profile',
    #'lon':slice(-1.78+10, 0.62+10),
    #'lat':slice(-1.09+47,-0.09+47),
    # rotated
    'lon':slice(-1.78, 0.62),
    'lat':slice(-1.09,-0.09),
}


###############################################################################
# DYAMOND DOMAINS
###############################################################################
# first DYAMOND extraction domain (small)
dom_dya_1 = {
    'label':'DYAMOND 1',
    'code':'DYAMOND_1',
    'lat':slice(-25,-5),
    'lon':slice(-19,15)
}

# main DYAMOND extraction domain (large)
dom_dya_2 = {
    'label':'DYAMOND 2',
    'code':'DYAMOND_2',
    'lat':slice(-30,18),
    'lon':slice(-45,18)
}





###############################################################################
# OLD ANALYSIS DOMAINS
###############################################################################
## South-East Atlantic 3D cross-sect
#dom_3D = {
#    'label' :'South-East Atlantic',
#    'code'  :'SEA',
#    'lon'   :slice(-17,16),
#    'lat'   :slice(-23,1.6),
#    #'lon'   :slice(0,18),
#    #'lat'   :slice(-16,0),
#}




###############################################################################
# OLD MODEL DOMAINS
###############################################################################
##############################
###### 3 LEVEL NESTING
##############################
####### 12km
#lon0 = -31.06
#lat0 = -31.06
#nlon = 509
#nlat = 342
#dom_lm_12_3lev = {
#    'code':'12km',
#    'label':'12km',
#    'lon':slice(lon0, lon0 + 0.11*nlon),
#    'lat':slice(lat0, lat0 + 0.11*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}
####### 4km
#lon0 = -35
#lat0 = -29
#nlon = 1400
#nlat = 1200
#dom_lm_4_3lev = {
#    'code':'4km',
#    'label':'4km',
#    'lon':slice(lon0, lon0 + 0.04*nlon),
#    'lat':slice(lat0, lat0 + 0.04*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}
#
##############################
###### 2 LEVEL NESTING
##############################
####### 12km_2lev
#lon0 = -28.09
#lat0 = -28.09
#nlon = 460
#nlat = 288
#dom_lm_12_2lev = {
#    'code':'12km_2lev',
#    'label':'12km_2lev',
#    'lon':slice(lon0, lon0 + 0.11*nlon),
#    'lat':slice(lat0, lat0 + 0.11*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}
#
####### 4km_2lev
#lon0 = -27.00
#lat0 = -27.00
#nlon = 1200
#nlat = 740
#dom_lm_4_2lev = {
#    'code':'4km_2lev',
#    'label':'4km_2lev',
#    'lon':slice(lon0, lon0 + 0.04*nlon),
#    'lat':slice(lat0, lat0 + 0.04*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}
#
####### 4km_itcz
#lon0 = -32
#lat0 = -29
#nlon = 1350
#nlat = 1200
#dom_lm_4_itcz = {
#    'code':'4km',
#    'label':'4.4km',
#    'lon':slice(lon0, lon0 + 0.04*nlon),
#    'lat':slice(lat0, lat0 + 0.04*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}


####### 50km alps for CS
#lon0 = -10
#lat0 = 25
#nlon = 90
#nlat = 80
#dom_lm_alps_50km = {
#    'code':'50km',
#    'label':'50km',
#    'lon':slice(lon0, lon0 + 0.45*nlon),
#    'lat':slice(lat0, lat0 + 0.45*nlat),
#    'nlon':nlon,
#    'nlat':nlat,}





