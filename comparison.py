#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description	    Class describing a comparison of a variable 
                accross multiple members.
author			Christoph Heim
date created    21.03.2019
date changed    06.11.2020
usage			use in another script
"""
###############################################################################
import numpy as np
###############################################################################

class Comparison:

    def __init__(self, var_name, members={}):
        """
        An instance of Comparison contains for one variable several
        members that shall be compared in an analysis.
        Note that before loading
        ARGS:
        var_name:   str -   name of variable (in package variable naming
                            convention). 
        members:    dict -  dictionary with key member code_name and value the
                            members (instances of Member class).
        """
        self.var_name = var_name
        self.members = members
        # set self to members
        for member_key,member in self.members.items():
            member[self.var_name].comparison = self

        self.stats = {}
        self.n_members = len(self.members)
        self.mappable = None

    def __repr__(self):
        repr = ''
        for mkey,member in self.members.items():
            repr += '; {}: {:.3g} '.format(mkey,
                        member[self.var_name].var.mean().values)
        return(repr)

    ###########################################################################


    def calc_statistics(self, centred0=False):
        """
        Calculate min and max values of all members and set in self.stat dict.
        Warning: is done only for the var_name for which this comparison object
                    object was created.
        ARGS:
            centred0:   log -   should value range min/max be centred around
                                zero?
        COMMENTS:
        """
        glob_min = np.Inf
        glob_max = -np.Inf
        for member_key,member in self.members.items():
            #print(var_mem.var)
            min = member[self.var_name].var.min().values
            if min < glob_min:
                glob_min = min
            max = member[self.var_name].var.max().values
            if max > glob_max:
                glob_max = max
        # centre values around 0
        if centred0:
            glob_max = np.max([np.abs(glob_min), np.abs(glob_max)]) 
            glob_min = - glob_max
        self.stats['min'] = glob_min
        self.stats['max'] = glob_max

